module test_mixeos_mod
  use mixeos_lib
  use mixeos_def
  use chem_lib
  use chem_def
  use eos_def
  use eos_lib
  use const_def, only: arg_not_provided
  use const_lib
  
  implicit none
  
  integer :: ierr
  integer :: Xsize
  integer :: Ysize
  character (len=256) :: filename, name, data_dir, eos_file_prefix, my_mesa_dir
  integer :: i, id, k
  logical, parameter :: use_cache = .true.
  logical, parameter :: load_h5water = .false.
  logical, parameter :: load_idwater = .true.
  logical :: load_water = .false.

  real(dp) :: X, Z, Zfrac_water, Y, abar, zbar, z2bar, ye, &
       mass_correction, sumx
  integer, parameter :: species = 4
  integer, parameter :: h1=1, he4=2, li7=3, water=4
  integer, pointer, dimension(:) :: net_iso, chem_id
  integer :: i_water
  real(dp), target :: xa(species), dabar_dx(species), dzbar_dx(species), dmc_dx(species)
  
  real(dp) :: P, log10P, T, lnT, log10T, Rho, log10Rho, lnRho, E
  real(dp), dimension(num_eos_basic_results) :: res_output, &
       d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
       res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy
  
  real(dp), parameter :: log10Exp = log10(exp(1d0))
  real(dp) :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta
  real(dp) :: mu
  
  integer :: eos_handle
  
contains
  
  subroutine setup_mixeos
    implicit none

    real(dp) :: frac, dabar_dx(species), dzbar_dx(species),  &
         sumx, xh, xhe

    my_mesa_dir = "/Users/neil/research/mesa"
    call const_init(my_mesa_dir, ierr)
    
    write(*,*) "Initializing chem"
    ierr = 0
    filename = "isotopes.data"
    call chem_init(filename, ierr)
    write(*,*) " ierr = ", ierr
    
    write(*,*) "Initializing eos"
    eos_file_prefix = 'mesa'
    call eos_init(eos_file_prefix, use_cache, ierr)
    write(*,*) " ierr = ", ierr
    
    !! Setup the eos
    write(*,*) "Calling mixeos_init"
    call mixeos_init(ierr)
    write(*,*) "ierr = ", ierr
    write(*,*)   

    !! parameters we have to pass to mixeos
    id = -1
    k = 0
    eos_handle = 1

    if(load_idwater) then
       name = "water"
       mu = 18.d0
       write(*,*) "Loading ideal water"
       call mixeos_add_ideal(mu, name)
       load_water = .true.
    else if(load_h5water) then
       write(*,*) "Adding water to the database"
       Xsize = 151
       Ysize = 51
       filename = "/Users/neil/research/mesa.planet/mixeos/data/wateridealfe.bin"
       name = "water"
       
       minlogRhoAlpha = 0d0
       minlogRhoBeta = -4.8d0
       !  maxlogRhoAlpha = 7.5
       !  maxlogRhoBeta = -24.25
       maxlogRhoAlpha = 9.0 
       maxlogRhoBeta = -24d0 
       call mixeos_add_h5tbl(Xsize, Ysize, filename, name, &
            minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta, ierr)
       write(*,*) "ierr = ", ierr
       load_water = .true.
    else
       write(*,*) "Not loading water"
    endif
    
    
    !! Setting up our custom composition {
    allocate(net_iso(num_chem_isos), chem_id(species), stat=ierr)
    
    chem_id(h1) = ih1; net_iso(ih1) = h1
    chem_id(he4) = ihe4; net_iso(ihe4) = he4
    chem_id(li7) = ili7; net_iso(ili7) = li7
    if(load_water) then
       i_water = get_nuclide_index("water")
    
       chem_id(water) = i_water; net_iso(i_water) = water
       
       write(*,*) "chem_id(", water,  ") = ", i_water
       write(*,*) "net_iso(", i_water, ") = ", water 
    endif
    X = 0.79
    Y = 0.20
    Z = 0.01
    xa(h1) = X
    xa(he4) = Y
    xa(li7) = 0.001
    if(load_water) then
       xa(water) = 0d0
    endif
    !! This may be the issue
    call composition_info( &
         species, chem_id, xa, xh, xhe, abar, zbar, z2bar, ye,  &
         mass_correction, sumx, dabar_dx, dzbar_dx, dmc_dx)
    ! for now, the eos requires "approx" values for abar and zbar
    
    Z = 0.2d0
    X = 0.8d0 * (1d0-Z)
    
    if(load_water) then
       Zfrac_water = 1.0d0
       
       xa(h1) = X
       xa(he4) = Y
       xa(water) = Z * Zfrac_water
    endif
    id = alloc_eos_handle(ierr)
    write(*,*) "Composition setup"
    !! } End setting up our custom composition

  end subroutine setup_mixeos

  subroutine shutdown_mixeos

    if(associated(net_iso)) then
       deallocate(net_iso)
       nullify(net_iso)
    endif

    if(associated(chem_id)) then
       deallocate(chem_id) 
       nullify(chem_id)
    endif

    write(*,*) "shutting down mixeos"
    call mixeos_shutdown      
    write(*,*) "mixeos shutdown done"
    call eos_shutdown
  end subroutine shutdown_mixeos


  subroutine test_lookupPT
    use mixeos_def
    use thermo
    integer, dimension(8) :: values1, values2
    integer :: i, which_other, eos_calls, max_iter
    real(dp) :: other_value, logRho_tol, other_tol, &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_output, logRho_guess, &
         dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas
    real(dp) :: minlnP=100, maxlnP=-100, minlnS, maxlnS
    character (len=256) :: filename
    integer ::ios

    real(dp), dimension(num_derivs) :: lnPVect, lnEVect, lnSVect, &
         PVect, EVect, SVect, lnPVect2, lnEVect2, lnSVect2

    call date_and_time(values=values1)

    which_other = i_lnPgas
    logRho_tol = 1d-4
    other_tol = 1d-4 

    filename = "P-S.dat"
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios, action='write', form='formatted')

    log10T = 5.
    T = 1d1**log10T
    lnT = log(T)

    do i=1,5
       write(*,*) 
       write(*,*) "-----------------------TEST LOG10P,LOG10T LOOKUP-----------------------"
       log10P = 15+1*i
       P = 10d0**log10P
       write(*,*) "log10P: ", log10P
       write(*,*) "log10T: ", log10T
       write(*,*) "-----------------------------------------------------------------------"

       other_value = log10(P)
       logRho_guess = arg_not_provided
       logRho_bnd1 = arg_not_provided
       logRho_bnd2 = arg_not_provided
       other_at_bnd1 = arg_not_provided
       other_at_bnd2 = arg_not_provided
       max_iter = 100

       call mixeosPT_get(id, k, eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa,&
            P, log10P, T, log10T, &
            Rho, logRho_output, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       
!       call mixeos_get_rho( &
!            id, k, eos_handle, Z, X, abar, zbar, &
!            species, chem_id, net_iso, xa, &
!            log10T, which_other, other_value, &
!            logRho_tol, other_tol, max_iter, logRho_guess,  &
!            logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
!            logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
!            eos_calls, ierr)


       if(ierr .ne. 0) then
          write(*,*) "Lookup could not be peformed.  Error detected at top level"
          write(*,*) "ierr = ", ierr
       else
          Rho = 1d1**logRho_output
          lnRho = log(Rho)

          if(log(P) .gt. maxlnP) then
             maxlnP = log(P)
             minlnS = res_output(i_lnS)
          endif
          if(log(P) .lt. minlnP) then
             minlnP = log(P)
             maxlnS = res_output(i_lnS)
          endif

          write(*,*) "[mixeos_get_rho Success]"
          write(*,*) "Rho: ", Rho
          write(*,*) "res_output: ", res_output
          write(*,*) "d_dlnRho: ", d_dlnRho_c_T_output
          write(*,*) "d_dlnT: ", d_dlnT_c_Rho_output

          write(*,*) "[Convert RES -> lnderivs]"
          call convRES_lnderivs(res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
               lnRho, lnT, lnPVect, lnEVect, lnSVect)
          write(*,*) "lnPVect: ", lnPVect
          write(*,*) "lnEVect: ", lnEVect
          write(*,*) "lnSVect: ", lnSVect

          write(*,*) "[Convert lnderivs -> derivs]"
          call convlnDerivsDerivs(lnRho, lnT, lnPVect, lnEVect, lnSVect, &
               PVect, EVect, SVect)
          write(*,*) "PVect: ", PVect
          write(*,*) "EVect: ", EVect
          write(*,*) "SVect: ", SVect

          write(*,*) "[Convert derivs -> lnderivs]"
          call convDerivslnDerivs(Rho, T, PVect, EVect, SVect, &
               lnPVect2, lnEVect2, lnSVect2)
          write(*,*) "lnPVect2: ", lnPVect2
          write(*,*) "lnEVect2: ", lnEVect2
          write(*,*) "lnSVect2: ", lnSVect2

          write(*,*) "[Difference between lnDerivs1 and lnDerivs2]"
          write(*,*) "lnPVectDiff: ", lnPVect - lnPVect2
          write(*,*) "lnEVectDiff: ", lnEVect - lnEVect2
          write(*,*) "lnSVectDiff: ", lnSVect - lnSVect2

          write(*,*) "[Convert lnderivs -> RES]"
          call convlnderivs_RES(lnRho, lnT, lnPVect2, lnEVect2, lnSVect2, &
               res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy)
          write(*,*) "res_copy: ", res_copy

          write(*,*) "[Difference between RES_copy and RES_output]"
          write(*,*) "res diff: ", res_copy - res_output
          write(*,*) "d_dlnRho diff: ", d_dlnRho_c_T_output - d_dlnRho_c_T_copy
          write(*,*) "d_dlnT diff: ", d_dlnT_c_Rho_output - d_dlnT_c_Rho_copy


          write(19,'(e14.6,e14.6,e14.6)') log(P), res_output(i_lnS), logRho_output
       endif
    enddo
    
    close(unit=19)

    call date_and_time(values=values2)
    write(*,*) values2-values1
    write(*,*) "LN PRESSURE RANGE: ", minlnP, maxlnP
    write(*,*) "LN S range: ", minlnS, maxlnS
  end subroutine test_lookupPT
  
  subroutine test_get_rhot
    real(dp) :: other_value, logRho_guess, &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_tol, other_tol, logRho_output, dlnRho_dlnP_const_T, &
         dlnRho_dlnT_const_P
    integer :: max_iter, eos_calls, which_other
    
    log10P = 10
    log10T = 5.3
    P = 10d0**log10P
    T = 10d0**log10T

    logRho_tol = 1d-4
    other_tol = 1d-4  
    which_other = i_lnPgas
    other_value = log(P)
    logRho_guess = arg_not_provided
    logRho_bnd1 = arg_not_provided
    logRho_bnd2 = arg_not_provided
    other_at_bnd1 = arg_not_provided
    other_at_bnd2 = arg_not_provided
    max_iter = 100

    write(*,*) "Calling mixeosPT_get"
    

    call mixeosPT_get(id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, &
         Rho, logRho_output, dlnRho_dlnP_const_T, dlnRho_dlnT_const_P, &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

    Rho = 1d1**logRho_output


    write(*,*) "PT lookup"
    call print_res(res_output)
    
    write(*,*) "Calling mixeos_get"

    log10Rho = logRho_output
    call mixeos_get( &
         id, k, eos_handle, Z, X, abar, zbar, & 
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, & 
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

    write(*,*) "Rho T lookup"
    call print_res(res_output)
  end subroutine test_get_rhot

  subroutine test_PT_grid
    use const_def
    use eos_def
    use mixeos_def, only: i_val, i_dlnRho, i_dlnT, i_dlnRho2, i_dlnT2, i_dlnRhodlnT, num_derivs
    use thermo, only: convRES_lnderivs
    real(dp), dimension(:), pointer :: log10PVect, log10TVect
    real(dp), dimension(:,:), pointer :: log10Rhomat, log10Emat, log10Smat

    real(dp) :: minlog10P, maxlog10P, minlog10T, maxlog10T, dlog10P, dlog10T
    character (len=256) :: filename
    real(dp) :: logRho_bnd1, logRho_bnd2, logRho_guess, logRho_output, &
         other_value, other_at_bnd1, other_at_bnd2
    real(dp) :: logRho_tol, other_tol
    real(dp), pointer, dimension(:,:,:) :: eos_res_mat
    real(dp), pointer, dimension(:,:,:) :: eos_d_dlnRho_mat, eos_d_dlnT_mat
    real(dp), pointer, dimension(:,:,:) :: lnPvec_mat, lnEvec_mat, lnSvec_mat
    real(dp), pointer, dimension(:) :: lnPVect, lnEVect, lnSVect

    integer :: p_ind, t_ind, i
    integer :: eos_calls, max_iter, which_other


    Xsize = 256
    Ysize = 256

    allocate(log10PVect(Xsize), log10TVect(Ysize))
    allocate(log10Rhomat(Xsize,Ysize), log10Emat(Xsize,Ysize), log10Smat(Xsize,Ysize))
    allocate(eos_res_mat(Xsize,Ysize,num_eos_basic_results))
    allocate(eos_d_dlnRho_mat(Xsize,Ysize,num_eos_basic_results))
    allocate(eos_d_dlnT_mat(Xsize,Ysize,num_eos_basic_results))
    allocate(lnPvec_mat(Xsize,Ysize,num_derivs))
    allocate(lnEvec_mat(Xsize,Ysize,num_derivs))
    allocate(lnSvec_mat(Xsize,Ysize,num_derivs))
    allocate(lnPVect(num_derivs), lnEVect(num_derivs), lnSVect(num_derivs))
    
    
    !! I'm sure some of this region fails, but it needs to return quickly in that zone
    
    minlog10P = 5.0
    maxlog10P = 15.0
    minlog10T = 2.0
    maxlog10T = 5.0

    dlog10P = (maxlog10P - minlog10P) / (Xsize-1)
    dlog10T = (maxlog10T - minlog10T) / (Ysize-1)

    do p_ind=1,Xsize
       log10PVect(p_ind) = minlog10P + dlog10P * (p_ind-1) 
    enddo

    do t_ind=1,Ysize
       log10TVect(t_ind) = minlog10T + dlog10T * (t_ind-1)
    enddo
    
    which_other = i_lnPgas

    logRho_tol = 1d-4
    other_tol = 1d-4
    do p_ind=1,Xsize         
       do t_ind = 1,Ysize

          log10T = log10TVect(t_ind)
          log10P = log10Pvect(p_ind)
          other_value = log10PVect(p_ind) / log10(exp(1d0)) !! make this into ln(P)
          logRho_guess = arg_not_provided
          logRho_bnd1 = arg_not_provided
          logRho_bnd2 = arg_not_provided
          other_at_bnd1 = arg_not_provided
          other_at_bnd2 = arg_not_provided
          max_iter = 100

          !! This should functionally be identical to mixeosPT_get
          call mixeos_get_rho( &
               id, k, eos_handle, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               log10T, which_other, other_value, &
               logRho_tol, other_tol, max_iter, logRho_guess,  &
               logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
               logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
               eos_calls, ierr)

          if(ierr .ne. 0) then
             log10Rhomat(p_ind, t_ind) = -100.
             log10Emat(p_ind, t_ind)   = -100.
             log10Smat(p_ind, t_ind)   = -100.
             lnPvec_mat(p_ind,t_ind,1:num_derivs) = 0d0
             lnEvec_mat(p_ind,t_ind,1:num_derivs) = 0d0
             lnSvec_mat(p_ind,t_ind,1:num_derivs) = 0d0

             eos_res_mat(p_ind,t_ind,:) = 0d0
             eos_d_dlnRho_mat(p_ind,t_ind,:) = 0d0
             eos_d_dlnT_mat(p_ind,t_ind,:) = 0d0
          else
             if(isnan(logRho_output)) then
                log10Rhomat(p_ind, t_ind) = -100. 
             else
                log10Rhomat(p_ind, t_ind) = logRho_output
             end if
             if(isnan(res_output(i_lnE))) then
                log10Emat(p_ind, t_ind) = -100.
             else
                log10Emat(p_ind, t_ind) = res_output(i_lnE) / log(1d1)
             end if
             if(isnan(res_output(i_lnS))) then
                log10Smat(p_ind, t_ind) = -100. !! not sure whats going on here - need to fix
             else
                log10Smat(p_ind, t_ind) = res_output(i_lnS) / log(1d1)
             end if
             
             eos_res_mat(p_ind,t_ind,:) = res_output
             eos_d_dlnRho_mat(p_ind,t_ind,:) = d_dlnRho_c_T_output
             eos_d_dlnT_mat(p_ind,t_ind,:) = d_dlnT_c_Rho_output
             
             do i=1,num_eos_basic_results
                if(isnan(eos_res_mat(p_ind,t_ind,i))) then
                   eos_res_mat(p_ind,t_ind,i) = -100d0
                   write(*,*) "test_PT_grid NaN at ", i
                   write(*,*) "log10P = ", log10P
                   write(*,*) "log10T = ", log10T
                endif
                if(isnan(eos_d_dlnRho_mat(p_ind,t_ind,i))) then
                   eos_d_dlnRho_mat(p_ind,t_ind,i) = -100d0
                endif
                if(isnan(eos_d_dlnT_mat(p_ind,t_ind,i))) then
                   eos_d_dlnT_mat(p_ind,t_ind,i) = -100d0
                endif
             end do

             lnRho = logRho_output / log10Exp
             lnT = log10T / log10Exp
             call convRES_lnderivs(res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, lnRho, lnT, &
                  lnPVect, lnEVect, lnSVect)
             lnPvec_mat(p_ind, t_ind, 1:num_derivs) = lnPVect(1:num_derivs)
             lnEvec_mat(p_ind, t_ind, 1:num_derivs) = lnEVect(1:num_derivs)
             lnSvec_mat(p_ind, t_ind, 1:num_derivs) = lnSVect(1:num_derivs)
             do i=1,num_derivs
                if(isnan(lnPvec_mat(p_ind, t_ind, i))) then
                   lnPvec_mat(p_ind, t_ind, i) = -100d0
                endif
                if(isnan(lnEvec_mat(p_ind, t_ind, i))) then
                   lnEvec_mat(p_ind, t_ind, i) = -100d0
                endif
                if(isnan(lnSvec_mat(p_ind, t_ind, i))) then
                   lnSvec_mat(p_ind, t_ind, i) = -100d0
                endif
             end do

          end if
       enddo
    enddo

    filename = "PTdata/log10PVect.data"      
    call write_vect(log10PVect, filename)

    filename = "PTdata/log10TVect.data"
    call write_vect(log10TVect, filename)

    filename = "PTdata/log10Rho.data"
    call write_matrix(log10Rhomat, filename)

    filename = "PTdata/log10E.data"
    call write_matrix(log10Emat, filename)

    filename = "PTdata/log10S.data"
    call write_matrix(log10Smat, filename)

    !!lnE
    filename = "PTdata/lnE.data"
    call write_matrix(lnEvec_mat(:,:,i_val), filename)

    filename = "PTdata/dlnEdlnRho.data"
    call write_matrix(lnEvec_mat(:,:,i_dlnRho), filename)

    filename = "PTdata/dlnEdlnT.data"
    call write_matrix(lnEvec_mat(:,:,i_dlnT), filename)

    filename = "PTdata/d2lnEdlnRho2.data"
    call write_matrix(lnEvec_mat(:,:,i_dlnRho2), filename)

    filename = "PTdata/d2lnEdlnT2.data"
    call write_matrix(lnEvec_mat(:,:,i_dlnT2), filename)

    filename = "PTdata/d2lnEdlnRhodlnT.data"
    call write_matrix(lnEvec_mat(:,:,i_dlnRhodlnT), filename)
    !!lnS
    filename = "PTdata/lnS.data"
    call write_matrix(lnSvec_mat(:,:,i_val), filename)

    filename = "PTdata/dlnSdlnRho.data"
    call write_matrix(lnSvec_mat(:,:,i_dlnRho), filename)

    filename = "PTdata/dlnSdlnT.data"
    call write_matrix(lnSvec_mat(:,:,i_dlnT), filename)
    
    filename = "PTdata/d2lnSdlnRho2.data"
    call write_matrix(lnSvec_mat(:,:,i_dlnRho2), filename)
    
    filename = "PTdata/d2lnSdlnT2.data"
    call write_matrix(lnSvec_mat(:,:,i_dlnT2), filename)
    
    filename = "PTdata/d2lnSdlnRhodlnT.data"
    call write_matrix(lnSvec_mat(:,:,i_dlnRhodlnT), filename)
    
    do i=1,num_eos_basic_results
       filename = "PTdata/res_" // trim(eosDT_result_names(i)) // ".data"
       call write_matrix(eos_res_mat(:,:,i), filename)
       
       filename = "PTdata/d_dlnRho_" // trim(eosDT_result_names(i)) // ".data"
       call write_matrix(eos_d_dlnRho_mat(:,:,i), filename)
       
       filename = "PTdata/d_dlnT_" // trim(eosDT_result_names(i)) // ".data"
       call write_matrix(eos_d_dlnT_mat(:,:,i), filename)
    enddo
    
    deallocate(log10PVect, log10TVect)
    deallocate(log10Rhomat, log10Emat, log10Smat)
    deallocate(eos_res_mat, eos_d_dlnRho_mat,eos_d_dlnT_mat)
    deallocate(lnPvec_mat, lnEvec_mat, lnSvec_mat)
    deallocate(lnPVect, lnEVect, lnSVect)

  contains
    subroutine write_vect(vect, filename)
      real(dp), intent(in), dimension(:) :: vect
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19,'(e14.6)') vect
      close(unit=19)

    end subroutine write_vect

    subroutine write_matrix(matrix, filename)
      real(dp), intent(in), dimension(:,:) :: matrix
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19, '(e14.6)') matrix
      close(unit=19)
    end subroutine write_matrix
  end subroutine test_PT_grid
  
  !! Plot out grids for logRho(logP, logT) and 
  !!                    logP(logRho, logT)
  !!                    logRho(logP, X) | logT
  !!                    logP(logRho, X) | logT
  
  subroutine orig_eos_grid
    use eos_def
    use eos_lib
    use chem_def
    use chem_lib
    use const_lib
    implicit none
    
    character (len=256) :: filename
    integer, parameter :: Xsize = 256, Ysize = 256
    integer :: i, j
    real(dp) :: logPvect(Xsize), logTvect(Ysize), logRhovect(Xsize), Zvect(Ysize)
    real(dp) :: logRho_mat(Xsize,Ysize), logP_mat(Xsize,Ysize)
!    real(dp) :: gradad_mat(Xsize,Ysize), logE_mat(Xsize,Ysize), logS_mat(Xsize,Ysize)
    real(dp) :: gradad_ij(Xsize,Ysize), logE_ij(Xsize,Ysize), logS_ij(Xsize,Ysize)
    real(dp) :: eos_res(num_eos_basic_results), eos_dlnRho(num_eos_basic_results), &
         eos_dlnT(num_eos_basic_results)

    real(dp) :: min_logP, max_logP, min_logRho, max_logRho, d_logP, d_logRho, &
                min_logT, max_logT, d_logT, &
                dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas
    
    write(*,*) "Orig eos grid"

    min_logP = 5.0
    max_logP = 20.0
    min_logRho = -5d0
    max_logRho = 2d0
    min_logT = 1d0
    max_logT = 7d0
    d_logP = (max_logP - min_logP) / (Xsize - 1)
    d_logRho = (max_logRho - min_logRho) / (Xsize - 1)
    d_logT = (max_logT - min_logT) / (Xsize - 1)

    write(*,*) "looking up orig PT table"
    do i=1,Xsize
       do j=1,Ysize
          log10P = min_logP + (i-1) * d_logP
          log10T = min_logT + (j-1) * d_logT
          P = 1d1**log10P
          T = 1d1**log10T
          logPvect(i) = log10P
          logTvect(j) = log10T

          call eosPT_get( &
               id, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               P, log10P, T, log10T, &
               Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
               eos_res, eos_dlnRho, eos_dlnT, ierr)
          logRho_mat(i,j) = log10Rho

          if((i.le.0).or.(i.gt.Xsize).or.(j.lt.0).or.(j.gt.Ysize)) then
             write(*,*) "index out of bounds"
          endif

          logE_ij(i,j) = eos_res(i_lnE) / log(1d1)
          logS_ij(i,j) = eos_res(i_lnS) / log(1d1)
          gradad_ij(i,j) = eos_res(i_grad_ad)
          
       enddo
    enddo

    do i=1,Xsize
       do j=1,Ysize
          log10Rho = min_logRho + (i-1) * d_logRho
          log10T = min_logT + (j-1) * d_logT
          Rho = 1d1**log10Rho
          T = 1d1**log10T
          
          logRhovect(i) = log10Rho
          
          call eosDT_get( &
               id, Z, X, abar, zbar,  &
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, &
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          logP_mat(i,j) = res_output(i_lnPgas) / ln10
       enddo
    enddo

    filename = "plot.data/test_basic_eos/log10Pvect.data"
    call write_vect(logPvect, filename)
    
    filename = "plot.data/test_basic_eos/log10Tvect.data"
    call write_vect(logTvect, filename)

    filename = "plot.data/test_basic_eos/log10Rhomat.data"
    call write_matrix(logRho_mat, filename)
    
    filename = "plot.data/test_basic_eos/gradad_mat.data"
    call write_matrix(gradad_ij, filename)
    
    filename = "plot.data/test_basic_eos/log10Emat.data"
    call write_matrix(logE_ij, filename)
    
    filename = "plot.data/test_basic_eos/log10Smat.data"
    call write_matrix(logS_ij, filename)    

    filename = "plot.data/test_basic_eos/log10Rhovect.data"
    call write_vect(logRhovect, filename)
    
    filename = "plot.data/test_basic_eos/log10Pmat.data"
    call write_matrix(logP_mat, filename)

  contains
    subroutine write_vect(vect, filename)
      real(dp), intent(in), dimension(:) :: vect
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19,'(e14.6)') vect
      close(unit=19)

    end subroutine write_vect

    subroutine write_matrix(matrix, filename)
      real(dp), intent(in), dimension(:,:) :: matrix
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19, '(e14.6)') matrix
      close(unit=19)
    end subroutine write_matrix
  end subroutine orig_eos_grid

  subroutine mixvect_PT(log10P, log10T, filename)
    use eos_def
    use eos_lib
    use chem_def
    use chem_lib
    use const_lib
    implicit none

    real(dp), intent(in) :: log10P, log10T
    character (len=256) :: filename

    integer :: fid = 19, i, ierr, ios
    integer, parameter :: LEN = 101
    real(dp) :: Z_i, log10S_i, log10Rho_i, log10E_i
    real(dp) :: P, T, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas
    real(dp) :: res(num_eos_basic_results), d_dlnRho_const_T(num_eos_basic_results), &
         d_dlnT_const_Rho(num_eos_basic_results)
    real(dp) :: dabar_dx(species), dzbar_dx(species),  &
         sumx, xh, xhe, approx_abar, approx_zbar

    P = 1d1**log10P
    T = 1d1**log10T

    write(*,*) "mixvect_PT", P, T
    open(unit=fid, file=trim(filename), status='replace',&
         iostat=ios, action='write', form='formatted')
    do i=1,LEN
       Z_i = real(i-1) / real(LEN-1)
       Z = Z_i
       X = 0.8 * (1d0 - Z)
       xa(h1) = X
       xa(he4) = Y
       xa(water) = Z * Zfrac_water

       write(*,*) Z_i


       call mixeosPT_get(id, k, eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            P, log10P, T, log10T, &
            Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
            res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)

       log10Rho_i = log10Rho
       log10E_i = res(i_lnE) / ln10
       log10S_i = res(i_lnS) / ln10
       if(ierr .ne. 0) then
          write(*,*) "Error in mixvect_PT", ierr
       endif
       write(fid,'(6e14.6)') Z_i, log10P, log10T, log10Rho_i, log10E_i, log10S_i
    enddo

    close(unit=fid)
  end subroutine mixvect_PT

  subroutine test_do_grid
    real(dp) :: lnRho1, lnRho2, lnRho!, Rho
    real(dp) :: lnT1, lnT2, lnT!, T
    integer :: rho_ind, t_ind

    real(dp), dimension(:), allocatable :: lnRhoVect, lnTVect, log10RhoVect, log10TVect
    real(dp), dimension(:,:), allocatable :: lnPmat, lnEmat, lnSmat
    real(dp), dimension(:,:), allocatable :: grad_ad_mat, dlnP_dlnRho_mat, dlnP_dlnT_mat, &
                                             Cp_mat, Cv_mat, dE_dRho_mat, dS_dT_mat, dS_dRho_mat

    character (len=256) :: filename

    Xsize = 512
    Ysize = 512

    allocate(lnRhoVect(Xsize), log10RhoVect(Xsize))
    allocate(lnTVect(Ysize), log10TVect(Ysize))
    allocate(lnPmat(Xsize,Ysize),lnEmat(Xsize,Ysize),lnSmat(Xsize,Ysize))
    allocate(grad_ad_mat(Xsize,Ysize),dlnP_dlnRho_mat(Xsize,Ysize), &
         dlnP_dlnT_mat(Xsize,Ysize),Cp_mat(Xsize,Ysize),Cv_mat(Xsize,Ysize),&
         dE_dRho_mat(Xsize,Ysize),dS_dT_mat(Xsize,Ysize),dS_dRho_mat(Xsize,Ysize))

    lnRho1 = -6.
    lnRho2 = 4.
    lnT1 = 6.
    lnT2 = 15.

    do rho_ind = 1, Xsize
       do t_ind = 1, Ysize
          lnRho = lnRho1 + (rho_ind - 1.) *  (lnRho2 - lnRho1) / Xsize
          lnT = lnT1 + (t_ind-1.) * (lnT2 - lnT1) / Ysize

          lnRhoVect(rho_ind) = lnRho
          lnTVect(t_ind) = lnT

          Rho = exp(lnRho)
          T = exp(lnT)

          log10Rho = log10(Rho)
          log10T = log10(T)
          log10RhoVect(rho_ind) = log10Rho
          log10TVect(t_ind) = log10T
          call mixeos_get( &
               id, k, eos_handle, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, &
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

          if(ierr .eq. 0) then
             lnPmat(rho_ind,t_ind) = res_output(i_lnPgas)
             lnEmat(rho_ind,t_ind) = res_output(i_lnE)
             lnSmat(rho_ind,t_ind) = res_output(i_lnS)
             grad_ad_mat(rho_ind,t_ind) = res_output(i_grad_ad)
             dlnP_dlnRho_mat(rho_ind,t_ind) = res_output(i_chiRho)
             dlnP_dlnT_mat(rho_ind,t_ind) = res_output(i_chiT)
             Cp_mat(rho_ind,t_ind) = res_output(i_Cp)
             Cv_mat(rho_ind,t_ind) = res_output(i_Cv)
             dE_dRho_mat(rho_ind,t_ind) = res_output(i_dE_dRho)
             dS_dT_mat(rho_ind,t_ind) = res_output(i_dS_dT)
             dS_dRho_mat(rho_ind,t_ind) = res_output(i_dS_dRho)
          else
             lnPmat(rho_ind,t_ind) = -100d0
             lnEmat(rho_ind,t_ind) = -100d0
             lnSmat(rho_ind,t_ind) = -100d0
             grad_ad_mat(rho_ind,t_ind) = -100d0
             dlnP_dlnRho_mat(rho_ind,t_ind) = -100d0
             dlnP_dlnT_mat(rho_ind,t_ind) = -100d0
             Cp_mat(rho_ind,t_ind) = -100d0
             Cv_mat(rho_ind,t_ind) = -100d0
             dE_dRho_mat(rho_ind,t_ind) = -100d0
             dS_dT_mat(rho_ind,t_ind) = -100d0
             dS_dRho_mat(rho_ind,t_ind) = -100d0
          endif
       enddo
    enddo
    
    filename = "DTdata/log10RhoVect.data"
    call write_vect(log10RhoVect, filename)
    
    filename = "DTdata/log10TVect.data"
    call write_vect(log10TVect, filename)
    
    filename = "DTdata/lnRhoVect.data"
    call write_vect(lnRhoVect, filename)

    filename = "DTdata/lnTVect.data"
    call write_vect(lnTVect, filename)

    filename = "DTdata/lnPmat.data"
    call write_matrix(lnPmat, filename)

    filename = "DTdata/lnEmat.data"
    call write_matrix(lnEmat, filename)

    filename = "DTdata/lnSmat.data"
    call write_matrix(lnSmat, filename)

    filename = "DTdata/grad_ad.data"
    call write_matrix(grad_ad_mat, filename)

    filename = "DTdata/dlnP_dlnRho.data"
    call write_matrix(dlnP_dlnRho_mat, filename)

    filename = "DTdata/dlnP_dlnT.data"
    call write_matrix(dlnP_dlnT_mat, filename)

    filename = "DTdata/Cp.data"
    call write_matrix(Cp_mat, filename)
    
    filename = "DTdata/Cv.data"
    call write_matrix(Cv_mat, filename)

    filename = "DTdata/dE_dRho.data"
    call write_matrix(dE_dRho_mat, filename)
    
    filename = "DTdata/dS_dT.data"
    call write_matrix(dS_dT_mat, filename)
    
    filename = "DTdata/dS_dRho.data"
    call write_matrix(dS_dRho_mat, filename)
    
    deallocate(lnRhoVect)
    deallocate(lnTVect)
    deallocate(lnPmat)
    deallocate(lnEmat)
    deallocate(lnSmat)
    deallocate(grad_ad_mat)
    deallocate(dlnP_dlnRho_mat)
    deallocate(dlnP_dlnT_mat)
    deallocate(Cp_mat)
    deallocate(Cv_mat)
    deallocate(dE_dRho_mat)
    deallocate(dS_dT_mat)
    deallocate(dS_dRho_mat)

  contains
    subroutine write_vect(vect, filename)
      real(dp), intent(in), dimension(:) :: vect
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19,'(e14.6)') vect
      close(unit=19)

    end subroutine write_vect

    subroutine write_matrix(matrix, filename)
      real(dp), intent(in), dimension(:,:) :: matrix
      character (len=256) :: filename
      integer :: ios

      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19, '(e14.6)') matrix
      close(unit=19)
    end subroutine write_matrix
  end subroutine test_do_grid


  subroutine test_PT_find_range
    use mixeos_mod  !! This is private module I am testing
    
    integer :: NSTEP = 50
    real(dp) :: minLogP, maxLogP, dLogP, minLogT, maxLogT, dLogT
    integer :: which_other, doing_P
    real(dp) :: other_value, log10P1, log10P2, log10T1, log10T2, other1, other2, &
                log10P1a, log10P2a, log10P1b, log10P2b
    
    logical, parameter :: find_P_range = .true.
    logical, parameter :: find_T_range = .false.

    integer :: ierr2, ierr3

    minLogT = 3d0
    maxLogT = 8d0
    dlogT = (maxLogT - minLogT) / real(NSTEP-1)
    
    which_other = i_lnPgas
    other_value = 18d0

    if(find_P_range) then
       doing_P = 0
       write(*,*) "Find P range"
       do i=1,NSTEP
          log10T = minLogT + dlogT * (i-1)          
          T = 1d1**log10T
          call PT_find_range(eos_handle, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               T, log10T, which_other, other_value, doing_P, &
               log10P1, log10P2, other1, other2, ierr)
          write(*,*) "PT_find_range results for log10T = ", log10T
          write(*,*) "log10Prange: ", log10P1, log10P2
          write(*,*) "other range: ", other1, other2
          write(*,*) "ierr = ", ierr
          write(*,*)
       !! Report what happened here
          
          doing_P = 0
          call PT_find_domain(T, log10T, doing_P, log10P1b, log10P2b, ierr3)
          write(*,*) "PT_find_domain results: ", log10P1b, log10P2b
          
          call get_pressure_range(T, log10T, log10P1a, log10P2a, ierr2)
          write(*,*) "log10Prange check: ", log10P1a, log10P2a
          write(*,*) "ierr2 = ", ierr2
          other_value = ((log10P1a + log10P2a) / 2d0) * ln10
       enddo

    end if

    if(find_T_range) then
       doing_P = 0
       minLogP = 7d0
       maxLogP = 19d0
       dlogP = (maxLogP - minLogP) / real(NSTEP-1)
       
       which_other = i_lnS
       other_value = 20d0
       
       do i=1,NSTEP
          log10P = minLogP + dlogP * (i-1)
          P = 1d1**log10P
          call PT_find_range(eos_handle, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               P, log10P, which_other, other_value, doing_P, &
               log10T1, log10T2, other1, other2, ierr)
          write(*,*) "PT_find_range results for log10P = ", log10P
          write(*,*) "log10Trange: ", log10T1, log10T2
          write(*,*) "other_range: ", other1, other2
          write(*,*) "ierr = ", ierr
          write(*,*)
       end do
    endif
    
  end subroutine test_PT_find_range

  subroutine output_Prange_over_T
    use base_eos_mod
    use mixeos_mod

    class(base_eos), pointer :: eos_ptr
    integer :: i, N, ierr
    real(dp) :: minlog10T, maxlog10T, dlog10T, log10P1, log10P2, log10Rho1, log10Rho2
    character(len=256) :: filename
    
    integer :: doing_P
    
    !! Write out a relationship for the eos(1)
    N = 100
    minlog10T = 2.0
    maxlog10T = 8.
    dlog10T = (maxlog10T - minlog10T) / (N - 1d0)
    filename = "eoslogPrange.dat"
    call mixeos_get_eos(1, eos_ptr, ierr)
    if(ierr .eq. 0) then
       open(unit=19,file=trim(filename), status='replace', action='write', form='formatted')
      
       do i=1,N
          log10T = minlog10T + (i-1) * dlog10T
          call eos_ptr%PT_get_PRhorange(log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
          write(19,'(3e14.6)') log10T, log10P1, log10P2
          
       enddo
       close(unit=19)
       
    else
       write(*,*) "mixeos_list(1) doesn't seem to be setup!!!??"
    endif
    
    !! Write out a relationship for the entire mixeos
    filename = "mixeoslogPrange.dat"
    open(unit=19,file=trim(filename), status='replace', action='write', form='formatted')
    
    doing_P = 0
    do i=1,N
       log10T = minlog10T + (i-1) * dlog10T
       T = 1d1**log10T
       call get_pressure_range(T, log10T, log10P1, log10P2, ierr)
!       call PT_find_domain(T, log10T, doing_P, log10P1, log10P2, ierr)
       write(19,'(3e14.6)') log10T, log10P1, log10P2
    end do
    close(unit=19)
    
  end subroutine output_Prange_over_T

  ! Bill requested to compare d(grad_ad)/d(lnT) from your eos to (grad_ad1 - grad_ad1)/(lnT1 - lnT2)  
  subroutine test_derivs
    use eos_def
    
    real(dp) :: minlogRho, maxlogRho, minlogT, maxlogT, dlogRho, dlogT, &
         delta_log10X, other1, other2, lnX1, lnX2, dother_dlnX, dother_dlnX_num, &
         other_value
    integer :: j
    logical, parameter :: test_Rho = .true. !! Testing Rho derivatives
    integer :: which_other = i_grad_ad
    

    minlogRho = -3d0
    maxlogRho = -2d0
    minlogT = 4d0
    maxlogT = 5d0
    
    Xsize = 2
    Ysize = 2
    
    dlogRho = (maxlogRho - minlogRho) / Xsize
    dlogT = (maxlogT - minlogT) / Ysize

    delta_log10X = 1d-2
    
    do i=1,Xsize
       do j=1, Ysize
          log10Rho = minlogRho + (i-1) * dlogRho
          log10T = minlogT + (j-1) * dlogT 


          if(test_Rho) then
             log10Rho = log10Rho - delta_log10X
          else
             log10T = log10T - delta_log10X
          end if
          
          Rho = 1d1**log10Rho
          T = 1d1**log10T
          
          !! Get grad_ad at (log10X - delta_log10X)
          if(test_Rho) then
             lnX1 = log(Rho)
          else
             lnX1 = log(T)
          endif
          call mixeos_get( &
               id, k, eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          other1 = res_output(which_other)
          
          !! the derivative of grad_ad at log10T
          if(test_Rho) then
             log10Rho = log10Rho + delta_log10X
             Rho = 1d1**log10Rho
          else
             log10T = log10T + delta_log10X 
             T = 1d1**log10T
          end if
          call mixeos_get( &
               id, k, eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          other_value = res_output(which_other)
          if(test_Rho) then
             dother_dlnX = d_dlnT_c_Rho_output(which_other)
          else
             dother_dlnX = d_dlnRho_c_T_output(which_other)
          end if
          !! get grad_ad at (log10T + delta_log10T)
          if(test_Rho) then
             log10Rho = log10Rho + delta_log10X
             Rho = 1d1**log10Rho
             lnX2 = log(Rho)
          else
             log10T = log10T + delta_log10X 
             T = 1d1**log10T
             lnX2 = log(T)
          end if
          call mixeos_get( &
               id, k, eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          
          other2 = res_output(which_other)
          
          dother_dlnX_num = (other2 - other1) / (lnX2 - lnX1)
          
          write(*,*) "adiabatic gradient at"
          write(*,*) "log10Rho = ", log10Rho
          write(*,*) "log10T   = ", log10T
          if(test_Rho) then
             write(*,*) "testing dlnRho"
          else
             write(*,*) "testing dlnT"
          endif
          write(*,*) "which_other = ", which_other
          write(*,*) "other value = ", other_value
          write(*,*) "dother_dlnX = ", dother_dlnX
          write(*,*) "numerical     = ", dother_dlnX_num
          write(*,*) "rel diff      = ", (dother_dlnX - dother_dlnX_num) / dother_dlnX

          
       enddo
    enddo

  end subroutine test_derivs

  
  !! Check the relationships between  i_lnPgas 
  !!               i_chiRho == dlnP_dlnRho
  !!               i_chiT   == dlnP_dlnT
  !! Perform two cuts
  !!  1. Plot lnP (lnRho), ndiff lnP (lnRho), dlnPdlnRho(lnRho), i_chiRho

  !!  2. Plot lnP (lnT), ndiff lnP (lnT), dlnPdlnT(lnT), i_chiT
  
  subroutine test_lnPconsistency
    use eos_def
    use eos_lib

    character(len=256) :: filename
    integer :: NPTS = 100
    real(dp) :: delta_logRho = 1d-1, delta_logT = 1d-1
    real(dp) :: dlogRho, minlogRho, maxlogRho, &
                dlogT, minlogT, maxlogT
    real(dp) :: lnPmid, dlnPdlnRho_ndiff, dlnPdlnRho_mat1, dlnPdlnRho_mat2, &
         d2lnPdlnRho2_ndiff, d2lnPdlnRho2_mat2, &
         lnP1, lnP2, dlnPdlnRho_1, dlnPdlnRho_2
    
    real(dp) :: dlnPdlnT_ndiff, dlnPdlnT_mat1, dlnPdlnT_mat3, &
         d2lnPdlnT2_ndiff, d2lnPdlnT2_mat3, &
         dlnPdlnT_1, dlnPdlnT_2, &
         d2lnPdlnRhodlnT_ndiffT, d2lnPdlnRhodlnT_mat2, d2lnPdlnRhodlnT_mat3

    
    !! Stage 1
    minlogRho = -3d0
    maxlogRho = -2d0
    log10T = 4d0
    T = 1d1**log10T
    lnT = log(T)
    dlogRho = (maxlogRho - minlogRho) / (NPTS - 1)
    
    filename = "test_lnP_lnRho.dat"
    open(unit=19,file=trim(filename),status='replace', &
         action='write', form='formatted')
    do i=1,NPTS
       log10Rho = minlogRho + (i-1) * dlogRho - delta_logRho
       Rho = 1d1**log10Rho
       
       !! logRho - delta_logRho
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnP1 = res_output(i_lnPgas)
       dlnPdlnRho_1 = d_dlnRho_c_T_output(i_lnPgas)       
       
       !! midpoint
       log10Rho = log10Rho + delta_logRho
       Rho = 1d1**log10Rho
       lnRho = log(Rho)
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnPmid = res_output(i_lnPgas)
       dlnPdlnRho_mat1 = res_output(i_chiRho)
       dlnPdlnRho_mat2 = d_dlnRho_c_T_output(i_lnPgas)
       d2lnPdlnRho2_mat2 = d_dlnRho_c_T_output(i_chiRho)

       !! logRho + delta_logRho
       log10Rho = log10Rho + delta_logRho
       Rho = 1d1**log10Rho
       lnRho = log(Rho)
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnP2 = res_output(i_lnPgas)
       dlnPdlnRho_2 = d_dlnRho_c_T_output(i_lnPgas)
       
       dlnPdlnRho_ndiff = (lnP2 - lnP1) / (delta_logRho * 2d0 * ln10)
       d2lnPdlnRho2_ndiff = (dlnPdlnRho_2 - dlnPdlnRho_1) / (delta_logRho * 2d0 * ln10)
       write(19,'(8e14.6)') log10Rho, lnRho, &
            lnPmid, &
            dlnPdlnRho_mat1, dlnPdlnRho_mat2, dlnPdlnRho_ndiff, &
            d2lnPdlnRho2_mat2, d2lnPdlnRho2_ndiff
    end do       
    close(unit=19)
    
    !! Stage 2
    minlogT = 4d0
    maxlogT = 5d0
    log10Rho = -3d0
    Rho = 1d1**log10Rho
    lnRho = log(Rho)
    dlogT = (maxlogT - minlogT) / (NPTS - 1)
    
    filename = "test_lnP_lnT.dat"
    open(unit=19,file=trim(filename),status='replace', &
         action='write', form='formatted')
    do i=1,NPTS
       log10T = minlogT + (i-1) * dlogT - delta_logT
       T = 1d1**log10T
       
       !! logT - delta_logT
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnP1 = res_output(i_lnPgas)
       dlnPdlnT_1 = d_dlnT_c_Rho_output(i_lnPgas)     
       dlnPdlnRho_1 = d_dlnRho_c_T_output(i_lnPgas)
       
       !! midpoint
       log10T = log10T + delta_logT
       T = 1d1**log10T
       lnT = log(T)
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnPmid = res_output(i_lnPgas)
       dlnPdlnT_mat1 = res_output(i_chiT)
       dlnPdlnT_mat3 = d_dlnT_c_Rho_output(i_lnPgas)
       d2lnPdlnT2_mat3 = d_dlnT_c_Rho_output(i_chiT)
       d2lnPdlnRhodlnT_mat2 = d_dlnRho_c_T_output(i_chiT)
       d2lnPdlnRhodlnT_mat3 = d_dlnT_c_Rho_output(i_chiRho)

       !! logT + delta_logT
       log10T = log10T + delta_logT
       T = 1d1**log10T
       lnT = log(T)
       call mixeos_get( &
            id, k, &
!       call eosDT_get( &
            eos_handle, Z, X, abar, zbar, & 
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, & 
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
       lnP2 = res_output(i_lnPgas)
       dlnPdlnT_2 = d_dlnT_c_Rho_output(i_lnPgas)
       dlnPdlnRho_2 = d_dlnRho_c_T_output(i_lnPgas)
       
       dlnPdlnT_ndiff = (lnP2 - lnP1) / (delta_logT * 2d0 * ln10)
       d2lnPdlnT2_ndiff = (dlnPdlnT_2 - dlnPdlnT_1) / (delta_logT * 2d0 * ln10)
       d2lnPdlnRhodlnT_ndiffT = (dlnPdlnRho_2 - dlnPdlnRho_1) / (delta_logT * 2d0 * ln10)
       write(19,'(11e14.6)') log10T, lnT, &
            lnPmid, &
            dlnPdlnT_mat1, dlnPdlnT_mat3, dlnPdlnT_ndiff, &
            d2lnPdlnT2_mat3, d2lnPdlnT2_ndiff, &
            d2lnPdlnRhodlnT_mat2, d2lnPdlnRhodlnT_mat3, d2lnPdlnRhodlnT_ndiffT
    end do       
    close(unit=19)

  end subroutine test_lnPconsistency
  
  !! How self consistent is the EOS?
  !! if there is an inconsistency, which variables should we pull from to create 
  !!  (to the extent possible) a thermodynamically self consistent eos
  subroutine test_lnSconsistency
    use eos_def
    use eos_lib

    character(len=256) :: filename
    integer :: NPTS = 100
    real(dp) :: delta_logRho = 1d-1, delta_logT = 1d-1
    real(dp) :: dlogRho, minlogRho, maxlogRho, &
                dlogT, minlogT, maxlogT
    real(dp) :: lnSmid, dlnSdlnRho_ndiff, dlnSdlnRho_mat1, dlnSdlnRho_mat2, &
         d2lnSdlnRho2_ndiff, d2lnSdlnRho2_mat2, &
         lnS1, lnS2, dlnSdlnRho_1, dlnSdlnRho_2
    
    real(dp) :: dlnSdlnT_ndiff, dlnSdlnT_mat1, dlnSdlnT_mat3, &
         d2lnSdlnT2_ndiff, d2lnSdlnT2_mat3, &
         dlnSdlnT_1, dlnSdlnT_2, &
         d2lnSdlnRhodlnT_ndiffT, d2lnSdlnRhodlnT_mat2, d2lnSdlnRhodlnT_mat3

    
  end subroutine test_lnSconsistency
  
  subroutine test_h_consistency
    use eos_def
    use eos_lib
    use thermo

    character(len=256) :: filename
    real(dp) :: minlogT, maxlogT, dlogT
    integer :: NPTS = 100
    real(dp) :: lnPgas_value, logRho_result, &
         logRho_tol, other_tol, logRho_bnd1, logRho_bnd2, &
         other_at_bnd1, other_at_bnd2, dh_dT_cP, h_val
    integer :: which_other = i_lnPgas
    integer :: eos_calls, max_iter
    real(dp) :: helm_res(num_helm_results)
    real(dp) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs), &
         Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), &
         lnPVect2(num_derivs), lnEVect2(num_derivs), lnSVect2(num_derivs)

    log10Rho = -3d0
    Rho = 1d1**log10Rho
    log10T = 4d0
    T = 1d1**log10T
    
    call eosDT_get( &
         eos_handle, Z, X, abar, zbar, & 
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, & 
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
    lnPgas_value = res_output(i_lnPgas)
    
    minlogT = 4d0
    maxlogT = 5d0
    dlogT = (maxlogT - minlogT) / (NPTS - 1)
    
    logRho_tol = 1d-10
    other_tol = 1d-10
    
    filename = "h_dT.dat"
    open(unit=19,file=trim(filename),status='replace', &
         action='write', form='formatted')
    
    do i=1,NPTS
       logRho_bnd1 = -5d0
       logRho_bnd2 = -2d0
       other_at_bnd1 = arg_not_provided
       other_at_bnd2 = arg_not_provided
       
       log10T = minlogT + (i-1) * dlogT
       T = 1d1**log10T
       max_iter = 100
       
       call eosDT_get_Rho( &
            eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10T, which_other, lnPgas_value, &
            logRho_tol, other_tol, max_iter, log10Rho, &
            logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
            logRho_result, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
            helm_res, eos_calls, ierr)
       dh_dT_cP = res_output(i_Cp)
       
       E = exp(res_output(i_lnE))
       P = exp(res_output(i_lnPgas))
       Rho = 1d1**logRho_result
       lnRho = log(Rho)
       lnT = log(T)
       
       h_val = E + P/Rho
       
       call convRES_lnderivs(res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
            lnRho, lnT, lnPvect2, lnEvect2, lnSvect2)
       call convlnderivs_RES(lnRho, lnT, lnPvect2, lnEvect2, lnSvect2, &
            res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy)
       
       write(19,'(6e14.6)') log10T, T, h_val, dh_dT_cP, res_copy(i_Cp), (dh_dT_cP - res_copy(i_Cp)) / dh_dT_cP
    enddo
    close(unit=19)
    
    
  end subroutine test_h_consistency

  !! Use get the RhoT function to pull a 1D vector of which_other over either Rho or T
  !! Print out 
  !!    log10X lnX other_value dother_value_dlnX
  subroutine DTpull_1dvect
    use eos_def
        
    real(dp) :: minlogX, maxlogX, dlogX, log10Y, log10X, lnX, other1, other2
    
    logical :: test_Rho = .true. !! Testing Rho derivatives
    integer :: which_other = i_grad_ad
    character(len=256) :: filename
    integer, parameter :: XSZ = 100
    real(dp), parameter :: delta_logX = 1d-2
    real(dp) :: log10Xvect(XSZ), lnXvect(XSZ), other_vect(XSZ), dother_vect_dlnX(XSZ), ndiff_other_dlnX(XSZ)
    integer :: loop_var
    
!    filename = "test_grad_ad.dat"
    
    do loop_var = 1,2
       if(loop_var .eq. 1) then
          test_Rho = .true.
       else
          test_Rho = .false.
       endif

       if(test_Rho) then
          minlogX = -3d0
          maxlogX = -2d0
          log10Y = 4.5
          log10T = log10Y
          T = 1d1**log10T
          filename = "Test_1D_spacial/test_grad_ad_T.dat"
       else
          minlogX = 3d0
          maxlogX = 6d0
          log10Y = -2d0
          log10Rho = log10Y
          Rho = 1d1**log10Rho
          filename = "Test_1D_spacial/test_grad_ad_Rho.dat"
       end if
       
       dlogX = (maxlogX - minlogX) / (XSZ - 1)
       
       open(unit=19,file=trim(filename),status='replace',action='write',form='formatted')
       do i=1,XSZ
          if(test_Rho) then
             log10Rho = minlogX + (i-1) * dlogX - delta_logX
             Rho = 1d1**log10Rho
             log10Xvect(i) = log10Rho
             lnXvect(i) = log(Rho)
          else
             log10T = minlogX + (i-1) * dlogX - delta_logX
             T = 1d1**log10T
             log10Xvect(i) = log10T
             lnXvect(i) = log(T)
          end if
          
          call mixeos_get( &
               !       call eosDT_get( &
               id, k, &
               eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          other1 = res_output(which_other)
          
          if(test_Rho) then
             log10Rho = log10Rho + delta_logX
             Rho = 1d1**log10Rho
          else
             log10T = log10T + delta_logX
             T = 1d1**log10T
          end if
          
          call mixeos_get( &
               !       call eosDT_get( &
               id, k, &
               eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          other_vect(i) = res_output(which_other)
          if(test_rho) then
             dother_vect_dlnX(i) = d_dlnRho_c_T_output(which_other)
          else
             dother_vect_dlnX(i) = d_dlnT_c_Rho_output(which_other)
          end if
          
          if(test_Rho) then
             log10Rho = log10Rho + delta_logX
             Rho = 1d1**log10Rho
          else
             log10T = log10T + delta_logX
             T = 1d1**log10T
          end if
          call mixeos_get( &
               !       call eosDT_get( &
               id, k, &
               eos_handle, Z, X, abar, zbar, & 
               species, chem_id, net_iso, xa, &
               Rho, log10Rho, T, log10T, & 
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
          other2 = res_output(which_other)
          
          ndiff_other_dlnX(i) = (other2 - other1) / (2d0 * delta_logX * ln10)
          
          write(19,'(5e14.6)') log10Xvect(i), lnXvect(i), other_vect(i), dother_vect_dlnX(i), ndiff_other_dlnX(i)
          write(*,*) log10Xvect(i), other_vect(i), dother_vect_dlnX(i), ndiff_other_dlnX(i)
       enddo
       close(unit=19)
    end do
  end subroutine DTpull_1dvect

  subroutine test_F
    use eos_def
    use eos_lib
    use thermo
    use interp1dfunction_mod
    
    character(len=256) :: filename
    integer, parameter :: XSZ = 100
!    integer, parameter :: num_Fderivs = 10

    real(dp) :: minlogRho, maxlogRho, dlogRho, delta_logRho, Rho1, Rho3
    real(dp) :: Fvect1(num_Fderivs), Fvect2(num_Fderivs), Fvect3(num_Fderivs)
    real(dp) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), &
                lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs)
    real(dp) :: tmp_nderiv
    real(dp), target :: Fmat(XSZ, num_Fderivs) !! Order it in this way to load into interpolator

    !! Ideally I would like to make smoother numeric derivatives
    real(dp), target :: log10Rho_vect(XSZ), Rho_vect(XSZ)
    real(dp), pointer, dimension(:) :: Rho_vect_ptr, log10Rho_vect_ptr, Fmat_ptr
    type(interp1dfunction) :: F_val_interp, dF_dRho_interp, dF_dT_interp, &
         d2F_dRho2_interp, d2F_dRhodT_interp, d2F_dT2_interp
    real(dp) :: dF_dRho_nd, d2F_dRho2_nd, d2F_dRhodT_nd, &
         d3F_dRho3_nd, d3F_dRho2dT_nd, d3F_dRhodT2_nd, &
         F_in, dF_dRho_in, dF_dT_in, d2F_dRho2_in, d2F_dRhodT_in, d2F_dT2_in
    
    minlogRho = -3d0
    maxlogRho = -2d0
    dlogRho = (maxlogRho - minlogRho) / (XSZ - 1)
    delta_logRho = 1d-2
    
    log10T = 4d0
    T = 1d1**log10T
    lnT = log(T)
    
    filename = "F_dRho.dat"
    open(unit=19,file=trim(filename),status='replace',action='write',form='formatted')
    !! Check if derivatives are self consistent in Rho
    do i=1,XSZ
       log10Rho = minlogRho + (i-1) * dlogRho - delta_logRho
       Rho = 1d1**log10Rho
       Rho1 = Rho
       call Get_Feos(Rho, log10Rho, T, log10T, Fvect1)
       
       log10Rho = log10Rho + delta_logRho
       Rho = 1d1**log10Rho
       lnRho = log(Rho)
       call Get_Feos(Rho, log10Rho, T, log10T, Fvect2)

       log10Rho_vect(i) = log10Rho
       Rho_vect(i) = Rho
       Fmat(i,1:num_Fderivs) = Fvect2(1:num_Fderivs)


       call ConvF_derivs(Rho, T, Fvect2, Pvect, Evect, Svect)
       call convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, lnPvect, lnEvect, lnSvect)
       call convlnderivs_RES(lnRho, lnT, lnPvect, lnEvect, lnSvect, &
            res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy)
       write(*,*) "Rho = ", Rho, " T = ", T
       write(*,*) "Res 1: ", res_output
       write(*,*) "Res 2: ", res_copy
       
       write(*,*) "d_dlnRho_c_T_output: ", d_dlnRho_c_T_output
       write(*,*) "d_dlnRho_c_T_copy:   ", d_dlnRho_c_T_copy

       write(*,*) "d_dlnT_c_Rho_output: ", d_dlnT_c_Rho_output
       write(*,*) "d_dlnT_c_Rho_copy:   ", d_dlnRho_c_T_copy

       log10Rho = log10Rho + delta_logRho
       Rho = 1d1**log10Rho
       Rho3 = Rho
       call Get_Feos(Rho, log10Rho, T, log10T, Fvect3)
       
       log10Rho = minlogRho + (i-1) * dlogRho
       
       write(19,'(21e14.6)') log10Rho, Fvect2, (Fvect3 - Fvect1) / (Rho3 - Rho1)
       
       tmp_nderiv = (Fvect3(i_val) - Fvect1(i_val)) / (Rho3 - Rho1)
       write(*,*) "dF/dRho: ", Fvect2(i_dRho), tmp_nderiv, Fvect2(i_dRho) / tmp_nderiv - 1d0
       write(*,*) "d2F/dRho2: ", Fvect2(i_dRho2), (Fvect3(i_dRho) - Fvect1(i_dRho)) / (Rho3 - Rho1)
       write(*,*) "d2F/dRhodT: ", Fvect2(i_dRhodT), (Fvect3(i_dT) - Fvect1(i_dT)) / (Rho3 - Rho1)
       write(*,*) "d3F/dRho3: ", Fvect2(i_dRho3), (Fvect3(i_dRho2) - Fvect1(i_dRho2)) / (Rho3 - Rho1)
       write(*,*) "d3F/dRho2dT: ", Fvect2(i_dRho2dT), (Fvect3(i_dRhodT) - Fvect1(i_dRhodT)) / (Rho3 - Rho1)
       write(*,*) "d3F/dRhodT2: ", Fvect2(i_dRhodT2), (Fvect3(i_dT2) - Fvect1(i_dT2)) / (Rho3 - Rho1)
    enddo
    close(unit=19)
    
    Rho_vect_ptr => Rho_vect(1:XSZ)
    log10Rho_vect_ptr => log10Rho_vect(1:XSZ)
    
    write(*,*) Fmat(:,i_val)

    !! Keep the legacy code above for now
    !! Try to get a cleaner numeric derivative using Fmat
    Fmat_ptr => Fmat(1:XSZ,i_val)
    write(*,*) Fmat_ptr
    call F_val_interp%Construct(log10Rho_vect_ptr,      Fmat_ptr,     XSZ, ierr)

    Fmat_ptr => Fmat(1:XSZ,i_dRho)
    call dF_dRho_interp%Construct(log10Rho_vect_ptr,    Fmat_ptr,     XSZ, ierr)

    Fmat_ptr => Fmat(1:XSZ,i_dT)
    call dF_dT_interp%Construct(log10Rho_vect_ptr,      Fmat_ptr,     XSZ, ierr)

    Fmat_ptr => Fmat(1:XSZ,i_dRho2)
    call d2F_dRho2_interp%Construct(log10Rho_vect_ptr,  Fmat_ptr,     XSZ, ierr)

    Fmat_ptr => Fmat(1:XSZ,i_dRhodT)
    call d2F_dRhodT_interp%Construct(log10Rho_vect_ptr, Fmat_ptr,     XSZ, ierr)

    Fmat_ptr => Fmat(1:XSZ,i_dT2)
    call d2F_dT2_interp%Construct(log10Rho_vect_ptr,    Fmat_ptr,     XSZ, ierr)

    filename = "F_dRho_nd.dat"
    write(*,*) "Opening file: ", filename
    open(unit=19,file=trim(filename),status='replace',action='write',form='formatted')

    do i=1,XSZ
       log10Rho = minlogRho + (i-1) * dlogRho
       Rho = 1d1**log10Rho
       
       write(*,*) "i = ", i
       call F_val_interp%interp(log10Rho, F_in, ierr)

       call dF_dRho_interp%interp(log10Rho, dF_dRho_in, ierr)

       call dF_dT_interp%interp(log10Rho, dF_dT_in, ierr)
       
       call d2F_dRho2_interp%interp(log10Rho, d2F_dRho2_in,ierr)
       
       call d2F_dRhodT_interp%interp(log10Rho, d2F_dRhodT_in,ierr)
       
       call d2F_dT2_interp%interp(log10Rho, d2F_dT2_in,ierr)

       dF_dRho_nd     = (1 / (ln10 * Rho)) * F_val_interp%deriv(log10Rho, ierr)

       d2F_dRho2_nd   = (1 / (ln10 * Rho)) * dF_dRho_interp%deriv(log10Rho, ierr)
       d2F_dRhodT_nd  = (1 / (ln10 * Rho)) * dF_dT_interp%deriv(log10Rho, ierr)
       d3F_dRho3_nd   = (1 / (ln10 * Rho)) * d2F_dRho2_interp%deriv(log10Rho, ierr)
       d3F_dRho2dT_nd = (1 / (ln10 * Rho)) * d2F_dRhodT_interp%deriv(log10Rho, ierr)
       d3F_dRhodT2_nd = (1 / (ln10 * Rho)) * d2F_dT2_interp%deriv(log10Rho, ierr)
       
       if(ierr .ne. 0) then
          write(*,*) "ierr = ", ierr
       end if
       
       write(19,'(13e14.6)') log10Rho, dF_dRho_nd, &
            d2F_dRho2_nd, d2F_dRhodT_nd, &
            d3F_dRho3_nd, d3F_dRho2dT_nd, d3F_dRhodT2_nd, &
            F_in, dF_dRho_in, dF_dT_in, d2F_dRho2_in, d2F_dT2_in, d2F_dRhodT_in
       
    enddo
    close(unit=19)
    write(*,*) "Wrote to file: ", trim(filename)
    
    call F_val_interp%Destroy()
    call dF_dRho_interp%Destroy()
    call dF_dT_interp%Destroy()
    call d2F_dRho2_interp%Destroy()
    call d2F_dRhodT_interp%Destroy()
    call d2F_dT2_interp%Destroy()
  contains
    subroutine Get_Feos(Rho, log10Rho, T, log10T, Fvect)
      real(dp), intent(in) :: Rho, log10Rho, T, log10T
      real(dp), intent(out) :: Fvect(num_Fderivs)
      
!      real(dp) :: res_tmp(num_eos_basic_results), &
!           d_dlnRho_tmp(num_eos_basic_results), &
!           d_dlnT_tmp(num_eos_basic_results)
      
!      call mixeos_get( &
      call eosDT_get( &
!           id, k, &
           eos_handle, Z, X, abar, zbar, & 
           species, chem_id, net_iso, xa, &
           Rho, log10Rho, T, log10T, & 
           res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
      call ConvRes_F(Rho, log10Rho, T, log10T, &
           res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, Fvect)
      
    end subroutine Get_Feos

  end subroutine test_F

  !! Do we believe that the thermodynamic derivative functions are working correctly?
  subroutine test_thermo_deriv
    use thermo
    
    real(dp) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs), &
         Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), &
         lnPVect2(num_derivs), lnEVect2(num_derivs), lnSVect2(num_derivs)
    logical :: test_cycle = .true.

    log10Rho = -2d0
    log10T = 4d0
    Rho = 1d1**log10Rho
    T = 1d1**log10T
    lnRho = log(Rho)
    lnT = log(T)
    
!        call mixeos_get( id, k, &
    call eosDT_get( &
         eos_handle, Z, X, abar, zbar, & 
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, & 
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)    
    
    call convRES_lnderivs(res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
         lnRho, lnT, lnPvect, lnEvect, lnSvect)
    
!    call convlnDerivsDerivs(lnRho, lnT, lnPvect, lnEvect, lnSVect, &
!         Pvect, Evect, Svect)
    
!    call convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, &
!         lnPvect2, lnEvect2, lnSVect2)

    lnPvect2 = lnPvect
    lnEvect2 = lnEvect
    lnSvect2 = lnSvect

    write(*,*) "components of dgrad_ad_dlnRho == -grad_ad**2 * ["
    write(*,*) "term1: ", -lnSvect(i_dlnRhodlnT) * lnPvect(i_dlnRho) / lnSvect(i_dlnRho)
    write(*,*) "term2: ", -lnSvect(i_dlnT) * lnPvect(i_dlnRho2) / lnSvect(i_dlnRho)
    write(*,*) "term3: ", lnSvect(i_dlnT) * lnPvect(i_dlnRho) * lnSvect(i_dlnRho2) / lnSvect(i_dlnRho)**2d0
    write(*,*) "term4: ", lnPvect(i_dlnRhodlnT)
    write(*,*) "]"
    call convlnderivs_RES(lnRho, lnT, lnPVect2, lnEVect2, lnSVect2, &
         res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy)

    write(*,*) "res output: ", res_output
    write(*,*) "res copy:   ", res_copy
    write(*,*) "rel diff:   ", (res_output - res_copy) / res_output

    write(*,*) "d_dlnRho_c_T_output: ", d_dlnRho_c_T_output
    write(*,*) "d_dlnRho_c_T_copy:   ", d_dlnRho_c_T_copy
    write(*,*) "rel diff:            ", (d_dlnRho_c_T_output - d_dlnRho_c_T_copy) / d_dlnRho_c_T_output

    write(*,*) "d_dlnT_c_Rho_output: ", d_dlnT_c_Rho_output
    write(*,*) "d_dlnT_c_Rho_copy:   ", d_dlnT_c_Rho_copy
    write(*,*) "rel diff:            ", (d_dlnT_c_Rho_output - d_dlnT_c_Rho_copy) / d_dlnT_c_Rho_output


    if(test_cycle) then
       call convRES_lnderivs(res_copy, d_dlnRho_c_T_copy, d_dlnT_c_Rho_copy, &
            lnRho, lnT, lnPvect2, lnEvect2, lnSvect2)
       call convlnderivs_RES(lnRho, lnT, lnPvect2, lnEvect2, lnSvect2, &
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output)
       
       
       write(*,*) "res cycle: ", res_output
       write(*,*) "res copy:   ", res_copy
       write(*,*) "rel diff:   ", (res_output - res_copy) / res_output
       
       write(*,*) "d_dlnRho_c_T_cycle: ", d_dlnRho_c_T_output
       write(*,*) "d_dlnRho_c_T_copy:   ", d_dlnRho_c_T_copy
       write(*,*) "rel diff:            ", (d_dlnRho_c_T_output - d_dlnRho_c_T_copy) / d_dlnRho_c_T_output
       
       write(*,*) "d_dlnT_c_Rho_cycle: ", d_dlnT_c_Rho_output
       write(*,*) "d_dlnT_c_Rho_copy:   ", d_dlnT_c_Rho_copy
       write(*,*) "rel diff:            ", (d_dlnT_c_Rho_output - d_dlnT_c_Rho_copy) / d_dlnT_c_Rho_output
    endif
  end subroutine test_thermo_deriv


  subroutine test_get_rho
    use eos_def
    use const_def, only: arg_not_provided
    implicit none

    real(dp) :: other_value
    integer :: which_other

    real(dp) :: logRho_tol = 1d-4, other_tol = 1d-4
    integer :: max_iter=100
    real(dp) :: logRho_guess = arg_not_provided
    real(dp) :: logRho_bnd1 = arg_not_provided
    real(dp) :: logRho_bnd2 = arg_not_provided
    real(dp) :: other_at_bnd1 = arg_not_provided
    real(dp) :: other_at_bnd2 = arg_not_provided
    integer :: eos_calls
    real(dp) :: logRho_output

    log10P = 10.
    log10T = 5.3
    P = 10d0**log10P
    T = 10d0**log10T

    logRho_tol = 1d-4
    other_tol = 1d-4  
    which_other = i_lnPgas
    other_value = log(P)
    logRho_guess = arg_not_provided
    logRho_bnd1 = arg_not_provided
    logRho_bnd2 = arg_not_provided
    other_at_bnd1 = arg_not_provided
    other_at_bnd2 = arg_not_provided
    max_iter = 100


    write(*,*) "Stepping into mixeos_get_rho"
    logRho_guess = 0d0
    call mixeos_get_rho( &
         id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, &
         logRho_tol, other_tol, max_iter, logRho_guess,  &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
         eos_calls, ierr)
    
    log10Rho = logRho_output
    Rho = 1d1**log10Rho
    write(*,*) "ierr = ", ierr
    write(*,*) "Rho = ", Rho
    write(*,*) "(log10Rho = ", log10Rho, ", log10T = ", log10T, ")"
    call print_res(res_output)
    
    other_value = res_output(i_lnS)
    which_other = i_lnS
    call mixeos_get_rho( &
         id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, &
         logRho_tol, other_tol, max_iter, logRho_guess,  &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
         eos_calls, ierr)
    
    log10Rho = logRho_output
    Rho = 1d1**log10Rho
    write(*,*) "ierr = ", ierr
    write(*,*) "Rho = ", Rho
    write(*,*) "(log10Rho = ", log10Rho, ", log10T = ", log10T, ")"
    call print_res(res_output)
    
    other_value = res_output(i_lnE)
    which_other = i_lnE
    call   mixeos_get_rho( &
         id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, &
         logRho_tol, other_tol, max_iter, logRho_guess,  &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
         eos_calls, ierr)
    
    log10Rho = logRho_output
    Rho = 1d1**log10Rho
    write(*,*) "ierr = ", ierr
    write(*,*) "Rho = ", Rho
    write(*,*) "(log10Rho = ", log10Rho, ", log10T = ", log10T, ")"
    call print_res(res_output)
    
    call mixeos_get(&
         id, k, eos_handle, Z, X, abar, zbar, & 
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, & 
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

    which_other = i_lnS
    other_value = res_output(i_lnS)
    write(*,*) "(lnS = ", other_value, ", log10T = ", log10T, ")"

    call mixeos_get_rho( &
         id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, &
         logRho_tol, other_tol, max_iter, logRho_guess,  &
         logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
         logRho_output, res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, &
         eos_calls, &
         ierr)
    
    write(*,*) "ierr = ", ierr
    call print_res(res_output)
    
  end subroutine test_get_rho

  !! Test the PT_get_T function
  !! confirm that this function works for which_other = i_lnE and i_lnS
  subroutine test_PT_get_T
    use eos_def
    use const_def, only: arg_not_provided
    implicit none

    real(dp) :: other_value
    integer :: which_other
    
    real(dp) :: log10T_tol, other_tol, logRho_guess, &
         log10T_bnd1, log10T_bnd2, other_at_bnd1, other_at_bnd2, &
         log10T_result, log10T_guess, dlnRho_dlnP_c_T, dlnRho_dlnT_c_P
    integer :: eos_calls, max_iter=100

    log10P = 14.
    log10T = 5.3
    P = 10d0**log10P
    T = 10d0**log10T

    log10T_tol = 1d-4
    other_tol = 1d-4  
    which_other = i_lnPgas
    other_value = log(P)
    log10T_guess = arg_not_provided
    log10T_bnd1 = arg_not_provided
    log10T_bnd2 = arg_not_provided
    other_at_bnd1 = arg_not_provided
    other_at_bnd2 = arg_not_provided
    max_iter = 100

    call mixeosPT_get(id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, &
         Rho, log10Rho, dlnRho_dlnP_c_T, dlnRho_dlnT_c_P, &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

    write(*,*) "Rho = ", Rho
    write(*,*) "log10Rho = ", log10Rho
    call print_res(res_output)    


    other_value = res_output(i_lnE)
    which_other = i_lnE
    call mixeosPT_get_T(id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10P, which_other, other_value, &
         log10T_tol, other_tol, max_iter, log10T_guess, &
         log10T_bnd1, log10T_bnd2, other_at_bnd1, other_at_bnd2, &
         log10T_result, Rho, log10Rho, dlnRho_dlnP_c_T, dlnRho_dlnT_c_P, &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, eos_calls, ierr)

    write(*,*) "ierr = ", ierr
    write(*,*) "log10T = ", log10T_result
    write(*,*) "Rho = ", Rho
    write(*,*) "log10Rho = ", log10Rho
    
    call print_res(res_output)
    
    other_value = res_output(i_lnS)
    which_other = i_lnS
    call mixeosPT_get_T(id, k, eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10P, which_other, other_value, &
         log10T_tol, other_tol, max_iter, log10T_guess, &
         log10T_bnd1, log10T_bnd2, other_at_bnd1, other_at_bnd2, &
         log10T_result, Rho, log10Rho, dlnRho_dlnP_c_T, dlnRho_dlnT_c_P, &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, eos_calls, ierr)

    write(*,*) "ierr = ", ierr
    write(*,*) "log10T = ", log10T_result
    write(*,*) "Rho = ", Rho
    write(*,*) "log10Rho = ", log10Rho
    
    call print_res(res_output)
    
  end subroutine test_PT_get_T

  subroutine print_res(res)
    use eos_lib, only: eos_result_names
    real(dp), intent(in) :: res(num_eos_basic_results)
    character (len=8) :: names(num_eos_basic_results)
    integer :: i
    call eos_result_names(names) 
    do i=1,num_eos_basic_results
       write(*,*) names(i), " = ", res(i)
    enddo
  end subroutine print_res

  subroutine test_build_model
    use const_def, only: pi, standard_cgrav
    use eos_def
    integer, parameter :: NCELL = 200
    real(dp) :: PGrid(NCELL), TGrid(NCELL), RhoGrid(NCELL), &
         MassGrid(NCELL), dMGrid(NCELL), RadiusGrid(NCELL), lnSGrid(NCELL)
    real(dp) :: Pouter = 1d7, &
         Mp = 2d33, & ! Mass of Neptune
         RhoGuess = 0.5d0, &
         TGuess = 1d9, &
         lnS_model = 20.0d0, &
         CoreMass, &
         CoreDensity = 1d0, &
         CoreRadius
    
    character (len=256) :: filename
    integer :: fid = 20

    !! (4/3) pi r^3 * Rho = Mass
    !!    r = ((3/4) * Mass / Rho)^(1/3) 

    integer :: i
    integer :: iter, num_iter = 100, ierr
    
    real(dp) :: Rho, log10Rho, T, log10T, plRad
    real(dp) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
         d_dlnT_c_Rho(num_eos_basic_results)
    
    CoreMass = 0.6 * Mp

    CoreRadius = ((3./4.) * CoreMass / CoreDensity)**(1d0/3d0)
    
    !! Setup the initial mass profile
    do i=1,NCELL
       MassGrid(i) = (-cos(real(i-1) * pi / (NCELL-1)) + 1d0) * 0.5 * (Mp - CoreMass) + CoreMass 
       if(i .gt. 1) then
          dMGrid(i-1) = MassGrid(i) - MassGrid(i-1)
       endif
       RhoGrid(i) = RhoGuess
       RadiusGrid(i) = (MassGrid(i) / (RhoGuess * 4d0*pi/3d0))**(1d0/3d0)    
       TGrid(i) = TGuess
       
       Rho = RhoGrid(i)
       log10Rho = log10(Rho)
       T = TGrid(i)
       log10T = log10(T)
       
       call mixeos_get(id, k, eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
       
       PGrid(i) = exp(res(i_lnPgas))
    enddo
    !! Figure out an initial profile by iterating

    do iter=1,num_iter
       call update_pressure
       call update_density

       if(mod(iter,10) .eq. 0) then
          do i=1,NCELL
             write(*,*) RhoGrid(i), PGrid(i), TGrid(i), lnSGrid(i)
          enddo
       end if
    end do

    do i=1,NCELL
       Rho = RhoGrid(i)
       log10Rho = log10(RhoGrid(i))
       T = TGrid(i)
       log10T = log10(T)
       call mixeos_get(id, k, eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
!       lnSGrid(i) = res(i_lnS)
       
    enddo
    
    filename = "atm.dat"
    open(unit=fid, file=filename)

    do i=1,NCELL
       write(fid,*) i, MassGrid(i), RadiusGrid(i), RhoGrid(i), PGrid(i), TGrid(i), lnSGrid(i)
    enddo
    close(unit=fid)

    plRad = RadiusGrid(NCELL)

    write(*,*) "The central pressure should be roughly: " , &
         standard_cgrav * Mp**2d0 / (8 * pi * plRad**4d0)

  contains

    subroutine update_density
      use chem_def, only: num_chem_isos
      use eos_def
      use const_def, only: arg_not_provided

      integer :: which_other, max_iter
      real(dp) :: Rho, log10Rho, logT_result, &
           d_dlnRho_dlnPgas_const_T, d_dlnRho_const_Pgas, &
           logPgas, other_value, logT_tol, other_tol, logT_guess, dv, &
           logT_bnd1, logT_bnd2, other_at_bnd1, other_at_Bnd2
      real(dp) :: res(num_eos_basic_results), d_dlnRho_const_T(num_eos_basic_results), &
           d_dlnT_const_Rho(num_eos_basic_results)
      integer :: eos_calls, ierr


      !! 1. Determine density from pressure and fixed entropy

      do i=1,NCELL

         other_value = lnS_model
         which_other = i_lnS
         max_iter = 100
         logPgas = log10(PGrid(i))
         logT_guess = log10(TGrid(i))
         logT_bnd1 = arg_not_provided
         logT_bnd2 = arg_not_provided
         other_at_bnd1 = arg_not_provided
         other_at_bnd2 = arg_not_provided
         logT_tol = 1d-4
         other_tol = 1d-4

         call mixeosPT_get_T(id, k, eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa, &
              logPgas, which_other, other_value, &
              logT_tol, other_tol, max_iter, logT_guess, &
              logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
              logT_result, Rho, log10Rho, d_dlnRho_dlnPgas_const_T, d_dlnRho_const_Pgas, &
              res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
         
         if(ierr.ne.0) then
            write(*,*) logPgas, logT_guess, logT_result, eos_calls, ierr
         endif
         RhoGrid(i) = Rho
         TGrid(i) = 1d1**logT_result
         lnSGrid(i) = res(i_lnS)

      enddo
      
      !! 2. Determine Radius from density

      RadiusGrid(1) = CoreRadius
      do i=2,NCELL 
         dv = (dMGrid(i-1) / ((RhoGrid(i) + RhoGrid(i-1))/2d0))
         !         write(*,*) dMGrid(i-1), RhoGrid(i), dv
         RadiusGrid(i) = (RadiusGrid(i-1)**3d0 + (3d0/(4d0*pi))*dv)**(1d0/3d0)

      end do
    end subroutine update_density


    subroutine update_pressure
      PGrid(NCELL) = Pouter
      do i=NCELL-1,2,-1
         PGrid(i) = Pgrid(i+1) + 2d0*standard_cgrav * (MassGrid(i+1)**2d0 - MassGrid(i)**2d0) &
              / (pi * (RadiusGrid(i+1) + RadiusGrid(i))**4d0)
      enddo
      Pgrid(1) = PGrid(2) + (4d0*pi/6d0) * standard_cgrav * (RhoGrid(1)*RadiusGrid(2))**2d0      
    end subroutine update_pressure

  end subroutine test_build_model

end module test_mixeos_mod





program test_mixeos
  use test_mixeos_mod
  implicit none

!  real(dp) :: log10P, log10T
!  character(len=256) :: filename

  call setup_mixeos
  write(*,*) "Setup EOS Complete"

!! 0. Test Thermodynamic self consistency
!! a. Are the derivative transformations true inverses?
!!  This assumes that the derivatives passed to mixeos are self consistent
!  call test_thermo_deriv
!! b. Are the eos outputs thermodynamically self consistent?
!! I am concerned that this may not be the case
!! Here we compare the numeric derivatives against the derivatives from the results vector
!  call test_lnPconsistency
!! I have learned that d_dlnRho(i_lnPgas) is a superior metric than res(i_chiRho)

!  call test_lnSconsistency

!  call test_F

!  call test_h_consistency

!! 1. Test if the range functions are working correctly
  call test_PT_find_range

!! 2. Test if you can lookup some PT calls
!  call test_lookupPT

!! 3. Test the Rho T calls
!  call test_get_rhot

!! 4. Test the get_rho call for which_other = i_lnPgas, i_lnS, i_lnE
!  call test_get_rho

!! 5. Test the derivatives vector.  Is it consistent with a numerical derivative?
!  call test_derivs
!! 5b. More Closely check the derivatives vector by plotting values against derivatives
!  call DTpull_1dvect

!! Chapter 3 of Thesis  
!! A) Generate data for PT surface plots
!  call test_PT_grid

!! B) Generate data for RhoT surface plots
!  call test_do_grid
  
!! C) Compare numeric derivative with actual derivatives
!  call DTpull_1dvect

!  call test_build_model
  
!  call test_PT_get_T

!  call orig_eos_grid


  
!  log10P = 13.
!  log10T = 5.0
!  filename = "Z_PT.dat"
!  call mixvect_PT(log10P, log10T, filename)
  
!  call output_Prange_over_T


  call shutdown_mixeos
  
end program test_mixeos
