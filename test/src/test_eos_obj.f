!! Test an eos module
!! This module doesn't know anything about the implementation of the eos.
!! 
!! A program that uses this module should first Construct the object as necessary
!!
!! The purpose of this module is to test various aspects of an eos implementation.

module test_eos_obj

contains

  !! DT_PT_test
  !! Test to see if PT -> Rho and DT -> P are true inverses
  subroutine DT_PT_test(obj, ierr)
    use const_def
    use mixeos_def
    use base_eos_mod
    
    class (base_eos), pointer, intent(in) :: obj
    integer, intent(out) :: ierr
    real(dp) :: logTmin = 2d0, logTmax = 7d0, logPmin = 0d0, logPmax = 20d0
    real(dp) :: bnd_logP1, bnd_logP2, bnd_logRho1, bnd_logRho2
    real(dp) :: logP, logT, logRho, P, T, Rho, &
         Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight, &
         Pvect2(num_derivs), Evect2(num_derivs), Svect2(num_derivs), weight2
    integer :: Nt = 100, Np = 100
    integer :: It, Ip
    
    logical :: print_flag = .false.
    integer :: dragon_count = 0
    ierr = 0
    
    do It = 1, Nt
       logT = logTmin + (It - 1) * (logTmax - logTmin) / (Nt - 1)
       call obj%PT_get_PRhorange(logT, bnd_logP1, bnd_logP2, bnd_logRho1, bnd_logRho2, ierr)
       if(ierr .eq. 0) then
          do ip = 1, np
             print_flag = .false.
             logP = logPmin + (Ip - 1) * (logPmax - logPmin) / (Np - 1)
             P = 1d1**logP
             T = 1d1**logT
             
             if((logP .gt. bnd_logP1) .and. (logP .lt. bnd_logP2)) then
                
                call obj%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)                
                if(ierr .ne. 0) print_flag = .true.
                if (print_flag) write(*,*) "PT (P = ", P, ", T = ", T, ") => Rho = ", Rho, " (ierr = ", ierr, ")"
                call obj%DT_get(Rho, T, Pvect2, Evect2, Svect2, weight2, ierr)
                if(ierr .ne. 0) print_flag = .true.
                
                if (print_flag) then 
                   write(*,*) "DT (D = ", Rho, ", T = ", T, ") => P = ", Pvect2(i_val), " (ierr = ", ierr, ")"
                   write(*,*)
                   write(*,*) "Pvect - Pvect2: ", Pvect - Pvect2
                   write(*,*) "Evect - Evect2: ", Evect - Evect2
                   write(*,*) "Svect - Svect2: ", Svect - Svect2
                endif
             else
                call obj%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
                write(*,*) "Dragons (logP=", logP, ", logT=", logT, ")", ierr, weight
                dragon_count = dragon_count + 1
             endif
             
          enddo
       else
          write(*,*) "logT: ", logT, " does not have a known P,Rho range"
       endif
    enddo
    
    write(*,*) "There are ", dragon_count, " or ", (real(dragon_count) / real(Np * Nt))
    
  end subroutine DT_PT_test

  
  
end module test_eos_obj

