# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :lnP
  attr_accessor :lnE
  attr_accessor :lnS
  attr_accessor :log10P
  attr_accessor :log10E
  attr_accessor :log10S
  attr_accessor :grad_ad
  attr_accessor :dlnPdlnRho
  attr_accessor :dlnPdlnT
  attr_accessor :Cp
  attr_accessor :Cv
  attr_accessor :dEdRho
  attr_accessor :dSdT
  attr_accessor :dSdRho

  def initialize(data_dir)
    
    read_image_Xs(data_dir, 'log10RhoVect.data', 'log10(\rho)')
    read_image_Ys(data_dir, 'log10TVect.data', 'log10(T)')
    
    @lnP = read_image_data(data_dir, 'lnPmat')
    @lnE = read_image_data(data_dir, 'lnEmat')
    @lnS = read_image_data(data_dir, 'lnSmat')
    @grad_ad = read_image_data(data_dir, 'grad_ad')
    @dlnPdlnRho = read_image_data(data_dir, 'dlnP_dlnRho')
    @dlnPdlnT  = read_image_data(data_dir, 'dlnP_dlnT')
    @Cp = read_image_data(data_dir, 'Cp')
    @Cv = read_image_data(data_dir, 'Cv')
    @dEdRho = read_image_data(data_dir, 'dE_dRho')
    @dSdT = read_image_data(data_dir, 'dS_dT')
    @dSdRho = read_image_data(data_dir, 'dS_dRho')
    
    @log10P = @lnP / log(1e1)
    @log10E = @lnE / log(1e1)
    @log10S = @lnS / log(1e1)
    
    end
  
end # class MY_data


class MY_plot

    include Math
    include Tioga
    include FigureConstants
    include Image_plot
        
    def initialize(data_dir)
      
      @data_dir = data_dir
      @figure_maker = FigureMaker.default
      t.def_eval_function { |str| eval(str) }
      t.save_dir = 'plot.figure'

            

      t.def_figure('lnP') { lnP }
      t.def_figure('lnE') { lnE }
      t.def_figure('lnS') { lnS }


      @image_data = MY_data.new(data_dir)
      @label_scale = 0.75
      @no_clipping = false #true        

      ### Load the neptune profile into the class
      @margin = 0.05

#      @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
#      @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
#      @LowMasslog10TProfile1 = @LowMassProfile1[1]

#      @LowMassProfile2 = Dvector.read("planet_data/RhoTset02.dat")
#      @LowMasslog10RhoProfile2 = @LowMassProfile2[0]
#      @LowMasslog10TProfile2 = @LowMassProfile2[1]

#      @LowMassProfile3 = Dvector.read("planet_data/RhoTset03.dat")
#      @LowMasslog10RhoProfile3 = @LowMassProfile3[0]
#      @LowMasslog10TProfile3 = @LowMassProfile3[1]

#      @HD80606Profile = Dvector.read("planet_data/RhoTset04.dat")
#      @HD80606log10RhoProfile = @HD80606Profile[0]
#      @HD80606log10TProfile = @HD80606Profile[1]
      
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
      t.def_enter_page_function { enter_page } 
      
    end
    
    def enter_page
        t.yaxis_numeric_label_angle = -90
        t.page_setup(11*72/2,8.5*72/2)
        t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
    end
    
    def clip_image
      t.fill_color = Black
      t.fill_frame
      return
    end
    
    def t
        @figure_maker
    end
    
    def d
        @image_data
    end
    
    def do_decorations(title)
    end
    
    # plot routines
    
    def lnP
      image_plot('d' => d, 'zs' => d.lnP, 'title' => 'lnP ', 'interpolate' => false,
                 'z_lower' => 20, 'z_upper' => 35, 'interpolate' => false)

#      t.subplot('right_margin' => 0.05, 
#                'left_margin' => @image_left_margin) do 
#        @bounds = [-5,2,5.5,2.5]
#        t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
#        t.top_edge_visible = false
#        t.right_edge_visible = false
#        t.show_plot(@bounds) {
#          t.line_width=1.0
#          t.show_polyline(@LowMasslog10RhoProfile1, @LowMasslog10TProfile1, Blue)
#          t.show_polyline(@LowMasslog10RhoProfile2, @LowMasslog10TProfile2, Red)
#          t.show_polyline(@LowMasslog10RhoProfile3, @LowMasslog10TProfile3, Green)
#          t.show_polyline(@HD80606log10RhoProfile, @HD80606log10TProfile, Black)
#        }
#      end
    end

    def lnE
      image_plot('d' => d, 'zs' => d.lnE, 'title' => 'lnE ', 'interpolate' => false,
                 'z_lower' => 20, 'z_upper' => 35, 'interpolate' => false)
    end

    def lnS
      image_plot('d' => d, 'zs' => d.lnS, 'title' => 'lnS ', 'interpolate' => false,
                 'z_lower' => 19, 'z_upper' => 21, 'interpolate' => false)
    end  

    def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
      xmin = xs.min if xmin == nil
      xmax = xs.max if xmax == nil
      ymin = ys.min if ymin == nil
      ymax = ys.max if ymax == nil
      
      width = (xmax == xmin) ? 1 : xmax - xmin
      height = (ymax == ymin) ? 1 : ymax - ymin
      
      left_boundary = xmin - margin * width
      right_boundary = xmax + margin * width
      
      top_boundary = ymax + margin * height
      bottom_boundary = ymin - margin * height
      
      return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
    end

    def pressure
      image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
                 'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
      # We would like to overplot some profile here.
      t.subplot('right_margin' => 0.05, 
                'left_margin' => @image_left_margin) do
        @bounds = [-4,2.5,2,10]
        t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
        t.top_edge_visible = false
        t.right_edge_visible = false
        t.show_plot(@bounds) {
#        clip_press_image
        t.line_width=2.5
        t.show_polyline(@LowMasslog10RhoProfile, @LowMasslog10TProfile, Blue)
#      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
#                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
        }
      end 
    end

end




MY_plot.new('DTdata')
