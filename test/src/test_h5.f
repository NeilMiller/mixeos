program test_h5
  use const_def
  use const_lib
  use test_eos_obj
  use h5table_eos_mod
  use base_eos_mod

  type(h5table_eos), target :: my_eos
  integer :: ierr
  character (len=256) :: my_mesa_dir
  class(base_eos), pointer :: my_eos_ptr

  !! Parameters for h5 table
  integer :: Xsize, Ysize
  character (len=256) :: filename, name

  ierr = 0
  
  Xsize = 81
  Ysize = 31
  filename = "/Users/neil/research/mesa.planet/mixeos/data/wateridealfe.bin"
  name = "water"
  

  my_mesa_dir = "/Users/neil/research/mesa/"

  call const_init(my_mesa_dir,ierr)
  
  call my_eos%Construct(Xsize, Ysize, filename, name, ierr)  
  
  my_eos_ptr => my_eos
  call DT_PT_test(my_eos_ptr, ierr)
  
end program test_h5

!! The purpose of this routine is to check that the h5 table is actually working
!!  and is continuous



