module aneos_access
  implicit none
  
  integer, parameter :: maxmat=21
  integer :: imat,nntype(maxmat),kpai
  
  double precision, parameter :: evtemp = 11604.8d0

  double precision, parameter :: delta = 1d-3
  logical, parameter :: lognder = .false.
  
  integer :: nmat
  logical :: ANEOS_init = .false.  
  
  integer, parameter :: num_free_derivs = 9
  integer, parameter :: a_val = 1
  integer, parameter :: a_dRho = 2
  integer, parameter :: a_dT = 3
  integer, parameter :: a_dRho2 = 4
  integer, parameter :: a_dT2 = 5
  integer, parameter :: a_dRhodT = 6
  integer, parameter :: a_dRho2dT = 7
  integer, parameter :: a_dRhodT2 = 8
  integer, parameter :: a_dRho2dT2 = 9
  
  double precision :: minlogRhoAlpha = 0., minlogRhoBeta = -5., maxlogRhoAlpha = 0., maxlogRhoBeta = 5.

contains
  subroutine ANEOSWater_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif

    kinp=21
    klst=22
    open(kinp,file='INPUT/aneos2.input')
    open(klst, file='OUTPUT/aneos.output')
    nmat = 1
    imat = 1
    nntype(1) = -1
    nntype(2) = -2
    nntype(3) = -3
    nntype(4) = -4
    nntype(5) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSWater_Init
  
  subroutine ANEOSSerpentine_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif
    
    kinp=21
    klst=22
!    open(kinp,file='INPUT/aneos2.input')
    open(kinp, file='INPUT/serpentine2.input')
    open(klst, file='OUTPUT/aneos_serp.output')
    nmat = 5
    imat = 1
    nntype(1) = -3
!    nntype(2) = -2
!    nntype(3) = -3
!    nntype(4) = -4
!    nntype(5) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSSerpentine_Init

  subroutine ANEOSIron_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif
    
    kinp=21
    klst=22
!    open(kinp,file='INPUT/aneos2.input')
    open(kinp,file='INPUT/iron2.input')
    open(klst, file='OUTPUT/aneos_iron.output')
    nmat = 5
    imat = 1
!    nntype(1) = -1
!    nntype(2) = -2
!    nntype(3) = -3
!    nntype(4) = -4
!    nntype(5) = -5
    nntype(1) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSIron_Init

  !! Here is where we call ANEOS.
  !! This is guarenteed to output a thermodynamically self-consistent state to the user
  subroutine ANEOS_get_val(Rho, T, Pvect, Evect, Svect, ierr)
    external aneos
    COMMON /FILEOS/ klst, kinp
    
    integer :: klst, kinp
    
    double precision, intent(in) :: Rho, T  !! Take inputs in [g/cc] and [K]
    double precision, intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
    integer, intent(out) :: ierr
    
    double precision :: Tev, dRho, dTev, tmpvar, dlog10Rho, dlog10Tev, log10Rho, log10Tev
    double precision :: free_energy, dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, d2fdxdy_num, d3fdx3_num, &
         d3fdxdy2_num, d3fdx2dy_num, d3fdy3_num
    double precision :: df_d, df_t, df_dd, df_tt, df_dt, df_ddd, df_ddt, df_dtt, df_ttt

    integer :: ierr

    if(ANEOS_init .eqv. .false.) then
       write(*,*) "You need to call ANEOSWater_Init"
       stop
    endif
    
    kpai = 2
    Tev = T / evtemp
    
    !! The derivatives are calculated in (Rho == g/cc, T == eV)
    call calculate_derivatives(Rho, Tev, delta, free_energy, dfdx_num, dfdy_num, &
         d2fdx2_num, d2fdy2_num, d2fdxdy_num, &
         d3fdx3_num, d3fdx2dy_num, d3fdxdy2_num, d3fdy3_num)
    
    df_d = dfdx_num
    df_t = dfdy_num / evtemp
    df_dd = d2fdx2_num
    df_tt = d2fdy2_num /evtemp**2d0
    df_dt = d2fdxdy_num / evtemp
    df_ddd = d3fdx3_num
    df_ddt = d3fdx2dy_num / evtemp
    df_dtt = d3fdxdy2_num / evtemp**2d0
    df_ttt = d3fdy3_num / evtemp**3d0
!    df_ddtt = d4fdrho2dt2 / evtemp**2d0

    Pvect(i_val) = RhoSQ * df_d
    Pvect(i_drho) = RhoSQ * df_dd + 2 * Rho * df_d
    Pvect(i_dt) = RhoSQ * df_dt
    Pvect(i_drho2) = RhoSQ * df_ddd + 4. * Rho * df_dd + 2. * df_d 
    Pvect(i_dt2) = RhoSQ * df_dtt
    Pvect(i_drhodt) = RhoSQ * df_ddt + 2. * Rho * df_dt
    
    Svect(i_val) = -df_t
    Svect(i_drho) = -df_dt
    Svect(i_dt) = -df_tt
    Svect(i_drho2) = -df_ddt
    Svect(i_dt2) = - df_ttt
    Svect(i_drhodt) = - df_dtt
    
    Evect(i_val) = free - T * df_t
    Evect(i_drho) = df_d - T * df_dt
    Evect(i_dt) = -T * df_tt
    Evect(i_drho2) = df_dd - T*df_ddt
    Evect(i_dt2) = -df_tt - T * df_ttt
    Evect(i_drhodt) = - T * df_dtt

  contains
    
    !! Calculate the first order derivatives
    subroutine calculate_derivatives(Rho, Tev, delta, free_energy, dfdrho_num, dfdt_num, &
         d2fdrho2_num, d2fdt2_num, d2fdrhodt_num, &
         d3fdrho3_num, d3fdrho2dt_num, d3fdrhodt2_num, d3fdt3_num, kros, ierr)
      use h5table_params


      double precision, intent(in) :: Rho, Tev, delta1
      double precision, intent(out) :: dfdrho_num, dfdt_num, &
           d2fdrho2_num, d2fdt2_num, d2fdrhodt_num, &
           d3fdrho3_num, d3fdrho2dt_num, d3fdrhodt2_num, d3fdt3_num, kros
      integer, intent(out) :: ierr

      double precision :: RhoVect(5), TevVect(5)
      double precision :: dRho, dTev
      integer :: idx, idy, i
      double precision :: free_cluster(5,5)!, kpai_sten(5,5)
      double precision :: dfdRho_ln(5), dfdT_ln(5), d2fdRho2_ln(5), d2fdT2_ln(5)
      double precision :: ppi,uui,ssi,ccvi
      double precision :: ddpdti,ddpdri,ffkrosi,ccsi,ffve,ffva

      ierr = 0

      dRho = Rho * delta1
      dTev = Tev * delta1
       
      RhoVect(1) = Rho - 2.d0 * dRho
      RhoVect(2) = Rho - dRho
      RhoVect(3) = Rho 
      RhoVect(4) = Rho + dRho
      RhoVect(5) = Rho + 2.d0 * dRho
    
      TevVect(1) = Tev - 2.d0 * dTev
      TevVect(2) = Tev - dTev
      TevVect(3) = Tev
      TevVect(4) = Tev + dTev
      TevVect(5) = Tev + 2.d0 * dTev


      do idx=1,5
         do idy=1,5
            
            call aneos(TevVect(idx), RhoVect(idy), ppi, uui, ssi, ccvi, ddpdti, &
                 ddpdri, ffkrosi, ccsi, kpai, imat, ffve, ffva)
            free_cluster(idx,idy) = uui - TevVect(idy) * ssi
!            kpai_sten(idx,idy) = kpai
         enddo
      end do

      dfdrho_num = (-free_cluster(5,3) + 8.d0* free_cluster(4,3) - 8d0*free_cluster(2,3) + free_cluster(1,3)) / (12.d0*dRho)
      dfdt_num = (-free_cluster(3,5) + 8.d0 * free_cluster(3,4) - 8d0*free_cluster(3,2) + free_cluster(3,1)) / (12.d0*dTev)

      
      do i=1,5
         dfdRho_ln(i) = (-free_cluster(5,i) + 8.d0* free_cluster(4,i) - 8d0*free_cluster(2,i) + free_cluster(1,i)) / (12.d0*dRho)
         dfdT_ln(i) = (-free_cluster(i,5) + 8.d0 * free_cluster(i,4) - 8d0*free_cluster(i,2) + free_cluster(i,1)) / (12.d0*dTev)
         d2fdRho2_ln(i) = (-3d0 * free_cluster(5,i) + 8d0 * free_cluster(4,i) - 10d0 * free_cluster(3,i) +  8d0*free_cluster(2,i) &
              - 3d0*free_cluster(1,i)) / (6d0 * dRho * dRho)
         d2fdT2_ln(i) = (-3d0 * free_cluster(i,5) + 8d0 * free_cluster(i,4) - 10d0 * free_cluster(i,3) +  8d0*free_cluster(i,2) &
              - 3d0*free_cluster(i,1)) / (6d0 * dTev * dTev)
      enddo
      d2fdRho2_num = d2fdRho2_ln(3)
      d2fdT2_num = d2fdT2_ln(3)
      d2fdRhodT_num = (-dfdRho_ln(5) + 8d0 * dfdRho_ln(4) - 8d0 * dfdRho_ln(2) + dfdRho_ln(1)) / (12d0 * dTev) 
      
      d3fdRho3_num = (-0.5d0*free_cluster(1,3)+free_cluster(2,3)-free_cluster(4,3)+0.5d0*free_cluster(5,3))/(dRho*dRho*dRho)
      d3fdRho2dT_num = (-d2fdRho2_ln(5) + 8d0 * d2fdRho2_ln(4) - 8d0 * d2fdRho2_ln(2) + d2fdRho2_ln(1)) / (12.d0 * dTev)
      d3fdRhodT2_num = (-d2fdT2_ln(5) + 8d0 * d2fdT2_ln(4) - 8d0 * d2fdT2_ln(2) + d2fdT2_ln(1)) / (12d0 * dRho)
      d3fdT3_num = (-0.5d0*free_cluster(3,1)+free_cluster(3,2)-free_cluster(3,4)+0.5d0*free_cluster(3,5))/(dTev * dTev *dTev)

      
    end subroutine calculate_derivatives

  end subroutine ANEOS_get_val


  !! WORK ON THIS SECTION
  subroutine ANEOS_get_Rho(tbl, P, T, Rho, Pvect, Evect, Svect, ierr)
    use num_lib
    use h5table_params   !! Parameters, only for h5table
      use h5table_register, only: set_handle, free_handle !! table register access procedures. 
      implicit none
      type (h5tbl), pointer, intent(in) :: tbl
      double precision, intent(in) :: P, T
      double precision, intent(out) :: Rho
      double precision, intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
      double precision :: P1, P2, Rho1, Rho2, RhoGuess, lnRho, &
           log10T, lnT, minlogRhoCut, maxlogRhoCut
      integer, intent(out) :: ierr

      integer :: tbl_handle, itermax
      integer, parameter :: lrpar = 20, lipar = 1
      double precision, target :: rpar(lrpar)
      integer :: ipar(lipar)
      double precision :: eps

      ierr = 0
      call set_handle(tbl, tbl_handle, ierr)  !! Think about this more
      
      rpar(1) = T
      rpar(2) = log(P)
      ipar(1) = tbl_handle

      log10T = log10(T)
      lnT = log(T)
      

      Rho1 = exp(tbl%minlnRho) * 1.01
      Rho2 = exp(tbl%maxlnRho) * 0.99

      minlogRhoCut = tbl%minlogRhoAlpha * log10T + tbl%minlogRhoBeta
      maxlogRhoCut = tbl%maxlogRhoAlpha * log10T + tbl%maxlogRhoBeta
      if(log10(Rho1) .lt. minlogRhoCut) then
         Rho1 = 1d1**minlogRhoCut * 1.01
      endif
      if(log10(Rho2) .gt. maxlogRhoCut) then
         Rho2 = 1d1**maxlogRhoCut * 0.99
      endif

      if(lnT .lt. tbl%minlnT) then
         ierr = H5TBLERR_MINTEMPERATURE
         call free_handle(tbl_handle, ierr)
         call set_error_condition
         return
      endif

      if(lnT .gt. tbl%maxlnT) then
         ierr = H5TBLERR_MAXTEMPERATURE
         call free_handle(tbl_handle, ierr)
         call set_error_condition
         return
      endif
!      write(*,*) "PT Diagnostics" 
!      write(*,*) log10T
!      write(*,*) tbl%minlogRhoAlpha, tbl%minlogRhoBeta
!      write(*,*) tbl%maxlogRhoAlpha, tbl%maxlogRhoBeta
!      write(*,*) minlogRhoCut, maxlogRhoCut

!      write(*,*) "Rho range: ", Rho1, Rho2

      if(Rho1 .gt. Rho2) then
         ierr = H5TBLERR_NOPRESSURESOL
!         write(*,*) "No valid density range"
         call free_handle(tbl_handle, ierr)
         call set_error_condition
         return
      endif

      call h5_get_val(tbl, Rho1, T, Pvect, Evect, Svect, ierr)
      P1 = Pvect(TBL_VAL)
      call h5_get_val(tbl, Rho2, T, Pvect, Evect, Svect, ierr)
      P2 = Pvect(TBL_VAL)
!      write(*,*) "h5tbl bounds"
!      write(*,*) "Rho1 = ", Rho1, " <=> log10(P1) = ", log10(P1)
!      write(*,*) "Rho2 = ", Rho2, " <=> log10(P2) = ", log10(P2)

      Rho = 0.
      Pvect(:) = 0.
      Evect(:) = 0.
      Svect(:) = 0.
      if(P2.lt.P) then
         call free_handle(tbl_handle, ierr)
         call set_error_condition
         ierr = H5TBLERR_MAXPRESSURE
         Pvect(TBL_VAL) = P2  
         if(mixdbg) then
            write(*,*) "Pressure greater than max pressure ", P2, " < ", P
         endif
         return
      elseif(P.lt.P1) then
         call free_handle(tbl_handle, ierr)
         call set_error_condition
         ierr = H5TBLERR_MINPRESSURE
         Pvect(TBL_VAL) = P1
         if(mixdbg) then
            write(*,*) "Pressure less than min pressure ", P1, " < ", P
         endif
         return
      else
         !! 2. Rootfind
         !!    a. Guess a density by assuming that lnP ~ alpha * lnRho + beta
         RhoGuess = exp(log(Rho1) + (log(P) - log(P1)) * (log(Rho1) - log(Rho2)) &
                                                       / (log(P1) - log(P2)))
         
!         write(*,*) "RhoGuess = ", RhoGuess
         eps = H5TBL_EPSILON
         itermax = H5TBL_ITERMAX
         !!    b. Execute
         lnRho = safe_root_with_initial_guess(DeltalnP_T, log(RhoGuess), &
              log(Rho1), log(Rho2), log(P1)-log(P), log(P2)-log(P), &
              itermax, eps, eps, lrpar, rpar, lipar, ipar, ierr)
         if(ierr .eq. 0) then
            Rho = exp(lnRho)
            Pvect(1:6) = rpar(3:8)
            Evect(1:6) = rpar(9:14)
            Svect(1:6) = rpar(15:20)
         else
            call set_error_condition
         end if
         !! We don't need the call below.
!         call h5_get_val(tbl, Rho, T, Pvect, Evect, Svect, ierr) 
         call free_handle(tbl_handle, ierr)

      endif

    contains
      subroutine set_error_condition
        Rho = 1d-100
        Pvect(1:6) = 1d0
        Evect(1:6) = 1d0
        Svect(1:6) = 1d0
      end subroutine set_error_condition
    end subroutine h5_get_Rho

    !! This should be in lnP or logP and logRho
    double precision function DeltalnP_T(lnRho, dlnPdlnRho,lrpar,rpar,lipar,ipar,ierr)
      use h5table_params !! Private parameters, only for h5table
      use h5table_register
      implicit none
      double precision, intent(in) :: lnRho
      double precision, intent(out) :: dlnPdlnRho
      double precision, intent(inout), target :: rpar(lrpar)
      double precision, pointer, dimension(:) :: Pvect, Evect, Svect
      integer, intent(inout), target :: ipar(lipar)      
      integer, intent(in) :: lrpar, lipar
      integer, intent(out) :: ierr

      integer :: tbl_handle, i
      type (h5tbl), pointer :: tbl
      double precision :: T, lnP0, lnP, Rho

      ierr = 0

      T = rpar(1)
      lnP0 = rpar(2)
      i = 2
      Pvect => rpar((i+1):(i+6)); i = i+6  
      Evect => rpar((i+1):(i+6)); i = i+6
      Svect => rpar((i+1):(i+6)); i = i+6

      tbl_handle = ipar(1)
      call get_tbl(tbl_handle, tbl, ierr)
      Rho = exp(lnRho)
      call h5_get_val(tbl, Rho, T, Pvect, Evect, Svect, ierr)
!      write(*,*) "Test: Rho = ", Rho, " <=> log10P = ", log10(Pvect(TBL_VAL))
      
      lnP = log(Pvect(TBL_VAL))
      DeltalnP_T = lnP - lnP0
      dlnPdlnRho = (Rho / Pvect(TBL_VAL)) * Pvect(TBL_DX)
    end function DeltalnP_T
    

  subroutine ANEOS_Shutdown
    integer :: klst, kinp

    COMMON /FILEOS/ klst, kinp

    if(ANEOS_init .eqv. .false.) then
       write(*,*) "You need to call ANEOSWater_Init"
       stop
    endif
    close(kinp)
    close(klst) 
  end subroutine ANEOS_Shutdown
end module aneos_access

