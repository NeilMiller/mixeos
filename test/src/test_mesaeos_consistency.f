!! The purpose of this is to test the degree to which the MESA eos is self-consistent.

!! Routines go here
module test_mod1
  use const_lib
  use chem_def
  use chem_lib
  use chem_def
  use eos_def
  use eos_lib

  implicit none

  integer, parameter :: species = 2
  integer, parameter :: h1 = 1, he4 = 2
  integer, pointer, dimension(:) :: net_iso, chem_id
  real(dp) :: X, Y, Z, abar, zbar, z2bar, ye
  real(dp), target:: xa(species)
  
  real(dp) :: dabar_dx(species), dzbar_dx(species),  dmc_dx(species), &
           sumx, xh, xhe, mass_correction
 
  integer :: eos_handle = 1
  real(dp), parameter :: epsilon = 1d-8

contains
  subroutine Init_Test
    implicit none
    
    character (len=256) :: data_dir, eos_file_prefix, my_mesa_dir, isotope_filename
    integer :: ierr
    logical, parameter :: use_cache = .true.

    my_mesa_dir = "/Users/neil/research/mesa"

    call const_init(my_mesa_dir, ierr)

    write(*,*) "Initializing chem"
    isotope_filename = "isotopes.data"
    data_dir = "/Users/neil/research/mesa/data"
    ierr = 0
    call chem_init(isotope_filename, ierr)
    write(*,*) " ierr = ", ierr
      
    write(*,*) "Initializing eos"
    eos_file_prefix = 'mesa'
    call eos_init(eos_file_prefix, use_cache, ierr)
    write(*,*) " ierr = ", ierr
      

    !! Setting up our custom composition {
    allocate(net_iso(num_chem_isos), chem_id(species), stat=ierr)
      
    chem_id(h1) = ih1; net_iso(ih1) = h1
    chem_id(he4) = ihe4; net_iso(ihe4) = he4

    X = 0.8
    Y = 0.2
    Z = 0.0
    xa(h1) = X
    xa(he4) = Y
    !! This may be the issue
    call composition_info( &
           species, chem_id, xa, xh, xhe, abar, zbar, z2bar, ye,  &
           mass_correction, sumx, dabar_dx, dzbar_dx, dmc_dx)
    
    
  end subroutine Init_Test
  
  subroutine Shutdown_Test
    implicit none

    
    write(*,*) "Shutting down eos module"
    call eos_shutdown
    
    deallocate(net_iso, chem_id)
    
  end subroutine Shutdown_Test
  
  
  !! THE MEAT
  subroutine Do_Test
    use eos_def
    implicit none
    
    integer, parameter :: N1 = 500
    integer, parameter :: N2 = 500
    real(dp), pointer, dimension(:) :: log10Pvect, log10Tvect
    real(dp), pointer, dimension(:,:) :: Diff_dlnPdlnRho,&
                                         Diff_dlnPdlnT, &
                                         Diff_dlnEdlnRho, &
                                         Diff_dlnEdlnT, &
                                         Diff_dlnSdlnRho, &
                                         Diff_dlnSdlnT
    real(dp) :: min_logP, max_logP, dlogP, min_logT, max_logT, dlogT, &
         P, T, log10P, log10T, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, E, S
    real(dp) :: res(num_eos_basic_results), &
                d_dlnRho_const_T(num_eos_basic_results), &
                d_dlnT_const_Rho(num_eos_basic_results)
    integer :: i,j, ierr
    character(len=256) :: filename
    
    allocate(log10Pvect(N1), log10Tvect(N2))
    allocate(Diff_dlnPdlnRho(N1,N2), Diff_dlnPdlnT(N1,N2), &
             Diff_dlnEdlnRho(N1,N2), Diff_dlnEdlnT(N1,N2), &
             Diff_dlnSdlnRho(N1,N2), Diff_dlnSdlnT(N1,N2))
    
    min_logP = 11d0
    max_logP = 11.5d0
    min_logT = 3.5d0
    max_logT = 3.7d0
    
    dlogP = (max_logP - min_logP) / (N1 - 1)
    dlogT = (max_logT - min_logT) / (N2 - 1)
    
    do i=1,N1
       log10Pvect(i) = min_logP + dlogP * (i-1)
    enddo

    do j=1,N2
       log10Tvect(j) = min_logT + dlogT * (j-1)
    enddo

    do i=1,N1
       do j=1,N2
          P = 1d1**log10Pvect(i)
          T = 1d1**log10Tvect(j)
          log10P = log10Pvect(i)
          log10T = log10Tvect(j)

          
          call eosPT_get(eos_handle, Z, X, abar, zbar,  &
               species, chem_id, net_iso, xa, &
               P, log10P, T, log10T, &
               Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas,  &
               res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)

          E = exp(res(i_lnE))
          S = exp(res(i_lnS))
          Diff_dlnPdlnRho(i,j) = d_dlnRho_const_T(i_lnPgas) - res(i_chiRho)
          Diff_dlnPdlnT(i,j)   = d_dlnT_const_Rho(i_lnPgas)   - res(i_chiT)
          Diff_dlnEdlnRho(i,j) = d_dlnRho_const_T(i_lnE)    - (Rho / E) * res(i_dE_dRho)
          Diff_dlnEdlnT(i,j)   = d_dlnT_const_Rho(i_lnE)    - (T / E) * res(i_Cv) 
          Diff_dlnSdlnRho(i,j) = d_dlnRho_const_T(i_lnS)    - (Rho/ S) * res(i_dS_dRho)
          Diff_dlnSdlnT(i,j)   = d_dlnT_const_Rho(i_lnS)    - (T / S) * res(i_dS_dT)
          
          if((abs(Diff_dlnPdlnRho(i,j)) .gt. epsilon) .or. &
               (abs(Diff_dlnPdlnT(i,j)) .gt. epsilon) .or. &
               (abs(Diff_dlnEdlnRho(i,j)) .gt. epsilon) .or. &
               (abs(Diff_dlnEdlnT(i,j)) .gt. epsilon) .or. &
               (abs(Diff_dlnSdlnRho(i,j)) .gt. epsilon) .or. &
               (abs(Diff_dlnSdlnT(i,j)) .gt. epsilon)) then
             write(*,*) "DIFFERENCE DETECTED"
             write(*,*) "logP = ", log10P
             write(*,*) "logT = ", log10T
             write(*,*) "Diff_dlnPdlnRho = ", Diff_dlnPdlnRho(i,j)
             write(*,*) "Diff_dlnPdlnT = ", Diff_dlnPdlnT(i,j)
             write(*,*) "Diff_dlnEdlnRho = ", Diff_dlnEdlnRho(i,j)
             write(*,*) "Diff_dlnEdlnT = ", Diff_dlnEdlnT(i,j)
             write(*,*) "Diff_dlnSdlnRho = ", Diff_dlnSdlnRho(i,j)
             write(*,*) "Diff_dlnSdlnT = ", Diff_dlnSdlnT(i,j)
             stop
          endif
          
       enddo
    enddo
    
    filename = "mesa_check.data/log10Pvect.data"
    call write_vect(log10Pvect, filename)
    
    filename = "mesa_check.data/log10Tvect.data"
    call write_vect(log10Tvect, filename)

    filename = "mesa_check.data/diff_dlnPdlnRho.data"
    call write_matrix(Diff_dlnPdlnRho, filename)
    
    filename = "mesa_check.data/diff_dlnPdlnT.data"
    call write_matrix(Diff_dlnPdlnT, filename)

    filename = "mesa_check.data/diff_dlnEdlnRho.data"
    call write_matrix(Diff_dlnEdlnRho, filename)
    
    filename = "mesa_check.data/diff_dlnEdlnT.data"
    call write_matrix(Diff_dlnEdlnT, filename)

    filename = "mesa_check.data/diff_dlnSdlnRho.data"
    call write_matrix(Diff_dlnSdlnRho, filename)
    
    filename = "mesa_check.data/diff_dlnSdlnT.data"
    call write_matrix(Diff_dlnSdlnT, filename)
    
    deallocate(log10Pvect)
    deallocate(log10Tvect)
    deallocate(Diff_dlnPdlnRho)
    deallocate(Diff_dlnPdlnT)
    deallocate(Diff_dlnEdlnRho)
    deallocate(Diff_dlnEdlnT)
    deallocate(Diff_dlnSdlnRho)
    deallocate(Diff_dlnSdlnT)

  contains
    subroutine write_vect(vect, filename)
      double precision, intent(in), dimension(:) :: vect
      character (len=256) :: filename
      integer :: ios
      
      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19,'(e14.6)') vect
      close(unit=19)
      
    end subroutine write_vect

    subroutine write_matrix(matrix, filename)
      double precision, intent(in), dimension(:,:) :: matrix
      character (len=256) :: filename
      integer :: ios
      
      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios, action='write', form='formatted')
      write(19, '(e14.6)') matrix
      close(unit=19)
    end subroutine write_matrix
    
  end subroutine Do_Test
  

end module test_mod1



program test_mesaeos_consistency
  use test_mod1
  implicit none
  
  call Init_Test
  call Do_Test
  call Shutdown_Test
  write(*,*) "Done"
end program test_mesaeos_consistency
