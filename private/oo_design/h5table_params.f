module h5table_params
  
  !! Parameters only for the h5table
  logical, parameter :: h5dbg = .false.
  integer, parameter :: H5TBLERR_FILE = -64
  integer, parameter :: H5TBLERR_OUTBOUND = -65
  integer, parameter :: H5TBLERR_BADIND = -66
  integer, parameter :: H5TBLERR_REGFULL = -67
  integer, parameter :: H5TBLERR_BADASSOC = -68

  integer, parameter :: H5TBLERR_MAXRHO = -69
  integer, parameter :: H5TBLERR_MINRHO = -70
  integer, parameter :: H5TBLERR_MAXPRESSURE = -71
  integer, parameter :: H5TBLERR_MINPRESSURE = -72
  integer, parameter :: H5TBLERR_MAXTEMPERATURE = -73
  integer, parameter :: H5TBLERR_MINTEMPERATURE = -74
  integer, parameter :: H5TBLERR_NOPRESSURESOL = -75

  integer, parameter :: H5TBLERR_NDERMIX = -80

  !!! Indexes for Biquintic interpolation
  integer, parameter :: TBL_VAL = 1
  integer, parameter :: TBL_DX = 2
  integer, parameter :: TBL_DY = 3
  integer, parameter :: TBL_DX2 = 4
  integer, parameter :: TBL_DY2 = 5
  integer, parameter :: TBL_DXDY = 6
  integer, parameter :: TBL_DX2DY = 7
  integer, parameter :: TBL_DXDY2 = 8
  integer, parameter :: TBL_DX2DY2 = 9

  !!! Parameters for a rootfind: Max iterations and Epsilon
  integer, parameter :: H5TBL_ITERMAX = 100
  double precision, parameter :: H5TBL_EPSILON = 1d-8
  
end module h5table_params
