module mixeos_mod
  use const_def, only: dp
  implicit none

  integer, parameter :: i_rho_flag = 1
  integer, parameter :: i_which_other = 2
  integer, parameter :: i_handle = 3
  integer, parameter :: i_species = 4
  integer, parameter :: i_eos_calls = 5
  integer, parameter :: n_iparams = 5

  integer, parameter :: n_rparams = 6
  integer, parameter :: i_other = 1
  integer, parameter :: i_fixed_log_input = 2
  integer, parameter :: i_Z = 3
  integer, parameter :: i_X = 4
  integer, parameter :: i_abar = 5
  integer, parameter :: i_zbar = 6
  
contains
  
  !! I believe that the get_res_PT code works
  
  !! If you want to improve efficiency of this routine by as much as 30%, then one
  !! way would be to define all of the vectors below inside of 
  !! the accessor type so that they are not allocated every time.
  !! I didn't perform this optimization yet because it makes the system
  !! more confusing for the reader
  !! The 30% number is from my "test_allocation_time.f" experiment
  !! I used the -O3 optimization flag with gfortran and it didn't help, maybe ifort isn't so bad
  

  !! This routine determines the minimum and maximum pressure that are valid for a given temperature
  subroutine get_pressure_range( & 
       T, log10T, log10P1, log10P2, ierr)
    use h5table_type
    use h5table, only:h5_get_PRhorange
    use mixeos_private
    use mixeos_def
    use const_lib, only: dp
    implicit none
    
    real(dp), intent(in) :: T, log10T
    real(dp), intent(out) :: log10P1, log10P2
    integer, intent(out) :: ierr
    
    type (h5tbl), pointer :: current_metal_tbl
    real(dp) :: m_log10P1, m_log10P2, m_log10Rho1, m_log10Rho2
    integer :: i
    
    log10P1 = -100. !! Way out there
    log10P2 = 100.
    
    ierr = 0
    
    !! Regardless of anything, only work within the temperature range
    if(log10T .lt. mixeos_minlog10T) then
       ierr = MIXEOSERR_MINTEMPERATURE
       return
    endif
    
    if(log10T .gt. mixeos_maxlog10T) then
       ierr = MIXEOSERR_MAXTEMPERATURE
       return
    endif
    
    !! Always include Hydrogen and Helium bound
    log10P1 = eos_minPTAlpha * log10T + eos_minPTBeta + 0.0001
    log10P2 = eos_maxPTAlpha * log10T + eos_maxPTBeta - 0.0001
!    write(*,*) "H/He logPRange: ", log10P1, log10P2

    do i=1, NUM_METALS 
       if(mixeos_chem_id(i).gt.0d0) then  !! If the metal is defined it is important
          current_metal_tbl => metal_tbls(i)
          call h5_get_PRhorange(current_metal_tbl, log10T, &
               m_log10P1, m_log10P2, m_log10Rho1, m_log10Rho2, ierr)
          
          if(m_log10P1 .gt. log10P1) then
             log10P1 = m_log10P1
          endif
          
          if(m_log10P2 .lt. log10P2) then
             log10P2 = m_log10P2
          endif
          
!          write(*,*) "h5 logPrange: ", m_log10P1, m_log10P2

          if(ierr .ne. 0) then
             return
          endif
       endif
    end do
    
  end subroutine get_pressure_range
  
  subroutine build_PT_boundary_grid(ierr)
    use const_lib, only: dp, ln10
    use interp_1d_lib
    use mixeos_def
    implicit none
    
    integer, intent(out) :: ierr
    integer :: ierr_par = 0

    real(dp) :: T, dlogT, log10T, log10P1, log10P2
    integer :: i

    integer :: nwork = max(pm_work_size, mp_work_size)
    real(dp), target :: work_ary(NUMBOUNDTEMP*nwork)
    real(dp), pointer, dimension(:) :: work
    real(dp), dimension(NUMBOUNDTEMP) :: logPbound_for_minlog10T, logPbound_for_maxlog10T
    
    !! This flag specifies if the region of good values has been passed through
    integer :: valid_region = 0 

    integer :: start_index_bottom = 1, start_index_top = 1
    integer :: end_index_bottom = NUMBOUNDTEMP, end_index_top = NUMBOUNDTEMP
    
    work => work_ary
    
    ierr = 0
    dlogT = (mixeos_maxlog10T - mixeos_minlog10T) / (real(NUMBOUNDTEMP) - 1d0)
    do i=1,NUMBOUNDTEMP
       
       mixeos_logTbound_vect(i) = mixeos_minlog10T + dlogT * (real(i - 1))
       log10T = mixeos_logTbound_vect(i)
       T = 1d1**log10T
       ierr_par = 0
       ierr = 0
       call get_pressure_range( &
            T, log10T, log10P1, log10P2, ierr_par)
       
       if(ierr_par .eq. 0) then
          mixeos_min_logPbound_vect(i) = log10P1
          mixeos_max_logPbound_vect(i) = log10P2
          if(valid_region .eq. 0) then
             write(*,*) "FLIPPING SWITCH - VALID_REGION"
          endif
          valid_region = 1
          
       else
          if(valid_region .eq. 0) then
!             write(*,*) "Setting Pbound to ", mixeos_minlnP / ln10
             mixeos_min_logPbound_vect(i) = mixeos_minlnP / ln10
             mixeos_max_logPbound_vect(i) = mixeos_minlnP / ln10
          else
!             write(*,*) "Setting Pbound to ", mixeos_maxlnP / ln10
             mixeos_min_logPbound_vect(i) = mixeos_maxlnP / ln10
             mixeos_max_logPbound_vect(i) = mixeos_maxlnP / ln10
          end if

          write(*,*) "The temperature log10T = ", log10T, " is not really valid with this mixture"
          write(*,*) " adjust mixeos_def : mixeos_minlog10T or mixeos_maxlog10T such that this error does not occur"
          
       end if
       
       if(valid_region .eq. 0) then
          ierr = ierr_par
       endif
       
       mixeos_min_logPbound_interp(1,i) = mixeos_min_logPbound_vect(i)
       mixeos_max_logPbound_interp(1,i) = mixeos_max_logPbound_vect(i)
!       write(*,*) i, log10T, log10P1, log10P2, ierr_par


       !! This code assumes there is one contiguoous region that matters and the rest should be ignored
       if(i .gt. 1) then
          if(mixeos_min_logPbound_vect(i) .eq. (mixeos_min_logPbound_vect(i-1))) then
             if(start_index_top .eq. (i-1)) then
                start_index_top = i
             elseif(end_index_top .gt. i) then
                end_index_top = i - 1 
             endif
          endif

          if(mixeos_max_logPbound_vect(i) .eq. (mixeos_max_logPbound_vect(i-1))) then
             if(start_index_bottom .eq. (i-1)) then
                start_index_bottom = i
             elseif(end_index_bottom .gt. i) then
                end_index_bottom = i - 1
             endif
          endif
       endif
       
    enddo

    NUMTBOUNDBOTTOM = end_index_bottom - start_index_bottom + 1
    NUMTBOUNDTOP = end_index_top - start_index_top + 1

    if(associated(mixeos_logPbound_for_minlog10T)) then
       deallocate(mixeos_logPbound_for_minlog10T) 
       nullify(mixeos_logPbound_for_minlog10T)
    endif
    if(associated(mixeos_logPbound_for_maxlog10T)) then
       deallocate(mixeos_logPbound_for_maxlog10T)
       nullify(mixeos_logPbound_for_maxlog10T)
    endif
    if(associated(mixeos_max_logTbound_interp)) then
       deallocate(mixeos_max_logTbound_interp)
       nullify(mixeos_max_logTbound_interp)
    endif
    if(associated(mixeos_min_logTbound_interp)) then
       deallocate(mixeos_min_logTbound_interp)
       nullify(mixeos_min_logTbound_interp)
    endif
    
    allocate(mixeos_logPbound_for_minlog10T(NUMTBOUNDBOTTOM))
    allocate(mixeos_logPbound_for_maxlog10T(NUMTBOUNDTOP))
    allocate(mixeos_min_logTbound_interp(4,NUMTBOUNDBOTTOM))
    allocate(mixeos_max_logTbound_interp(4,NUMTBOUNDTOP))    

    mixeos_logPbound_for_minlog10T(1:NUMTBOUNDBOTTOM) = mixeos_max_logPbound_vect(start_index_bottom:end_index_bottom)
    mixeos_logPbound_for_maxlog10T(1:NUMTBOUNDTOP) = mixeos_min_logPbound_vect(start_index_top:end_index_top)
    mixeos_min_logTbound_interp(1,1:NUMTBOUNDBOTTOM) = mixeos_logTbound_vect(start_index_bottom:end_index_bottom)
    mixeos_max_logTbound_interp(1,1:NUMTBOUNDTOP) = mixeos_logTbound_vect(start_index_top:end_index_top)

    allocate(interp_work(NUMBOUNDTEMP, nwork))
    
    call interp_m3q(mixeos_logTbound_vect, NUMBOUNDTEMP, mixeos_min_logPbound_interp, nwork, interp_work, ierr)
    if(ierr .ne. 0) then
       write(*,*) "min logP(logT) interpolation failed"
    endif
    call interp_m3q(mixeos_logTbound_vect, NUMBOUNDTEMP, mixeos_max_logPbound_interp, nwork, interp_work, ierr)
    if(ierr .ne. 0) then
       write(*,*) "max logP(logT) interpolation failed"
    end if

    deallocate(interp_work)
    nullify(interp_work)


!    write(*,*) "Setting up minlogT boundary"
!    write(*,*) NUMTBOUNDBOTTOM, " points"
!    do i=1,NUMTBOUNDBOTTOM
!       write(*,*) mixeos_logPbound_for_minlog10T(i), mixeos_min_logTbound_interp(1,i)
!    enddo
    allocate(interp_work(NUMTBOUNDBOTTOM,nwork))
    call interp_pm(mixeos_logPbound_for_minlog10T, NUMTBOUNDBOTTOM, mixeos_min_logTbound_interp, nwork, interp_work, ierr)
    if(ierr .ne. 0) then
       write(*,*) "min logT(logP) interpolation failed"
    endif

    deallocate(interp_work)
    nullify(interp_work)

    allocate(interp_work(NUMTBOUNDTOP,nwork))

!    write(*,*) "maxlogT boundary"
!    write(*,*) NUMTBOUNDTOP, " points", start_index_top, end_index_top
!    do i=1,NUMTBOUNDTOP
!       write(*,*) mixeos_logPbound_for_maxlog10T(i), mixeos_max_logTbound_interp(1,i)
!    enddo
    
    call interp_pm(mixeos_logPbound_for_maxlog10T, NUMTBOUNDTOP, mixeos_max_logTbound_interp, nwork, interp_work, ierr)
    if(ierr .ne. 0) then
       write(*,*) "max logT(logP) interpolation failed"
    endif
    
    deallocate(interp_work)
    nullify(interp_work)

    write(*,*) "Finishing build_PT_boundary_grid"
    
  end subroutine build_PT_boundary_grid
  
  
  subroutine PT_find_domain(Xval, log10X, doing_P, log10Y1, log10Y2, ierr)
    use interp_1d_lib
    use mixeos_def
    use const_lib, only:dp, ln10
    implicit none

    !! Check to make sure that we are in a valid range
    integer, intent(in) :: doing_P !! 1 if X == P, 0 if X == T
    real(dp), intent(in) :: Xval, log10X
    real(dp), intent(out) :: log10Y1, log10Y2 
    integer, intent(out) :: ierr
    
    real(dp) :: log10T, log10P

    ierr = 0

    if(doing_P .eq. 1) then
       log10T = log10X
       if(log10T .lt. mixeos_minlog10T) then
          ierr = MIXEOSERR_MINTEMPERATURE
          return
       elseif(log10T .gt. mixeos_maxlog10T) then
          ierr = MIXEOSERR_MAXTEMPERATURE
          return
       endif
    else
       log10P = log10X
       if(log10P .lt. mixeos_logPbound_for_minlog10T(1)) then
          ierr = MIXEOSERR_MINPRESSURE
          return
       elseif(log10P .gt. mixeos_logPbound_for_minlog10T(NUMTBOUNDBOTTOM)) then
          ierr = MIXEOSERR_MAXPRESSURE
          return
       endif
    endif

    if(doing_P .eq. 1) then
       log10Y1 = mixeos_minlnP / ln10
       log10Y2 = mixeos_maxlnP / ln10

       log10T = log10X
       if((log10T .ge. mixeos_logTbound_vect(1)) &
            .and. (log10T .le. mixeos_logTbound_vect(NUMBOUNDTEMP))) then
          call interp_value(mixeos_logTbound_vect, NUMBOUNDTEMP, mixeos_min_logPbound_interp, log10T, log10P, ierr)
          if((ierr .eq. 0) .and. (log10P .gt. log10Y1)) then
             log10Y1 = log10P
          endif
          
          call interp_value(mixeos_logTbound_vect, NUMBOUNDTEMP, mixeos_max_logPbound_interp, log10T, log10P, ierr)
          if((ierr .eq. 0) .and. (log10P .lt. log10Y2)) then
             log10Y2 = log10P
          endif
       endif
    else
       log10Y1 = mixeos_minlog10T !! These values may be a bit crazy
       log10Y2 = mixeos_maxlog10T
       
       log10P = log10X
       if((log10P .ge. mixeos_logPbound_for_minlog10T(1)) &
            .and. (log10P .le. mixeos_logPbound_for_minlog10T(NUMTBOUNDBOTTOM))) then
          call interp_value(mixeos_logPbound_for_minlog10T, NUMTBOUNDBOTTOM, mixeos_min_logTbound_interp, log10P, log10T, ierr)
          if((ierr .eq. 0) .and. (log10T .gt. log10Y1)) then
             log10Y1 = log10T
          endif
!          write(*,*) "Inside of minlogT domain", mixeos_logPbound_for_minlog10T(1), mixeos_logPbound_for_minlog10T(NUMTBOUNDBOTTOM)
       else
          !! If it isn't in the domain then, we need to use the largest minimum from h5 and the eos
          write(*,*) "Outside of minlogT domain", mixeos_logPbound_for_minlog10T(1), mixeos_logPbound_for_minlog10T(NUMTBOUNDBOTTOM)
       endif
       
       if((log10P .ge. mixeos_logPbound_for_maxlog10T(1)) &
            .and. (log10P .le. mixeos_logPbound_for_maxlog10T(NUMTBOUNDTOP))) then
          call interp_value(mixeos_logPbound_for_maxlog10T,NUMTBOUNDTOP, mixeos_max_logTbound_interp, log10P, log10T, ierr)
          if((ierr .eq. 0) .and. (log10T .lt. log10Y2)) then
             log10Y2 = log10T
          endif
!          write(*,*) "Inside of maxlogT domain", mixeos_logPbound_for_maxlog10T(1), mixeos_logPbound_for_maxlog10T(NUMTBOUNDTOP)
       else
          !! If it isn't in the domain then we need to use the smallest, maximum from h5 and the eos
          write(*,*) "Outside of maxlogT domain", mixeos_logPbound_for_maxlog10T(1), mixeos_logPbound_for_maxlog10T(NUMTBOUNDTOP)
       endif
    endif
    

    log10Y1 = log10Y1 + BOUNDARY_BUFFER
    log10Y2 = log10Y2 - BOUNDARY_BUFFER
    
  end subroutine PT_find_domain


  subroutine PT_find_range(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       Xval, log10X, which_other, other_value, doing_P, &
       log10Y1, log10Y2, other1, other2, ierr)
    use interp_1d_lib
    use mixeos_def
    use const_lib, only:dp, ln10
    use eos_def


    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    integer, intent(in) :: doing_P !! 1 if X == P, 0 if X == T
    integer, intent(in) :: species
    integer, pointer, dimension(:) :: chem_id, net_iso
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: Xval, log10X
    real(dp), intent(out) :: log10Y1, log10Y2, other1, other2
    integer, intent(out) :: ierr
    

    real(dp), pointer, dimension(:) :: res, &
         d_dlnRho_c_T, d_dlnT_c_Rho
    integer :: ierr1 = -1, ierr2 = -1
    
    real(dp) :: T, log10T, P, log10P, Rho

    !! Maybe we use this for binomial search, maybe not
    real(dp) :: logY_test, other_test, dother_dlogY

    !! Use the relationship given from mixeos_min_logPbound_vect, mixeos_max_logPbound_vect
    !! to get a tighter boundaries here.  It is still likely that the values in the boundary
    !! are not actually valid points
    
    allocate(res(num_eos_basic_results))
    allocate(d_dlnRho_c_T(num_eos_basic_results))
    allocate(d_dlnT_c_Rho(num_eos_basic_results))

    call PT_find_domain(Xval, log10X, doing_P, log10Y1, log10Y2, ierr)
    
    if(ierr .ne. 0) then
       write(*,*) "[PT_find_range] PT_find_domain failed: ", ierr
       write(*,*) "Xval = ", Xval
       write(*,*) "log10X = ", log10X
       write(*,*) "log10Y1 = ", log10Y1
       write(*,*) "log10Y2 = ", log10Y2
    endif

!    write(*,*) "Determined logYrange: ", log10Y1, log10Y2

    !! Do we want to actually evaluate the end points?
    !! Or do we want to just do a search through bisection?
    call setup_variables(log10X, log10Y1)
    
    if(mixdbg) then
       write(*,*) "Setup variables low"
       write(*,*) " P : ", P, log10P
       write(*,*) " T : ", T, log10T
    endif
    if(.not. associated(net_iso)) then
       write(*,*) "[PT_find_range] net_iso not associated"
    endif

    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr1)
    
    if(ierr1 .eq. 0) then
       if(which_other .eq. 0) then
          other1 = log10(Rho)
       else
          other1 = res(which_other)       
       end if
    else
       ierr = ierr1
       write(*,*) "FAIL to find good bounds!!!"
       write(*,*) "ierr = ", ierr, " for min range"
    endif
    
!    write(*,*) "called get_res_PT"

    call setup_variables(log10X, log10Y2)
    
    if(mixdbg) then
       write(*,*) "Setup variables high"
       write(*,*) " P : ", P, log10P
       write(*,*) " T : ", T, log10T
    endif
    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr2)
    
    if(ierr2 .eq. 0) then
       if(which_other .eq. 0) then
          other2 = log10(Rho)
       else
          other2 = res(which_other)
       end if
    else
       ierr = ierr2
       write(*,*) "FAIL to find good bounds!!!"
       write(*,*) "ierr = ", ierr, " for max range"
    endif
    
    if(ierr .eq. 0) then
       if((other_value - other1) * (other_value - other2) .gt. 0d0) then
          !! The function did work, but the value is not between the two outer boundaries
          write(*,*) "PT_find_range not in range: ", ierr
          write(*,*) "other_value = ", other_value
          write(*,*) "other1 = ", other1
          write(*,*) "other2 = ", other2
          write(*,*) "which_other = ", which_other
          ierr = ERROR_OUTOFBOUND
       end if
    end if
    
    call free_local_mem

  contains
    subroutine setup_variables(log10X_input, log10Y_input)
      real(dp) :: log10X_input, log10Y_input
      if(doing_P .eq. 1) then
         log10P = log10Y_input
         P = 1d1**log10P
         log10T = log10X_input
         T = 1d1**log10T
      else
         log10P = log10X_input
         P = 1d1**log10P
         log10T = log10Y_input
         T = 1d1**log10T
      endif
      
    end subroutine setup_variables
    
    subroutine free_local_mem
      deallocate(res)
      deallocate(d_dlnRho_c_T)
      deallocate(d_dlnT_c_Rho)
    end subroutine free_local_mem

  end subroutine PT_find_range
  
  
  subroutine get_res_PT(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       P, log10P, T, log10T, Rho,  &
       res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
    use eos_def
    use eos_lib, only: eosPT_get, eosDT_get_Rho
    use mixeos_def
    use mixeos_private
    use h5table
    use h5table_params
    use thermo
    use const_def, only: arg_not_provided
    use chem_def
    
    implicit none
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in):: xa(:) !! mass fractions
    real(dp), intent(in) :: P, log10P, T, log10T    
    real(dp), intent(out) :: Rho
    real(dp), intent(out), pointer, dimension(:) :: res_output, &
         d_dlnRho_c_T_output, d_dlnT_c_Rho_output
    integer, intent(out) :: ierr
    
    real(dp), pointer, dimension(:) ::  & !! Need the target attribute here too
         Pvect, lnPvect, &
         Svect, lnSvect, &
         Evect, lnEvect
    real(dp), target, dimension(num_derivs, NUM_METALS+1) :: & !! Need the target attribute
         RhoMat_PT, VolMat_PT, SMat_PT, EMat_PT
    real(dp), dimension(num_derivs) ::  &  
         VolVect_mixPT, RhoVect_mixPT, EVect_mixPT, SVect_mixPT, &
         Pvect_mixRhoT, Evect_mixRhoT, Svect_mixRhoT, &
         lnPVect_mixRhoT, lnEVect_mixRhoT, lnSVect_mixRhoT
    
    real(dp) :: eos_res(num_eos_basic_results), &
         eos_dlnRho_dlnPgas_const_T, &
         eos_d_dlnRho_dlnT_const_Pgas, &
         eos_d_dlnRho_const_T(num_eos_basic_results), &
         eos_d_dlnT_const_Rho(num_eos_basic_results), &
         hhe_Rho, hhe_log10Rho, hhe_lnRho, lnP, lnT
    real(dp) :: hhe_Pvect(num_derivs), hhe_lnPvect(num_derivs), &
         hhe_Evect(num_derivs), hhe_lnEvect(num_derivs), &
         hhe_Svect(num_derivs), hhe_lnSvect(num_derivs)
    
    integer :: i
    type (h5tbl), pointer :: current_metal_tbl
    real(dp), pointer, dimension(:) :: current_Rhovect_PT, current_Evect_PT, current_Svect_PT
    
    !! X_i is the vector that is passed to AddVolMix
    !! The code below must ensure that the xa vector is correctly
    !! mapped to the X_i vector 
    real(dp), target, dimension(NUM_METALS+1) :: X_i  
    real(dp) :: lnRho, logRhoGuess, logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2
    real(dp) :: helm_res(num_helm_results)
    integer :: actual_eos_calls
    
    real(dp) :: Xsum_other, Xarg, Yarg, Zarg, abar_arg, zbar_arg
    real(dp) :: Xval, log10X, log10Y1, log10Y2
    integer :: doing_P

    integer :: cid
    real(dp) :: tmp_x(species), tmp_y(species), tmp_z(species), xsum_eos

    res_output(:) = 0d0
    d_dlnRho_c_T_output(:) = 0d0
    d_dlnT_c_Rho_output(:) = 0d0


    log10X = log10T
    Xval = T
    doing_P = 1
    call PT_find_domain(Xval, log10X, doing_P, log10Y1, log10Y2, ierr)
    if((log10Y1 .gt. log10P) .or. (log10Y2 .lt. log10P)) then
       write(*,*) "[get_res_PT] error ", log10Y1, log10Y2, log10P
       ierr = ERROR_OUTOFBOUND
       return
    endif
    

!    write(*,*) "Inside get_res_PT", num_derivs
    
    allocate(Pvect(num_derivs), lnPvect(num_derivs), Svect(num_derivs), lnSvect(num_derivs), Evect(num_derivs), lnEvect(num_derivs))
    
!    write(*,*) "variables allocated"

    
!    write(*,*) "Cleared out output variables"

    RhoMat_PT(:,:) = 1.d0
    EMat_PT(:,:) = 1.d0
    SMat_PT(:,:) = 1.d0
    X_i(:) = 0d0
    
!    write(*,*) "Zeroed out matrix"

    lnP = log(P)
    lnT = log(T)    
    !! Call the standard eosPT_get(P,T) -> Rho, results
    hhe_Rho = 0d0
    hhe_log10Rho = 0d0
    hhe_lnRho = 0d0
    eos_res(:) = 0d0
    eos_d_dlnRho_const_T(:) = 0d0
    eos_d_dlnT_const_Rho(:) = 0d0
        
!    write(*,*) "set eos_results to zero"

    X_i(1) = 0d0
!    write(*,*) net_iso(ih1), net_iso(ihe4)
    if(.not. associated(net_iso)) then
       write(*,*) "Net iso is not allocated"
    endif
    current_Rhovect_PT => RhoMat_PT(1,1:num_derivs)
    current_Evect_PT   => EMat_PT(1,1:num_derivs)
    current_Svect_PT   => SMat_PT(1,1:num_derivs)
    
    ierr = 0
    
    if(log10T .lt. mixeos_minlog10T) then
       ierr = MIXEOSERR_MINTEMPERATURE
       return
    endif
    
    if(log10T .gt. mixeos_maxlog10T) then
       ierr = MIXEOSERR_MAXTEMPERATURE
       write(*,*) "mixeos logT too big!! : logT = ", log10T, " limit ", mixeos_maxlog10T
       return
    endif
    
    Xsum_other = 0d0
    do i=1, ndef_metals
       if(net_iso(mixeos_chem_id(i)) .eq. 0) then
          X_i(i+1) = 0d0
       else
          X_i(i+1) =  xa(net_iso(mixeos_chem_id(i)))
          Xsum_other = Xsum_other + X_i(i+1)
       endif
    end do
    X_i(1) = 1d0 - Xsum_other
    
    
    if(X_i(1) .gt. 0d0) then

       !! Xarg, Yarg, Zarg is the composition for the sake of the "normal" eos
       !! It is important that a correct abar_arg and zbar_arg are calculated
       Xarg = xa(net_iso(ih1)) / X_i(1)
       Yarg = xa(net_iso(ihe4)) / X_i(1)
       Zarg = (1d0 - Xarg - Yarg)

       tmp_x(1:species) = 0d0
       tmp_y(1:species) = 0d0
       tmp_z(1:species) = 0d0
       do i=1,species
          cid = chem_id(i)
          if(which_eos(cid) .eq. 0) then             
             tmp_x(i) = xa(i)
             tmp_z(i) = chem_isos%Z(cid)
             tmp_y(i) = xa(i) / chem_isos% Z_plus_N(cid)
          endif
       enddo
       
       xsum_eos = sum(tmp_x(1:species))  !! This could be less than one in this case
       abar_arg = xsum_eos/sum(min(1d0,max(tmp_y(1:species),1.0d-50)))
       zbar_arg = abar * sum(tmp_y(1:species)*tmp_z(1:species))/xsum_eos
       
       !! Let me repeat
       !! IT IS ABSOLUTELY IMPORTANT THAT ALL OF THESE ARGUMENTS ARE RIGHT
       !! MAKE SURE THAT Z IS ZERO WHEN PASSED TO THE MESA EOS MODULE SINCE
       !! WE ARE PERFORMING ADDITIVE VOLUME.  I THINK THE ISSUE IS THAT THE
       !! ABAR AND ZBAR PARAMETERS AFFECT THE RESULTS OF THE TABLE AND IF THESE
       !! ARE MISCONFIGURED THEN YOU GET A PROBLEM.
       
       tmp_x = tmp_x * (1d0 / xsum_eos) !! This is a version of the xa vector
       !! that only includes the elements that are in the regular eos
       !! and is normalized

       !! if my calculation for abar and zbar don't work use the following
!       subroutine get_composition_info(
!     >      num_isos, chem_id, x, xh, xhe, 
!     >      abar, zbar, z2bar, ye, mass_correction,
!     >      sumx, skip_partials, dabar_dx, dzbar_dx, dmc_dx)
       call eosPT_get(eos_handle, Zarg, Xarg, abar_arg, zbar_arg, &
            species, chem_id, net_iso, tmp_x, &
            P, log10P, T, log10T, &
            hhe_Rho, hhe_log10Rho, eos_dlnRho_dlnPgas_const_T, eos_d_dlnRho_dlnT_const_Pgas, &
            eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, ierr)
       
       if(ierr .ne. 0) then
          write(*,*) "eosPT_get failed"
          write(*,*) "P input: ", P
          write(*,*) "log10P input: ", log10P
          write(*,*) "T input: ", T
          write(*,*) "log10T input: ", log10T
          return
       endif
       
!       write(*,*) "[get_res_PT] eosPT_get res: ", eos_res
!       write(*,*) "[get_res_PT] eosPT_get ierr: ", ierr
       
       hhe_lnRho = log(hhe_Rho)
       if(mixdbg) then
          write(*,*) "[Mixeos_mod]"
          write(*,*) "hhe_Rho: ", hhe_Rho
          write(*,*) "hhe_log10Rho: ", hhe_log10Rho
       endif
       
       call convRES_lnderivs(eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, &
            hhe_lnRho, lnT, hhe_lnPvect, hhe_lnEvect, hhe_lnSvect)
       call convlnDerivsDerivs(hhe_lnRho, lnT, hhe_lnPvect, hhe_lnEvect, hhe_lnSvect, &
            hhe_Pvect, hhe_Evect, hhe_Svect)
       call convRhoT_PT(hhe_Rho, T, P, &
            hhe_Pvect, hhe_Evect, hhe_Svect, & !! These have (Rho, T) independent
            current_Rhovect_PT, current_Evect_PT, current_Svect_PT)
       
!       write(*,*) "[H/He RhoVect] ", current_Rhovect_PT
    else
       !! These are weighted with zero weight
       !! It is good to have this be a non-zero number though so that
       !! if the pipeline inverts them we don't have NaN seeping in
       current_Rhovect_PT(:) = 1d0
       current_Evect_PT(:) = 1d0
       current_Svect_PT(:) = 1d0
    endif
    
    if(mixdbg) then
       if(ierr .ne. 0) then
          write(*,*) "We've got a problem"
       endif
    endif
    
    if(ierr .ne. 0) then
       return
    endif
    
    !! Call get(P,T) -> Rho, results for each of the component species
    metal_loop : do i=1, ndef_metals
       if(net_iso(mixeos_chem_id(i)) .ne. 0) then
          current_metal_tbl => metal_tbls(i)
          current_Rhovect_PT => RhoMat_PT((i+1), 1:num_derivs)
          current_Evect_PT   => EMat_PT((i+1), 1:num_derivs)
          current_Svect_PT   => SMat_PT((i+1), 1:num_derivs)
          
          if(X_i(i+1) .gt. 0.d0) then  
             !! Should we also check that the h5table is initialized???
             call h5_get_Rho(current_metal_tbl, P, T, Rho, Pvect, Evect, Svect, ierr)
             if(mixdbg) then
                write(*,*) "[mixeos/h5_get_Rho results]", i
                write(*,*) "P: ", P
                write(*,*) "T: ", T
                write(*,*) "Rho: ", Rho
                write(*,*) "Pvect: ", Pvect
                write(*,*) "Evect: ", Evect
                write(*,*) "Svect: ", Svect
                write(*,*) "ierr: ", ierr
             endif
             
             if(ierr .eq. 0) then
                call convRhoT_PT(Rho, T, P, &
                     Pvect, Evect, Svect, & 
                     current_Rhovect_PT, current_Evect_PT, current_Svect_PT)
             else
                current_Rhovect_PT(1:num_derivs) = 0d0
                current_Evect_PT(1:num_derivs) = 0d0
                current_Svect_PT(1:num_derivs) = 0d0
                exit metal_loop
                !! Since ierr /= 0, the code below should result in returning nothing
             endif
          endif
       end if
       
    enddo metal_loop
    
    if(ierr .eq. 0) then
       
       if(mixdbg) then
          write(*,*) "[mixeos PT deriv matrix]"
          write(*,*) "[mixeos PT/RhoMat] "
          do i=1,NUM_METALS+1
             write(*,*) RhoMat_PT(i,1:num_derivs)
          enddo
          write(*,*) "[mixeos_PT/EMat] "
          do i=1,NUM_METALS+1
             write(*,*) EMat_PT(i,1:num_derivs)
          enddo
          write(*,*) "[mixeos_PT/SMat] "
          do i=1,NUM_METALS+1
             write(*,*) SMat_PT(i,1:num_derivs)
          end do
       endif
       call convRhoMatVolMat(RhoMat_PT, VolMat_PT, NUM_METALS+1)
       if(mixdbg)  then
          write(*,*) "[mixeos_PT/VolMat]"
          do i=1,NUM_METALS+1
             write(*,*) VolMat_PT(i,1:num_derivs)
          enddo
       endif
       call AddVolMix(VolMat_PT, SMat_PT, EMat_PT, X_i, NUM_METALS+1, &
            VolVect_mixPT, EVect_mixPT, SVect_mixPT)
       if(mixdbg) then
          write(*,*) "[mixeos_PT/AddVolMix Vector] "
          write(*,*) "Vol: ", VolVect_mixPT
          write(*,*) "E: ", EVect_mixPT
          write(*,*) "S: ", SVect_mixPT
       endif
       
       call convVolVecRhoVec(VolVect_mixPT, RhoVect_mixPT)
       
       if(mixdbg) then
          write(*,*) "[mixeos_PT/RhoVect_PT]: ", RhoVect_mixPT
       endif
       
       Rho = RhoVect_mixPT(i_val)
       lnRho = log(Rho)

       if(mixdbg) then
          write(*,*) "Rho = ", Rho
          write(*,*) "lnRho = ", lnRho
       endif

       call convPT_RhoT(Rho, T, P, &
            Rhovect_mixPT, EVect_mixPT, SVect_mixPT, &
            PVect_mixRhoT, EVect_mixRhoT, SVect_mixRhoT)
       
       if(mixdbg) then
          write(*,*) "[mixeos_PT/Convert PT -> RhoT]"
          write(*,*) "PVecRhoT: ", PVect_mixRhoT
          write(*,*) "EvecRhoT: ", EVect_mixRhoT
          write(*,*) "SvecRhoT: ", SVect_mixRhoT
       endif
       
       call convDerivslnDerivs(Rho,T,PVect_mixRhoT, EVect_mixRhoT, SVect_mixRhoT, &
            lnPVect_mixRhoT, lnEVect_mixRhoT, lnSVect_mixRhoT)
       
       if(mixdbg) then
          write(*,*) "[mixeos_PT/Convert RhoT -> lnDerivsRhoT]"
          write(*,*) "lnPVectRhoT: ", lnPVect_mixRhoT
          write(*,*) "lnEVectRhoT: ", lnEVect_mixRhoT
          write(*,*) "lnSVectRhoT: ", lnSVect_mixRhoT
       endif
       
       call convlnderivs_RES(lnRho, lnT, &
            lnPvect_mixRhoT, lnEvect_mixRhoT, lnSvect_mixRhoT, &
            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output)
       
    else
       res_output(:) = 0d0
       d_dlnRho_c_T_output = 0d0
       d_dlnT_c_Rho_output = 0d0
    endif
    
    deallocate(Pvect, lnPvect, Evect, lnEvect, Svect, lnSvect)
    
  contains 
    subroutine print_res(res)
      use eos_lib, only: eos_result_names
      real(dp), intent(in) :: res(num_eos_basic_results)
      character (len=8) :: names(num_eos_basic_results)
      integer :: i
      call eos_result_names(names) 
      do i=1,num_eos_basic_results
         write(*,*) names(i), " = ", res(i)
      enddo
    end subroutine print_res
  end subroutine get_res_PT
  
  
  !! Root finding routine that calls get_res_PT
  !! lnP is the independent coordinate
  !! Generalize this routine so that it works for any eos value - 
  !! if the which_other = 0, then Val is lnRho
  
  real(dp) function PT_DVal(log10X,dValdlog10X,lrpar,rpar,lipar,ipar,ierr)
    use mixeos_def    
    use eos_def, only: num_eos_basic_results, i_lnPgas
    use chem_def, only: num_chem_isos
    use h5table_params
    use const_lib, only: ln10
    implicit none
    real(dp), intent(in) :: log10X
    real(dp), intent(out) :: dValdlog10X
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(in) :: lrpar, lipar
    integer, intent(out) :: ierr
    
    integer :: eos_handle
    real(dp) :: Z, X,abar,zbar
    integer :: species
    integer, pointer, dimension(:) :: chem_id, net_iso
    real(dp), pointer :: xa(:)
    
    real(dp) :: log10P, P, T, log10T, other_value, log10var2, var2
    real(dp), pointer :: Rho
    real(dp), pointer, dimension(:) :: &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output
    
    integer :: which_other = 0
    integer :: doing_P
    integer :: i = 1 !! integer index 
    integer :: j = 1 !! double index
    
    ierr = 0

    !! Get variables from ipar {
    i = 1
    eos_handle = ipar(i) ; i = i+1
    which_other = ipar(i) ; i = i+1
    species = ipar(i) ; i = i+1
    doing_P = ipar(i) ; i = i+1
    chem_id => ipar(i:(i+species-1)) ; i = i+species
    net_iso => ipar(i:(i+num_chem_isos-1)) ; i = i+num_chem_isos
    !! }

    !! Get input variables from rpar {
    j = 1
    Z = rpar(j) ; j = j+1
    X = rpar(j) ; j = j+1
    abar = rpar(j) ; j = j+1
    zbar = rpar(j) ; j = j+1
    var2 = rpar(j) ; j = j+1    
    log10var2 = log10(var2)
    other_value = rpar(j) ; j = j+1
    xa => rpar(j:(j+species-1)) ; j = j+species
    Rho => rpar(j) ; j = j+1
    res_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    d_dlnRho_c_T_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    d_dlnT_c_Rho_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    !! }       

!    write(*,*) "[PT_DVal] doing_P = ", doing_P

    if(doing_P .eq. 1) then
       P = 1d1**log10X
       log10P = log10X

       T = var2
       log10T = log10var2
    else
       T = 1d1**log10X
       log10T = log10X

       P = var2
       log10P = log10var2
    endif
    
!    write(*,*) "[PT_DVAL] -> get_res_PT"
!    write(*,*) "   P = ", P, " ; log10P = ", log10P
!    write(*,*) "   T = ", T, " ; log10T = ", log10T
    
    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho,  &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
    


!    write(*,*) "  ierr = ", ierr
!    write(*,*) "  res_output: ", res_output
    
    if(ierr .ne. 0) then
       dValdlog10X = 1.
       write(*,*) "[PT_DVAL]: Why is this happening?"
       if(ierr .eq. H5TBLERR_MINPRESSURE) then
          PT_DVal = -1d0
       else
          PT_DVal = 1d0
       endif
    else
       if(doing_P .eq. 1) then
          if(which_other .eq. 0) then
             dValdlog10X = 1d0 / d_dlnRho_c_T_output(i_lnPgas)
             PT_DVal = log10(Rho) - other_value
          else
             PT_DVal = res_output(which_other) - other_value
             dValdlog10X = ln10 * d_dlnRho_c_T_output(which_other) / d_dlnRho_c_T_output(i_lnPgas)
          endif
       else
          if(which_other .eq. 0) then
             PT_DVal = log10(Rho) - other_value
             dValdlog10X = - d_dlnT_c_Rho_output(i_lnPgas) / d_dlnRho_c_T_output(i_lnPgas)
          else
             PT_DVal = res_output(which_other) - other_value
             dValdlog10X = ln10 * ((-d_dlnT_c_Rho_output(i_lnPgas) / d_dlnRho_c_T_output(i_lnPgas)) * &
                  d_dlnRho_c_T_output(which_other) + d_dlnT_c_Rho_output(which_other))
          endif
       end if
    endif
  end function PT_DVal
  
  subroutine get_res(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       Rho, log10Rho, T, log10T, &
       res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    use chem_def
    use eos_def
    use mixeos_def
!    use thermo, only: convlnderivs_RES, convDerivslnDerivs
    use num_lib
    implicit none
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: Rho, log10Rho, T, log10T
    real(dp), intent(out) :: res(num_eos_basic_results)
    ! partial derivatives of the basic results wrt lnd and lnT
    ! d_dlnRho_c_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results) 
    ! d_dlnT(i) = d(res(i))/dlnT|Rho
    real(dp),  intent(out) :: d_dlnT_c_Rho(num_eos_basic_results) 
    
    integer, intent(out) :: ierr
    
    real(dp) :: lnRho, lnT, dlnRhodlnP
    real(dp) ::  log10Pguess, log10P1, log10P2, other1, other2, other_value
    
    integer :: lrpar, lipar, which_other
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, log10P, tmp_Rho
    integer :: doing_P = 1
    

    !! Overall algorithm
    !! 1. Look up the pressure range 

    which_other = 0
    other_value = log10Rho
    
    if(.not. associated(net_iso)) then
       write(*,*) "[mixeos_mod/get_res] net_iso not associated"
    endif

    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         T, log10T, which_other, other_value, doing_P, &
         log10P1, log10P2, other1, other2, ierr)
    
    if(ierr .ne. 0) then
       write(*,*) "[get_res] PT_find_range failed with ierr = ", ierr
    endif

!    write(*,*) "[get_res] PT_find_range results: "
!    write(*,*) "  log10P1 = ", log10P1
!    write(*,*) "  log10P2 = ", log10P2
!    write(*,*) "  other1 = ", other1
!    write(*,*) "  other2 = ", other2
!    write(*,*) "  ierr = ", ierr
!    write(*,*)

    !! 2. call a safe root find procedure
    if(ierr .eq. 0) then !! Only proceed if there is no error condition
       
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       
       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       
       lnRho = log(Rho)

       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = 0 ; i = i+1 !! Which_other = 0
       ipar(i) = species ; i = i+1
       ipar(i) = doing_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
       
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = T ; j = j+1
       rpar(j) = log10Rho ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       
       
       log10PGuess = (log10P1 + log10P2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
!       write(*,*) "PT_get_P: Calling safe root find: ", log10PGuess, log10P1, log10P2, dVal1, dVal2
       log10P = safe_root_with_initial_guess(PT_DVal, log10PGuess, log10P1, log10P2, dVal1, dVal2, &
            mixeos_getRhoT_maxiter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       tmp_Rho = rpar(j) ; j = j+1
       res = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnRho_c_T = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnT_c_Rho = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results

!       write(*,*) "[get_res] res: ", res
!       write(*,*) "  ierr = ", ierr

       deallocate(rpar)
       deallocate(ipar)       
    endif

  end subroutine get_res

  subroutine get_Rho(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       log10T, which_other, other_value, &
       logRho_tol, other_tol, max_iter, logRho_guess,  &
       logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
       logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
       ierr)    
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: arg_not_provided, dp
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: log10T ! log10 of temperature

    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logRho_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logRho_guess ! log10 of density
    real(dp), intent(in) :: logRho_bnd1, logRho_bnd2 ! bounds for logRho
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    real(dp) :: log10P1, log10P2, lnRho_bnd1, lnRho_bnd2
    
    real(dp), intent(out) :: logRho_result ! log10 of density

    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: Rho, T

    real(dp) :: logPgas_tol , logPgas_result
    real(dp) :: helm_res(num_helm_results)

    integer :: doing_P = 1

    ierr = 0
    logPgas_tol = logRho_tol

    call PT_get_P(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, other_tol, logPgas_tol, &
         max_iter, logPgas_result, Rho, logRho_result, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, helm_res, eos_calls, ierr)
    
  end subroutine get_Rho
  
  subroutine get_T(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       log10Rho, which_other, other_value, &
       logT_tol, other_tol, max_iter, logT_guess, & 
       logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
       logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: arg_not_provided
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: log10Rho ! log10 of density
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        
    
    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logical, parameter :: doing_Rho = .false.
    
    real(dp) :: res1(num_eos_basic_results), d_dlnRho_c_T1(num_eos_basic_results), d_dlnT_c_Rho1(num_eos_basic_results)
    real(dp) :: res2(num_eos_basic_results), d_dlnRho_c_T2(num_eos_basic_results), d_dlnT_c_Rho2(num_eos_basic_results)
    real(dp) :: Rho, T, log10T, local_other_at_bnd1, local_other_at_bnd2, local_logT_bnd1, local_logT_bnd2

    ierr = 0

    !! Determine values at boundaries other_value (logTlow, logThigh)
    !! 
    !! Run do_safe_get_Rho_T

    if(logT_bnd1 .eq. arg_not_provided) then
!       local_logT_bnd1 = accessor%minlog10T
    else
       local_logT_bnd1 = logT_bnd1
    endif

    if(logT_bnd2 .eq. arg_not_provided) then
!       local_logT_bnd2 = accessor%maxlog10T
    else
       local_logT_bnd2 = logT_bnd2
    endif
    
    if(other_at_bnd1.eq. arg_not_provided) then
       Rho = 10d0**log10Rho
       T = 10d0**local_logT_bnd1
       log10T = local_logT_bnd1
       call get_res(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res1, d_dlnRho_c_T1, d_dlnT_c_Rho1, ierr)
       local_other_at_bnd1 = res1(which_other)
    else
       local_other_at_bnd1 = other_at_bnd1
    endif
    
    if(other_at_bnd2 .eq. arg_not_provided) then
       Rho = 10d0**log10Rho
       T = 10d0**local_logT_bnd2
       log10T = local_logT_bnd2
       call get_res(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res2, d_dlnRho_c_T2, d_dlnT_c_Rho2, ierr)
       local_other_at_bnd2 = res2(which_other)
    else
       local_other_at_bnd2 = other_at_bnd2
    endif
    
    if(((local_other_at_bnd1.lt.other_value).and.(local_other_at_bnd2.gt.other_value)).or. &
         ((local_other_at_bnd1.gt.other_value).and.(local_other_at_bnd2.lt.other_value))) then
    
!       logT_guess = (local_logT_bnd1 + local_logT_bnd2) / 2.d0
    
       call do_safe_get_Rho_T(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10Rho, &
            which_other, other_value, doing_Rho, &
            logT_guess, logT_result, &
            local_logT_bnd1, local_logT_bnd2, local_other_at_bnd1, local_other_at_bnd2, &
            logT_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    else
       write(*,*) "target other_value: ", other_value
       write(*,*) "other1: ", local_other_at_bnd1
       write(*,*) "other2: ", local_other_at_bnd2
       ierr = ERROR_OUTOFBOUND
    endif
  end subroutine get_T
  
  !! This routine is called by get_Rho and get_T
  subroutine do_safe_get_Rho_T( &
       eos_handle, Zcomp, Xcomp, abar, zbar, &
       species, chem_id, net_iso, xa, &
       the_other_log, & 
       which_other, other_value, doing_Rho, & 
       initial_guess, x, xbnd1, xbnd2, other_at_bnd1, other_at_bnd2, & 
       xacc, yacc, ntry, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use chem_def
    use eos_def
    use num_lib
    use const_def
    use mixeos_def
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Zcomp, Xcomp, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    logical, intent(in) :: doing_Rho
    real(dp), intent(in) :: initial_guess ! for x
    real(dp), intent(out) :: x ! if doing_Rho, then logRho, else logT
    real(dp), intent(in) :: the_other_log
    real(dp), intent(in) :: xbnd1, xbnd2, other_at_bnd1, other_at_bnd2
    real(dp), intent(in) :: xacc, yacc ! tolerances
    integer, intent(in) :: ntry ! max number of iterations        
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    integer :: i, j, lrpar, lipar
    real(dp), parameter :: dx = 0.1d0
    integer, pointer :: ipar(:)
    real(dp), pointer :: rpar(:)
    real(dp) :: rho, T, xb1, xb3, y1, y3, dfdx, f, logRho, logT

    ierr = 0

    eos_calls = 0
    x = initial_guess
    
    if (doing_Rho) then
       rho = arg_not_provided
       T = 10**the_other_log
    else
       T = arg_not_provided
       rho = 10**the_other_log
    end if
    
    lipar = n_iparams + species + num_chem_isos
    lrpar = n_rparams + species + num_eos_basic_results*3
    allocate(ipar(lipar),rpar(lrpar))
    if(doing_Rho) then
       ipar(i_rho_flag) = 1
    else
       ipar(i_rho_flag) = 0
    endif
    ipar(i_which_other) = which_other
    ipar(i_handle) = eos_handle
    ipar(i_species) = species
    ipar(i_eos_calls) = eos_calls   !! set to zero above
    i = n_iparams + 1
    ipar(i:(i+species-1)) = chem_id(1:species) ; i = i + species
    ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos


    rpar(i_other) = other_value
    rpar(i_fixed_log_input) = the_other_log
    rpar(i_Z) = Zcomp
    rpar(i_X) = Xcomp
    rpar(i_abar) = abar
    rpar(i_zbar) = zbar
    i = n_rparams + 1
    rpar(i:(i+species-1)) = xa

    xb1 = xbnd1; xb3 = xbnd2
    y1 = other_at_bnd1 - other_value
    y3 = other_at_bnd2 - other_value
    
    x = safe_root_with_initial_guess(  &
         get_f_df, initial_guess, xb1, xb3, y1, y3, ntry,  &
         xacc, yacc, lrpar, rpar, lipar, ipar, ierr)
    if (ierr /= 0) then
       write(*, *) 'do_safe_get_Rho_T: safe_root returned ierr', ierr
!       write(*, *) trim(alert_message)
       return
    end if
    
    i = n_rparams
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    eos_calls = ipar(i_eos_calls)
    
    deallocate(ipar, rpar)

  end subroutine do_safe_get_Rho_T

  !! This is the function that is passed to safe_root_with_initial_guess
  real(dp) function get_f_df(x, dfdx, lrpar, rpar, lipar, ipar, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: ln10
    implicit none

    integer, intent(in) :: lrpar, lipar
    real(dp), intent(in) :: x
    real(dp), intent(out) :: dfdx
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(out) :: ierr    

    logical :: doing_Rho
    integer :: which_other, eos_handle, i, species
    real(dp) :: Zcomp, Xcomp, abar, zbar
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer :: xa(:)
    real(dp), pointer, dimension(:) :: res, d_dlnRho_c_T, d_dlnT_c_Rho
    real(dp) :: Rho, T, log10Rho, log10T, other, fixed_log_input
    
    ierr = 0
    
    doing_Rho = (ipar(i_rho_flag) /= 0)
    which_other = ipar(i_which_other)
    eos_handle = ipar(i_handle)
    species = ipar(i_species)
    i = n_iparams + 1
    chem_id => ipar(i:(i+species-1)) ; i = i+species
    net_iso => ipar(i:(i+num_chem_isos-1)) ; i = i+num_chem_isos
    
    i = 0
    other = rpar(i_other) 
    fixed_log_input = rpar(i_fixed_log_input)
    Zcomp = rpar(i_Z)
    Xcomp = rpar(i_X)
    abar = rpar(i_abar)
    zbar = rpar(i_zbar)
    
    
    if(doing_Rho) then
       log10Rho = x
       Rho = 1d1**log10Rho
       log10T = fixed_log_input
       T = 1d1**log10T !arg_not_provided
    else
       log10T = x
       T = 1d1**log10T
       log10Rho = fixed_log_input
       Rho = 1d1**log10Rho !arg_not_provided
    endif
    
    call get_res( &
         eos_handle, Zcomp, Xcomp, abar, zbar, &
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    if (ierr /= 0) then
       return
    end if
    ipar(i_eos_calls) = ipar(i_eos_calls)+1
    
    i = n_rparams
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results



    get_f_df = res(which_other) - other
    
    if (doing_Rho) then
       dfdx = d_dlnRho_c_T(which_other)*ln10
    else
       dfdx = d_dlnT_c_Rho(which_other)*ln10
    end if
    
  end function get_f_df

  !! This function takes (Temperature, and an other quantity and tries to find Pressure)  
  subroutine PT_get_P(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logT, which_other, other_value, other_tol, logPgas_tol, &
       max_iter, & !logPgas_guess, logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2, &
       logPgas_result, Rho, log10Rho, res, d_dlnRho_const_T, d_dlnT_const_Rho, helm_res, &
       eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_lib, only: dp, arg_not_provided
    use num_lib
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: logT, other_value, other_tol, logPgas_tol ! , &
!         logPgas_guess, logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2
    integer, intent(in) :: which_other, max_iter
    
    real(dp), intent(out) :: logPgas_result, Rho, log10Rho
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_const_T(num_eos_basic_results), &
         d_dlnT_const_Rho(num_eos_basic_results)
    real(dp), intent(out) :: helm_res(num_helm_results)
    integer, intent(out) :: eos_calls, ierr
    
    real(dp) :: T, log10T, log10P, lnP1, lnP2, log10P1, log10P2, other1, other2
    
    integer :: lrpar, lipar
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, log10PGuess
    integer :: doing_P = 1

    !! Overall algorithm
    !! 1. Look up the pressure range 
    T = 1d1**logT
    log10T = logT
    
    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         T, log10T, which_other, other_value, doing_P, &
         log10P1, log10P2, other1, other2, ierr)
    
    !! 2. call a safe root find procedure
    if(ierr .eq. 0) then !! Only proceed if there is no error condition
       
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       

       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       

       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = which_other ; i = i+1
       ipar(i) = species ; i = i+1
       ipar(i) = doing_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
       
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = T ; j = j+1
       rpar(j) = other_value ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       

       log10PGuess = (log10P1 + log10P2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
!       write(*,*) "PT_get_P: Calling safe root find: ", log10PGuess, log10P1, log10P2, dVal1, dVal2
       log10P = safe_root_with_initial_guess(PT_DVal, log10PGuess, log10P1, log10P2, dVal1, dVal2, &
            mixeos_getRhoT_maxiter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       Rho = rpar(j) ; j = j+1
       log10Rho = log10(Rho)
       res = rpar(j:(j+num_eos_basic_results-1))   ;   j = j+num_eos_basic_results
       d_dlnRho_const_T = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnT_const_Rho = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results

       deallocate(rpar)
       deallocate(ipar)       
    endif
    
  end subroutine PT_get_P


  !! There may be a more optimal way of solving this problem
  !!  The method I have in mind here is to use 
  subroutine PT_get_T(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logP, which_other, other_value, other_tol, logT_tol, &
       max_iter, & ! logT_guess, logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
       logT_result, Rho, log10Rho, res, d_dlnRho_const_T, d_dlnT_const_Rho, helm_res, &
       eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_lib, only: dp, arg_not_provided
    use num_lib
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: logP, other_value, other_tol, logT_tol ! , &
                  ! logT_guess, logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2
    integer, intent(in) :: which_other, max_iter
    
    real(dp), intent(out) :: logT_result, Rho, log10Rho
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_const_T(num_eos_basic_results), &
         d_dlnT_const_Rho(num_eos_basic_results)
    real(dp), intent(out) :: helm_res(num_helm_results)
    integer, intent(out) :: eos_calls, ierr

    integer :: doing_P = 0

    integer :: lrpar, lipar
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, lnRho
    real(dp) :: P, log10P, log10Tguess, log10T, log10T1, log10T2, other1, other2

    P = 1d1**logP
    log10P = logP
    
    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, which_other, other_value, doing_P, &
         log10T1, log10T2, other1, other2, ierr)
    
    
    !! 2. call a safe root find procedure
    if(ierr .eq. 0) then !! Only proceed if there is no error condition
       
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       
       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       
       lnRho = log(Rho)

       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = which_other ; i = i+1
       ipar(i) = species ; i = i+1
       ipar(i) = doing_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
       
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = P ; j = j+1
       rpar(j) = other_value ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       

       log10TGuess = (log10T1 + log10T2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
       log10T = safe_root_with_initial_guess(PT_DVal, log10TGuess, log10T1, log10T2, dVal1, dVal2, &
            max_iter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       logT_result = log10T
       Rho = rpar(j) ; j = j+1
       log10Rho = log10(Rho)
       res = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnRho_const_T = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results
       d_dlnT_const_Rho = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results
    endif

  end subroutine PT_get_T

end module mixeos_mod
