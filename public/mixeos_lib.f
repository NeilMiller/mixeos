!! On a matter of style:
!!  The *_lib.f files generally don't include the implementation of the code.
!!  See the private/mixeos_mod.f where many of these routines are actually implemented
!!  Part of the thought here is that we can prevent the user from misusing code by hiding
!!  the implementation from them

module mixeos_lib
  use const_lib, only: dp
  implicit none
  
contains
  
  subroutine mixeos_init(ierr)
    use mixeos_def
    use chem_def, only: num_chem_isos
    
    implicit none
    
    integer, intent(out) :: ierr
    
    ierr = 0
    
  end subroutine mixeos_init  

  !! Equation of State Adding routines.
  !! The user chooses an equation of state implementation depending on
  !! which mixeos_add_XXX routine is called.  
  !! ---------
  !! Make sure that name is the same as in the isotopes file
  subroutine mixeos_add_h5tbl(Xsize, Ysize, filename, name, &
       minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta, ierr)
    use mixeos_def
    use mixeos_mod
    use base_eos_mod
    use h5table_eos_mod
    use chem_lib, only: get_nuclide_index
    use chem_def, only: nuclide_not_found
    use const_def, only: ln10
    implicit none
    
    integer, intent(in) :: Xsize, Ysize
    character (len=256), intent(in) :: filename, name
    integer, intent(out) :: ierr
    type (h5table_eos), pointer :: metaltbl
    class (base_eos), pointer :: eos_tbl
    integer :: isotope_number
    real(dp), intent(in) :: minlogRhoAlpha, minlogRhoBeta, &
         maxlogRhoAlpha, maxlogRhoBeta
    
    ierr = 0
    
    allocate(metaltbl)
    call metaltbl%Construct(Xsize, Ysize, filename, name, ierr)
    call metaltbl%slice_h5table(minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)
    isotope_number = get_nuclide_index(trim(name))
    write(*,*) "Adding [", trim(name), "] at ", isotope_number
    
    eos_tbl => metaltbl
    call mixeos_add_eos(eos_tbl, isotope_number, ierr)
    
  end subroutine mixeos_add_h5tbl
  
  subroutine mixeos_add_ideal(mu, name)
    use mixeos_def
    use mixeos_mod
    use base_eos_mod
    use ideal_eos_mod
    use chem_lib, only: get_nuclide_index
    use chem_def, only: nuclide_not_found
    use const_def

    real(dp), intent(in) :: mu
    character (len=256), intent(in) :: name
    type(ideal_eos), pointer :: gas_tbl
    class (base_eos), pointer :: eos_tbl
    
    integer :: ierr, isotope_number
    
    allocate(gas_tbl)
    call gas_tbl%Construct(mu)
    call gas_tbl%set_name(name)
    isotope_number = get_nuclide_index(trim(name))
    write(*,*) "Adding ideal eos [", trim(name), "] at ", isotope_number
    
    eos_tbl => gas_tbl
    call mixeos_add_eos(eos_tbl, isotope_number, ierr)
    
  end subroutine mixeos_add_ideal

  !! TODO in the future
  !! 1. mixeos_add_ANEOS
  !!    A module/object should be built that calls ANEOS directly instead of using these tables
  !!    this may require modifying the ANEOS code to output higher order derivatives
  !!    of the free energy expression.  I did not take the time to understand this
  !! 2. mixeos_add_3tbl
  !!    A module/object should be written that takes the 3 table approach
  !!    lnP, lnE, lnS over a uniform grid in lnRho, lnT space and then outputs
  !!    the first and second derivatives.
  !! 
  !! 012 Any of these tables can be improved by having Pressure lookup tables implemented
  !!     such that we can find the Rho(P,T) in linear time rather than doing a rootfind.
  !!
  !! 3. represent the tables in the gibbs free energy framework
  !!    
  !! See private/base_eos_mod.f for the parent class
  !! See private/h5table_eos_mod.f for a template of the derived class
  !!  or private/ideal_eos_mod.f
  
  subroutine mixeos_shutdown
    use mixeos_mod

    integer :: i
    
    write(*,*) "mixeos_shutdown"
    call mixeos_clean_list
    
  end subroutine mixeos_shutdown
  
  !! Using the Equation of State
  !! --------------------------------------------------------------------
  subroutine mixeos_get( & 
       id, k, eos_handle, Z, X, abar, zbar, & 
       species, chem_id, net_iso, xa, &
       Rho, log10Rho, T, log10T, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    use mixeos_def
    use mixeos_mod, only: get_res
    use eos_def
    use chem_def, only: num_chem_isos
    use const_def, only: arg_not_provided
    implicit none
    
    integer, intent(in) :: id, k, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: Rho, T, log10Rho, log10T   
    real(dp), intent(out) :: res(:)
    ! partial derivatives of the basic results wrt lnd and lnT
    ! d_dlnRho_c_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnRho_c_T(:) 
    ! d_dlnT(i) = d(res(i))/dlnT|Rho
    real(dp), intent(out) :: d_dlnT_c_Rho(:) 
    integer, intent(out) :: ierr
    
    real(dp) :: Rhoarg, log10Rhoarg, Targ, log10Targ
    
    ierr = 0
    
    if(log10Rho.ne.arg_not_provided) then
       Rhoarg = 1d1**log10Rho
       log10Rhoarg = log10Rho
    elseif(Rho.ne.arg_not_provided) then
       Rhoarg = Rho
       log10Rhoarg = log10(Rho)
    else
       ierr = MIXEOSERR_ARGS
       return
    endif
    if(log10T.ne.arg_not_provided) then
       Targ = 1d1**log10T
       log10Targ = log10T
    elseif(T.ne.arg_not_provided) then
       Targ = T
       log10Targ = log10(T)
    else
       ierr = MIXEOSERR_ARGS
       return
    endif
    
    call do_othereosDT_get(xa)
    if(mixdbg0) then
       write(*,*) "mixeos_get", Rho, T, res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if

  contains
      
    subroutine do_othereosDT_get(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)
      
!      call display_chem_info(chem_id_ptr, net_iso_ptr)

!    write(*,*) "Calling mixeos_mod%get_res"
      call get_res(eos_handle, Z, X, abar, zbar, &
           species, chem_id, net_iso, xa_ptr, &
           Rhoarg, log10Rhoarg, Targ, log10Targ,  &
           res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
      !    write(*,*) "res: ", res
    end subroutine do_othereosDT_get
    
  end subroutine mixeos_get
  
  subroutine mixeos_get_rho( &
       id, k, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logT, which_other, other_value, &
       logRho_tol, other_tol, max_iter, logRho_guess,  &
       logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
       logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
       ierr)
    use eos_def
    use const_def, only: arg_not_provided
    use mixeos_def
    use mixeos_mod, only: get_Rho, get_res_PT
    use chem_def, only: num_chem_isos
    implicit none
    
    integer, intent(in) :: id, eos_handle, k

    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
            
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    real(dp), intent(in) :: logT ! log10 of temperature

    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logRho_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logRho_guess ! log10 of density
    real(dp), intent(in) :: logRho_bnd1, logRho_bnd2 ! bounds for logRho
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logRho_result ! log10 of density

    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_c_T(:)
    real(dp), intent(out) :: d_dlnT_c_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: P, T, lnP, log10P, Rho

    ierr = 0


    call do_othereosDT_get_rho(xa)
    if(mixdbg0) then
       write(*,*) "mixeos_get_rho", other_value, logT, logRho_result, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if

  contains
    
    subroutine do_othereosDT_get_rho(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)

      !! Treat the pressure case specially
      if(which_other .eq. i_lnPgas) then 
         lnP = other_value
         P = exp(other_value)
         log10P = log10(P)
         T = 1d1**logT
       

         call get_res_PT(eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa_ptr, &
              P, log10P, T, logT, Rho, &
              res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
         logRho_result = log10(Rho)
      else    
         call get_Rho(eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa_ptr, &
              logT, which_other, other_value, &
              logRho_tol, other_tol, max_iter, logRho_guess,  &
              logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
              logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
              ierr)
      end if
    end subroutine do_othereosDT_get_rho
  end subroutine mixeos_get_rho

  subroutine mixeos_get_t( &
               id, k, eos_handle, Z, X, abar, zbar, &
               species, chem_id, net_iso, xa, &
               logRho, which_other, other_value, &
               logT_tol, other_tol, max_iter, logT_guess, & 
               logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
               logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use eos_def
    use const_def, only: arg_not_provided
    use mixeos_def
    use mixeos_mod, only: get_T
    use chem_def, only: num_chem_isos
    
    implicit none
    
    integer, intent(in) :: id, k, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
            
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logRho ! log10 of density
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        
    
    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_c_T(:)
    real(dp), intent(out) :: d_dlnT_c_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    ierr = 0

    call do_othereosDT_get_T(xa)
    if(mixdbg0) then
       write(*,*) "mixeos_get_T", logRho, other_value, logT_result, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if
  contains
    subroutine do_othereosDT_get_T(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)

      call get_T(eos_handle, Z, X, abar, zbar, &
           species, chem_id, net_iso, xa_ptr, &
           logRho, which_other, other_value, &
           logT_tol, other_tol, max_iter, logT_guess, & 
           logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
           logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    end subroutine do_othereosDT_get_T
  end subroutine mixeos_get_t

  ! the following routine uses gas pressure and temperature as input variables
  subroutine mixeosPT_get(&
       id, k, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       Pgas, log10Pgas, T, log10T, &
       Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
    
    use eos_def
    use chem_def, only: num_chem_isos
    use mixeos_def
    use mixeos_mod
    implicit none

    ! INPUT
    
    integer, intent(in) :: id, k, eos_handle

    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: Pgas, log10Pgas ! the gas pressure
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    ! "arg_not_provided" is defined in mesa const_def
    
    real(dp), intent(in) :: T, log10T ! the temperature
    ! provide both if you have them.  else pass one and set the other to arg_not_provided
    
    ! OUTPUT
    
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    real(dp), intent(out) :: res(:)
    ! partial derivatives of the basic results wrt lnd and lnT
    real(dp), intent(out) :: d_dlnRho_const_T(:) 
    ! d_dlnRho_const_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnT_const_Rho(:) 
    ! d_dlnT_const_Rho(i) = d(res(i))/dlnT|Rho
    
    integer, intent(out) :: ierr ! 0 means AOK.
    
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0

    call do_mixeosPT_get(xa)
    if(mixdbg0) then
       write(*,*) "mixeosPT_get", Pgas, T, Rho, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if

  contains

    subroutine do_mixeosPT_get(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)

      call get_res_PT(eos_handle, Z, X, abar, zbar, &
           species, chem_id, net_iso, xa_ptr, &
           Pgas, log10Pgas, T, log10T, Rho, &
           res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
      log10Rho = log10(Rho)
      
      dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
      dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
      if(ierr .ne. 0) then
         write(*,*) "mixeosPT_get error"
         write(*,*) "  log10P = ", log10Pgas
         write(*,*) "  log10T = ", log10T
         write(*,*) "  ierr   = ", ierr
      endif
    end subroutine do_mixeosPT_get
  end subroutine mixeosPT_get
  
  
  ! eosPT search routines
  !! This is the hard one that I have to figure out how I want to do...
  subroutine mixeosPT_get_T( &
       id, k, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logPgas, which_other, other_value,&
       logT_tol, other_tol, max_iter, logT_guess, &
       logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2,&
       logT_result, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    
    ! finds log10 T given values for gas pressure and 'other',
    ! and initial guess for temperature.
    ! does up to max_iter attempts until logT changes by less than tol.
    
    ! 'other' can be any of the basic result variables for the eos
    ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
    
    use chem_def, only: num_chem_isos
    use eos_def
    use mixeos_def
    use mixeos_mod
    
    integer, intent(in) :: id, k, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logPgas ! log10 of gas pressure
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
         
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        
    
    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logT_result = 0
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    call do_mixeosPT_get_T(xa)
    if(mixdbg0) then
       write(*,*) "mixeosPT_get_T", other_value, logPgas, Rho, logT_result, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if

  contains
    subroutine do_mixeosPT_get_T(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)
      
      call PT_get_T(eos_handle, Z, X, abar, zbar, &
           species, chem_id, net_iso, xa_ptr, &
           logPgas, which_other, other_value, other_tol, logT_tol, &
           max_iter, logT_result, Rho, log10Rho, res, d_dlnRho_const_T, d_dlnT_const_Rho, &
           eos_calls, ierr)
    end subroutine do_mixeosPT_get_T
  end subroutine mixeosPT_get_T
  
  
  subroutine mixeosPT_get_Pgas(&
       id, k, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logT, which_other, other_value,&
       logPgas_tol, other_tol, max_iter, logPgas_guess, &
       logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2,&
       logPgas_result, Rho, log10Rho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
     
    ! finds log10 Pgas given values for temperature and 'other', and initial guess for gas pressure.
    ! does up to max_iter attempts until logPgas changes by less than tol.
    
    ! 'other' can be any of the basic result variables for the eos
    ! specify 'which_other' by means of the definitions in eos_def (e.g., i_lnE)
    
    use chem_def, only: num_chem_isos
    use eos_def
    use mixeos_def
    use mixeos_mod
    
    integer, intent(in) :: id, k, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logT ! log10 of temperature
    
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logPgas_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logPgas_guess ! log10 of gas pressure
    real(dp), intent(in) :: logPgas_bnd1, logPgas_bnd2 ! bounds for logPgas
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logPgas_result ! log10 of gas pressure
    real(dp), intent(out) :: Rho, log10Rho ! density
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)

    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: P, T, lnP, log10P

    logPgas_result = 0
    Rho = 0
    log10Rho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    call do_mixeosPT_get_Pgas(xa)
    if(mixdbg0) then
       write(*,*) "mixeosPT_get_Pgas", other_value, logT, Rho, P, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if
  contains
    subroutine do_mixeosPT_get_Pgas(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)

      if(which_other .eq. i_lnPgas) then 
         !! This would be pretty unusual - do we even want this to work?
         write(*,*) "mixeos_lib: Unexpected usage"
         lnP = other_value
         P = exp(lnP)
         log10P = log10(P)
         call get_res_PT(eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa_ptr, &
              P, log10P, T, logT, Rho, &
              res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
         log10Rho = log10(Rho)
         dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
         dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
      else
         call PT_get_P(eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa_ptr, &
              logT, which_other, other_value, other_tol, logPgas_tol, &
              max_iter, logPgas_result, Rho, log10Rho, &
              res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
         dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
         dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
      endif
    end subroutine do_mixeosPT_get_Pgas
  end subroutine mixeosPT_get_Pgas
  
  
  !! It is extremely important that abar, zbar represent the H/He component by itself
  !! rather than the mixture.  I may have to compute these by thelmselves inside
  !! the mixture before we pass the arguments to the eos module
  subroutine mixeosPT_get_Pgas_for_Rho(&
       id, k, eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa,&
       logT, logRho_want,&
       logPgas_tol, logRho_tol, max_iter, logPgas_guess, &
       logPgas_bnd1, logPgas_bnd2, logRho_at_bnd1, logRho_at_bnd2,&
       logPgas_result, Rho, logRho, dlnRho_dlnPgas_const_T, dlnRho_dlnT_const_Pgas, &
       res, d_dlnRho_const_T, d_dlnT_const_Rho, eos_calls, ierr)
    
    ! finds log10 Pgas given values for temperature and density, and initial guess for gas pressure.
    ! does up to max_iter attempts until logPgas changes by less than tol.
    
    use chem_def, only: num_chem_isos         
    use eos_def
    use mixeos_def
    use mixeos_mod
    
    integer, intent(in) :: id, k, eos_handle
    
    real(dp), intent(in) :: Z ! the metals mass fraction
    real(dp), intent(in) :: X ! the hydrogen mass fraction
    
    real(dp), intent(in) :: abar
    ! mean atomic number (nucleons per nucleus; grams per mole)
    real(dp), intent(in) :: zbar ! mean charge per nucleus
    
    integer, intent(in) :: species
    integer, pointer :: chem_id(:) ! maps species to chem id
    ! index from 1 to species
    ! value is between 1 and num_chem_isos         
    integer, pointer :: net_iso(:) ! maps chem id to species number
    ! index from 1 to num_chem_isos (defined in chem_def)
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    real(dp), intent(in) :: xa(:) ! mass fractions
    
    real(dp), intent(in) :: logT ! log10 of temperature
    
    real(dp), intent(in) :: logRho_want ! log10 of desired density
    real(dp), intent(in) :: logRho_tol
    
    real(dp), intent(in) :: logPgas_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logPgas_guess ! log10 of gas pressure
    real(dp), intent(in) :: logPgas_bnd1, logPgas_bnd2 ! bounds for logPgas
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: logRho_at_bnd1, logRho_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logPgas_result ! log10 of gas pressure
    real(dp), intent(out) :: Rho, logRho ! density corresponding to logPgas_result
    real(dp), intent(out) :: dlnRho_dlnPgas_const_T
    real(dp), intent(out) :: dlnRho_dlnT_const_Pgas
    
    real(dp), intent(out) :: res(:)
    real(dp), intent(out) :: d_dlnRho_const_T(:)
    real(dp), intent(out) :: d_dlnT_const_Rho(:)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: Rhoarg, log10Rhoarg, Targ, log10Targ

    ierr = 0
    logPgas_result = 0
    Rho = 0
    logRho = 0
    dlnRho_dlnPgas_const_T = 0
    dlnRho_dlnT_const_Pgas = 0
    res = 0
    d_dlnRho_const_T = 0
    d_dlnT_const_Rho = 0
    eos_calls = 0
    
    
    Rhoarg = 1d1**logRho_want
    log10Rhoarg = logRho_want
    Targ = 1d1**logT
    log10Targ = logT

    call do_mixeosPT_get_Pgas_for_Rho(xa)
    if(mixdbg0) then
       write(*,*) "mixeosPT_get_Pgas_for_Rho", Rhoarg, Targ, &
            res(i_lnPgas), res(i_lnE), res(i_lnS), ierr
    end if
  contains

    subroutine do_mixeosPT_get_Pgas_for_Rho(xa)
      real(dp), target :: xa(:)
      real(dp), pointer :: xa_ptr(:)
      xa_ptr(1:species) => xa(1:species)
      
      
      call get_res(eos_handle, Z, X, abar, zbar, &
           species, chem_id, net_iso, xa_ptr, &
           Rhoarg, log10Rhoarg, Targ, log10Targ,  &
           res, d_dlnRho_const_T, d_dlnT_const_Rho, ierr)
      
      dlnRho_dlnPgas_const_T = 1d0 / d_dlnRho_const_T(i_lnPgas)
      dlnRho_dlnT_const_Pgas = - d_dlnT_const_Rho(i_lnPgas) / d_dlnRho_const_T(i_lnPgas)
    end subroutine do_mixeosPT_get_Pgas_for_Rho
  end subroutine mixeosPT_get_Pgas_for_Rho
  
end module mixeos_lib
