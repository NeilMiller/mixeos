!! Author : Neil Miller
!!
!! Date   : August, 2012
!!
!! Do we need the recursive modifier?
module inverse_mod
  use const_lib, only: dp
  type, abstract :: inverse
   contains     
     procedure(function_interface), deferred :: function_actual 
     procedure(ndim_interface), deferred :: get_ndim 
     procedure :: inverse_function
  end type inverse
  
  abstract interface
     subroutine function_interface(this, x_ptr, y_val, ierr, dydx_ptr)
       import inverse
       class(inverse) :: this
       real(dp), pointer, dimension(:), intent(in) :: x_ptr
       real(dp), intent(out) :: y_val
       integer, intent(out) :: ierr
       real(dp), optional, pointer, dimension(:), intent(out) :: dydx_ptr
     end subroutine function_interface
     
     subroutine ndim_interface(this, ndim_out)
       import inverse
       class(inverse) :: this
       integer, intent(out) :: ndim_out
     end subroutine ndim_interface
  end interface
  
contains
  function inverse_function(this, xfixed_ptr, xvar_index, y_tgt, &
       xvar_guess, xvar_min, xvar_max, y_tol, ierr, del_frac, nstep)
    class(inverse) :: this
    real(dp), pointer, dimension(:), intent(in) :: xfixed_ptr
    integer, intent(in) :: xvar_index
    real(dp), intent(in) :: y_tgt
    real(dp), intent(in) :: xvar_guess
    real(dp), intent(in) :: xvar_min, xvar_max
    real(dp), intent(in) :: y_tol
    integer, intent(out) :: ierr
    real(dp), optional, intent(in) :: del_frac
    integer, optional, intent(out) :: nstep
    real(dp) :: inverse_function
    
    real(dp), pointer, dimension(:) :: x_ptr, dydx_ptr
    real(dp) :: y_tst, del_y, xnew, delx_frac
    integer :: ndim, steps=0
    
    ierr = 0
    
    if(present(del_frac)) then
       if((del_frac .le. 0d0) .or. (del_frac .gt. 1d0)) then
          ierr = -4
          return
       else
          delx_frac = del_frac
       end if
    else
       delx_frac = 1d0
    end if

    call this%get_ndim(ndim)
    
    allocate(x_ptr(ndim), dydx_ptr(ndim))    
    x_ptr(1:ndim) = xfixed_ptr(1:ndim)
    if((xvar_guess .gt. xvar_min) .and. &
         (xvar_guess .lt. xvar_max)) then
       x_ptr(xvar_index) = xvar_guess
    elseif(xvar_min .lt. xvar_max) then
       x_ptr(xvar_index) = (xvar_min + xvar_max) / 2d0
    else
       ierr = -1
       return
    end if
    
    call this%function_actual(x_ptr, y_tst, ierr, dydx_ptr=dydx_ptr)
    if(ierr .ne. 0) return
    del_y = y_tst - y_tgt
    do while(abs(del_y) .gt. y_tol)
       xnew = x_ptr(xvar_index) - delx_frac * del_y / dydx_ptr(xvar_index)
       if(xnew .gt. xvar_max) then
          if(x_ptr(xvar_index) .eq. xvar_max) then
             ierr = -2
             return
          else
             x_ptr(xvar_index) = xvar_max
          endif
       elseif(xnew .lt. xvar_min) then
          if(x_ptr(xvar_index) .eq. xvar_min) then
             ierr = -3
             return
          else
             x_ptr(xvar_index) = xvar_min
          endif
       else
          x_ptr(xvar_index) = xnew
       endif
       
       call this%function_actual(x_ptr, y_tst, ierr, dydx_ptr=dydx_ptr)
       if(ierr .ne. 0) return
       del_y = y_tst - y_tgt
       steps = steps + 1
    enddo
    
    inverse_function = x_ptr(xvar_index)
    
    deallocate(x_ptr, dydx_ptr)
    
    if(present(nstep)) nstep = steps
    
  end function inverse_function
end module inverse_mod
