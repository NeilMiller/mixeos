!!! Is there is a reason for this complex structure?
!!! mixeos_lib depends on mixeos_mod
!!! mixeos_mod depends on mixeos_private
!!! mixeos_private depends on NUM_METALS constant defined in mixeos_params
!!! mixeos_def also depends on mixeos_params
!! I actually don't think there is.

module mixeos_params
  implicit none
  integer, parameter :: i_P = 1
  integer, parameter :: i_E = 2
  integer, parameter :: i_S = 3

  integer, parameter :: i_val = 1

  integer, parameter :: i_dRho = 2
  integer, parameter :: i_dlnRho = 2
  integer, parameter :: i_dlnP = 2
  integer, parameter :: i_dX = 2
  integer, parameter :: i_dZ = 2
  
  integer, parameter :: i_dT = 3
  integer, parameter :: i_dlnT = 3
  integer, parameter :: i_dY = 3

  integer, parameter :: i_dRho2 = 4
  integer, parameter :: i_dlnRho2 = 4
  integer, parameter :: i_dlnP2 = 4
  integer, parameter :: i_dX2 = 4
  integer, parameter :: i_dZ2 = 4

  integer, parameter :: i_dT2 = 5
  integer, parameter :: i_dlnT2 = 5
  integer, parameter :: i_dY2 = 5

  integer, parameter :: i_dRhodT = 6
  integer, parameter :: i_dlnRhodlnT = 6
  integer, parameter :: i_dlnPdlnT = 6
  integer, parameter :: i_dXdY = 6
  integer, parameter :: i_dZdY = 6
  
  !! size of derivative vectors : for allocation
  integer, parameter :: num_derivs = 6
  
  integer, parameter :: NUM_METALS = 5
  double precision, parameter :: minRho = 1e-10
  double precision, parameter :: maxRho = 1e5
  double precision, parameter :: minT = 1e1
  double precision, parameter :: maxT = 1e8
  double precision, parameter :: minlog10Rho = log10(minRho)
  double precision, parameter :: maxlog10Rho = log10(maxRho)
  double precision, parameter :: minlog10T = log10(minT)
  double precision, parameter :: maxlog10T = log10(maxT)

  integer, parameter :: MIXEOSERR_MEM = -32

end module mixeos_params
