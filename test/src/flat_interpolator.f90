module flat_interpolator_mod

  implicit none

  abstract interface 
    function interface_poly(x,order,nd) result(psi)
  
      real(8) :: psi
     
      real(8), intent(in) :: x
      integer, intent(in) :: order
      integer, intent(in) :: nd
    
    end function interface_poly
  end interface 
  
  
  type :: flat_interpolator
    
    integer :: iord = 0 
    character(len=12) :: interp_type = 'not set'
    procedure(interface_poly), nopass, pointer :: poly => NULL() 
    
  contains  
    
    procedure :: f1D => interp_1D_single
    procedure :: f2D => interp_2D_single
    procedure :: f3D => interp_3D_single
    procedure :: f4D => interp_4D_single
    procedure :: set_order => interp_set_order
    
  end type flat_interpolator

  ! Functions
  private :: interp_1D_single
  private :: interp_2D_single
  private :: interp_3D_single
  private :: interp_4D_single
  private :: interp_set_order
  
  private :: linear_hermite_poly
  private :: cubic_hermite_poly
  private :: quintic_hermite_poly
  
contains

  function interp_set_order(this,label,iorder) result(ierr)
  
    integer :: ierr
    class(flat_interpolator), intent(inout) :: this
    integer, intent(in), optional :: iorder
    character(*), optional :: label
    
    ierr = 0
    
    if (present(label)) then
    select case(TRIM(label))
      case('linear')
        this%iord = 0
        this%poly => linear_hermite_poly
        this%interp_type = 'linear'
      case('cubic')
        this%iord = 1
        this%poly => cubic_hermite_poly
        this%interp_type = 'cubic'
      case('quintic')
        this%iord = 2
        this%poly => quintic_hermite_poly 
        this%interp_type = 'quintic'
      case default
        ierr = 1
    end select
    endif
    
    if (present(iorder)) then
    select case(iorder)
      case(1)
        this%iord = 0
        this%poly => linear_hermite_poly
        this%interp_type = 'linear'
      case(2)
        this%iord = 1
        this%poly => cubic_hermite_poly
        this%interp_type = 'cubic'
      case(3)
        this%iord = 2 
        this%poly => quintic_hermite_poly  
        this%interp_type = 'quintic'
      case default
        ierr = 1
    end select
    endif
    
    if (present(iorder).and.present(label)) ierr = ierr + 2
    
    return
  
  end function interp_set_order
  
  function interp_1D_single(this,x,del,F,ideriv,idmax) result(y)
    
    real(8) :: y
    class(flat_interpolator), intent(in)    :: this
    real(8), intent(in)  :: x(:)       ! Scaled interpolation point
    real(8), intent(in)  :: del(:)     ! Grid size
    real(8), intent(in)  :: F(0:,0:)    ! Edge values
    
    integer, intent(in)  :: ideriv(:)   ! Only calculate one derivative
    integer, intent(in)  :: idmax
     
    integer :: i,a,l,m,n,nd,amax
    integer :: id1,id2
    real(8) :: xx,yy
    real(8) :: psi1(0:1,0:2)   ! Indices: x or 1-x,order,derivative
    real(8) :: f1
     
    id1 = ideriv(1)

    ! Calculate the hermite polynomials 
    ! at the interpolation point
    f1 = del(1)**(-id1)
    amax = this%iord
    do a=0,amax 
      do i=0,1
        if (i==0) then
          xx = x(1)
        elseif (i==1) then
          xx = 1.d0 - x(1)
        endif
        
        psi1(i,a) = this%poly(xx,a,id1)
        
        if (i==1 .and. a==1) then
          psi1(i,a) = -psi1(i,a)
        endif 
        
        if (i==1) then
          psi1(i,a) =  psi1(i,a)*(-1.d0)**id1
        endif  
       
        ! Multiply the desired delta 
        psi1(i,a) = f1*psi1(i,a)       
      enddo
      f1 = f1*del(1)
    enddo
    
    ! Reconstruct the value of the function at the desired point
    y   = 0.d0
    do a=0,amax
    do i=0,1
      y = y + F(a,i)*psi1(i,a)
    enddo
    enddo
 
    return
  
  end function interp_1D_single
  
  function interp_2D_single(this,x,del,F,ideriv,idmax) result(y)
    
    real(8) :: y
    class(flat_interpolator), intent(in) :: this
    real(8), intent(in)  :: x(:)                 ! Scaled interpolation point
    real(8), intent(in)  :: del(:)               ! Grid size
    real(8), intent(in)  :: F(0:,0:)             ! Edge values
    
    integer, intent(in)  :: ideriv(:)            ! Only calculate one derivative
    integer, intent(in)  :: idmax 
    
    integer :: i,j,a,b,l,m,n,nd,amax
    integer :: id1,id2
    integer :: idxp,idxd
    real(8) :: xx,yy
    real(8) :: psi1(0:1,0:2)   ! Indices: x or 1-x,order,derivative
    real(8) :: psi2(0:1,0:2)
    real(8) :: f1,f2
    
    id1 = ideriv(1)
    id2 = ideriv(2) 

    ! Calculate the hermite polynomials 
    ! at the interpolation point
    amax = this%iord
    f1 = del(1)**(-id1) 
    f2 = del(2)**(-id2)
    do a=0,amax
      do i=0,1
        if (i==0) then
          xx = x(1)
          yy = x(2)
        elseif (i==1) then
          xx = 1.d0 - x(1)
          yy = 1.d0 - x(2)
        endif
        psi1(i,a) = this%poly(xx,a,id1)
        psi2(i,a) = this%poly(yy,a,id2)
        
        if (i==1 .and. a==1) then
          psi1(i,a) = -psi1(i,a)
          psi2(i,a) = -psi2(i,a)
        endif 
        
        if (i==1) then
          psi1(i,a) =  psi1(i,a)*(-1.d0)**id1
          psi2(i,a) =  psi2(i,a)*(-1.d0)**id2
        endif  
       
        ! Multiply the desired delta 
        psi1(i,a) = f1*psi1(i,a)       
        psi2(i,a) = f2*psi2(i,a)       
      
      enddo
      f1 = f1*del(1)
      f2 = f2*del(2)
    enddo
    
    ! Reconstruct the value of the function at the desired point
    y = 0.d0
    do b=0,amax
    do a=0,amax
      idxd = idmax*b + a
    do j=0,1
    do i=0,1
      idxp = 2*j + i
      y = y + F(idxd,idxp)*psi1(i,a)*psi2(j,b)
    enddo
    enddo
    enddo
    enddo
 
    return
  
  end function interp_2D_single
  
  
  function interp_3D_single(this,x,del,F,ideriv,idmax) result(y)
    
    real(8) :: y
    class(flat_interpolator), intent(in) :: this
    real(8), intent(in)  :: x(:)                 ! Scaled interpolation point
    real(8), intent(in)  :: del(:)               ! Grid size
    real(8), intent(in)  :: F(0:,0:)             ! Edge values
    
    integer, intent(in)  :: ideriv(:)            ! Only calculate one derivative
    integer, intent(in)  :: idmax 
    
    integer :: i,j,k,a,b,c,l,m,n,nd,amax
    integer :: id1,id2,id3
    integer :: idxp,idxd
    real(8) :: xx,yy,zz
    real(8) :: psi1(0:1,0:2)   ! Indices: x or 1-x,order,derivative
    real(8) :: psi2(0:1,0:2)
    real(8) :: psi3(0:1,0:2)
    real(8) :: f1,f2,f3
    
    id1 = ideriv(1)
    id2 = ideriv(2) 
    id3 = ideriv(3) 

    ! Calculate the hermite polynomials 
    ! at the interpolation point
    amax = this%iord
    f1 = del(1)**(-id1) 
    f2 = del(2)**(-id2)
    f3 = del(3)**(-id3)
    do a=0,amax
      do i=0,1
        if (i==0) then
          xx = x(1)
          yy = x(2)
          zz = x(3)
        elseif (i==1) then
          xx = 1.d0 - x(1)
          yy = 1.d0 - x(2)
          zz = 1.d0 - x(3)
        endif
        psi1(i,a) = this%poly(xx,a,id1)
        psi2(i,a) = this%poly(yy,a,id2)
        psi3(i,a) = this%poly(zz,a,id3)
        
        if (i==1 .and. a==1) then
          psi1(i,a) = -psi1(i,a)
          psi2(i,a) = -psi2(i,a)
          psi3(i,a) = -psi3(i,a)
        endif 
        
        if (i==1) then
          psi1(i,a) =  psi1(i,a)*(-1.d0)**id1
          psi2(i,a) =  psi2(i,a)*(-1.d0)**id2
          psi3(i,a) =  psi3(i,a)*(-1.d0)**id3
        endif  
       
        ! Multiply the desired delta 
        psi1(i,a) = f1*psi1(i,a)       
        psi2(i,a) = f2*psi2(i,a)
        psi3(i,a) = f3*psi3(i,a)
      
      enddo
      f1 = f1*del(1)
      f2 = f2*del(2)
      f3 = f3*del(3)
    enddo
    
    ! Reconstruct the value of the function at the desired point
    y = 0.d0
    do c=0,amax
    do b=0,amax
    do a=0,amax
      idxd = idmax*idmax*c + idmax*b + a
    do k=0,1
    do j=0,1
    do i=0,1
      idxp = 4*k + 2*j + i
      y = y + F(idxd,idxp)*psi1(i,a)*psi2(j,b)*psi3(k,c)
    enddo
    enddo
    enddo
    enddo
    enddo
    enddo
 
    return
  
  end function interp_3D_single
  
  function interp_4D_single(this,x,del,F,ideriv,idmax) result(y)
    
    real(8) :: y
    class(flat_interpolator), intent(in) :: this
    real(8), intent(in)  :: x(:)                 ! Scaled interpolation point
    real(8), intent(in)  :: del(:)               ! Grid size
    real(8), intent(in)  :: F(0:,0:)             ! Edge values
    
    integer, intent(in)  :: ideriv(:)            ! Only calculate one derivative
    integer, intent(in)  :: idmax 
    
    integer :: i,j,k,a,b,c,d,l,m,n,nd,amax
    integer :: id1,id2,id3,id4
    integer :: idxp,idxd
    integer :: idxp1,idxp2,idxp3
    integer :: idxd1,idxd2,idxd3
    integer :: idmax2,idmax3
    real(8) :: xx,yy,zz,ww
    real(8) :: psi1(0:1,0:2)   ! Indices: x or 1-x,order,derivative
    real(8) :: psi2(0:1,0:2)
    real(8) :: psi3(0:1,0:2)
    real(8) :: psi4(0:1,0:2)
    real(8) :: f1,f2,f3,f4
    
    id1 = ideriv(1)
    id2 = ideriv(2) 
    id3 = ideriv(3) 
    id4 = ideriv(4)
    
    ! Calculate the hermite polynomials 
    ! at the interpolation point
    amax = this%iord
    f1 = del(1)**(-id1) 
    f2 = del(2)**(-id2)
    f3 = del(3)**(-id3)
    f4 = del(4)**(-id4)
    do a=0,amax
      do i=0,1
        if (i==0) then
          xx = x(1)
          yy = x(2)
          zz = x(3)
          ww = x(4)
        elseif (i==1) then
          xx = 1.d0 - x(1)
          yy = 1.d0 - x(2)
          zz = 1.d0 - x(3)
          ww = 1.d0 - x(4)
        endif
        psi1(i,a) = this%poly(xx,a,id1)
        psi2(i,a) = this%poly(yy,a,id2)
        psi3(i,a) = this%poly(zz,a,id3)
        psi4(i,a) = this%poly(ww,a,id4)
        
        if (i==1 .and. a==1) then
          psi1(i,a) = -psi1(i,a)
          psi2(i,a) = -psi2(i,a)
          psi3(i,a) = -psi3(i,a)
          psi4(i,a) = -psi4(i,a)
        endif 
        
        if (i==1) then
          psi1(i,a) =  psi1(i,a)*(-1.d0)**id1
          psi2(i,a) =  psi2(i,a)*(-1.d0)**id2
          psi3(i,a) =  psi3(i,a)*(-1.d0)**id3
          psi4(i,a) =  psi4(i,a)*(-1.d0)**id4
        endif  
       
        ! Multiply the desired delta 
        psi1(i,a) = f1*psi1(i,a)       
        psi2(i,a) = f2*psi2(i,a)
        psi3(i,a) = f3*psi3(i,a)
        psi4(i,a) = f4*psi4(i,a)
        
      enddo
      f1 = f1*del(1)
      f2 = f2*del(2)
      f3 = f3*del(3)
      f4 = f4*del(4)
    enddo
    
    ! Reconstruct the value of the function at the desired point
    y = 0.d0
    idmax2 = idmax*idmax
    idmax3 = idmax2*idmax
    do d=0,amax
      idxd1 = idmax3*d
    do c=0,amax
      idxd2 = idmax2*c + idxd1
    do b=0,amax
      idxd3 = idmax*b + idxd2
    do a=0,amax
      idxd = idxd3 + a
    do l=0,1
      idxp1 = 8*l
    do k=0,1
      idxp2 = idxp1 + 4*k
      f1 = psi3(k,c)*psi4(l,d)
    do j=0,1
      idxp3 = idxp2 + 2*j
      f2 = f1*psi2(j,b)
    do i=0,1
      idxp = idxp3 + i
      y = y + F(idxd,idxp)*psi1(i,a)*f2
    enddo
    enddo
    enddo
    enddo
    enddo
    enddo
    enddo
    enddo
    
    return
  
  end function interp_4D_single
  
  function linear_hermite_poly(x,order,nd) result(psi)
  
    real(8) :: psi
    
    real(8), intent(in) :: x
    integer, intent(in) :: order
    integer, intent(in) :: nd
    
    select case (nd)
    case(0)
      select case (order)
      case(0)
        psi = 1.d0 - x
      case default
        psi = 0.
      end select
    case(1)
      select case (order)
      case(0)
        psi = -1.d0 
      case default
        psi = 0.
      end select
    case default 
      psi = 0.d0
    end select  
    
    return
    
  end function linear_hermite_poly
  
  function cubic_hermite_poly(x,order,nd) result(psi)
  
    real(8) :: psi
    
    real(8), intent(in) :: x
    integer, intent(in) :: order
    integer, intent(in) :: nd
    
    
    real(8) :: x2,x3
    
    select case (nd)
    case(0)
      x2 = x*x
      x3 = x*x2
      select case (order)
      case(0)
        psi = 2.d0*x3 - 3.d0*x2 + 1.d0
      case(1)
        psi = x3 - 2.d0*x2 + x
      case default
        psi = 0.
      end select
    case(1)
      x2 = x*x
      select case (order)
      case(0)
        psi = 6.d0*x2 - 6.d0*x 
      case(1)
        psi = 3.d0*x2 - 4.d0*x + 1.d0
      case default
        psi = 0.
      end select
    case(2)
      select case (order)
      case(0)
        psi = 12.d0*x - 6.d0 
      case(1)
        psi = 6.d0*x  - 4.d0
      case default
        psi = 0.
      end select
    case(3)
      select case (order)
      case(0)
        psi = 12.d0
      case(1)
        psi = 6.d0
      case default
        psi = 0.
      end select
    case default 
      psi = 0.d0
    end select  
        
    return
    
  end function cubic_hermite_poly

  function quintic_hermite_poly(x,order,nd) result(psi)
  
    real(8) :: psi
    
    real(8), intent(in) :: x
    integer, intent(in) :: order
    integer, intent(in) :: nd
    
    real(8) :: x2,x3,x4,x5
    
   
    
    select case (nd)
    case(0)
      x2 = x*x
      x3 = x*x2
      x4 = x*x3
      x5 = x*x4
      select case (order)
      case(0)
        psi = -6.d0*x5   + 15.d0*x4 - 10.d0*x3 + 1.d0
      case(1)
        psi = -3.d0*x5   + 8.d0*x4  - 6.d0*x3 + x
      case(2)
        psi = 0.5d0*(-x5 + 3.d0*x4  - 3.d0*x3 + x2)
      case default
        psi = 0.
      end select
    case(1)
      x2 = x*x
      x3 = x*x2
      x4 = x*x3
      select case (order)
      case(0)
        psi = -30.d0*x4 + 60.d0*x3 - 30.d0*x2
      case(1)
        psi = -15.d0*x4 + 32.d0*x3 - 18.d0*x2 + 1.d0
      case(2)
        psi = 0.5d0*(-5.d0*x4  + 12.d0*x3 - 9.d0*x2  + 2.d0*x)
      case default
        psi = 0.
      end select
    case(2)
      x2 = x*x
      x3 = x*x2
      select case (order)
      case(0)
        psi = -120.d0*x3 + 180.d0*x2 - 60.d0*x
      case(1)
        psi = -60.d0*x3  + 96.d0*x2  - 36.d0*x
      case(2)
        psi = 0.5d0*(-20.d0*x3 + 36.d0*x2  - 18.d0*x   + 2.d0)
      case default
        psi = 0.
      end select
    case(3)
      x2 = x*x
      select case (order)
      case(0)
        psi = -360.d0*x2 + 360.d0*x   - 60.d0
      case(1)
        psi = -180.d0*x2 + 192.d0*x   - 36.d0 
      case(2)
        psi = 0.5d0*(-60.d0*x2  + 72.d0*x    - 18.d0)
      case default
        psi = 0.
      end select
    case(4)
      select case (order)
      case(0)
        psi = -720.d0*x    + 360.d0
      case(1)
        psi = -360.d0*x   + 192.d0
      case(2)
        psi = 0.5d0*(-120.d0*x   + 72.d0)
      case default
        psi = 0.
      end select
    case(5)
      select case (order)
      case(0)
        psi = -720.d0
      case(1)
        psi = -360.d0
      case(2)
        psi = -60.d0
      case default
        psi = 0.
      end select
    case default 
      psi = 0.d0
    end select  
        
    return
    
  end function quintic_hermite_poly

end module flat_interpolator_mod
