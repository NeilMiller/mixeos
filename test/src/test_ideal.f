program test_ideal
  use const_def
  use const_lib
  use test_eos_obj
  use ideal_eos_mod
  use base_eos_mod

  type(ideal_eos), target :: my_eos
  real(dp) :: mu
  integer :: ierr
  character (len=256) :: my_mesa_dir
  class(base_eos), pointer :: my_eos_ptr

  my_mesa_dir = "/Users/neil/research/mesa/"

  call const_init(my_mesa_dir,ierr)
  
  mu = 18d0
  call my_eos%Construct(mu)  
  my_eos_ptr => my_eos
  call DT_PT_test(my_eos_ptr, ierr)
  
end program test_ideal
