!! This module has been extensively checked mathematically and fairly well tested.
!! Don't change the math unless you are absolutely certain since you may be introducing
!! a bug.

!! This should be a public module because these routines may be useful for other equation
!! of state software.  

!! Beware of degeneracy.  The MESA/RES, Derivs, or lnDerivs formats include redundant information
!!  a natural basis vector is {F, dF/dRho, dF/dT...}
!!   or {G...}

module thermo
  use const_lib
  implicit none
  
contains
  
  !! These two functions below are actually the same, but with different
  !! symbols (X,Y,Z) is a well defined relation and other functions F_i(X,Y)
  !! then you can convert first and second derivatives of Z(X,Y), and F_i(X,Y)
  !!   to first and second derivatives of X(Z,Y) and F_i(Z,Y)
  !! this function then is the general format.  The first row is the X dimension
  !! the second row is the Y dimension
  !! the third row is the Z dimension
  !! the rows after that are F_i
  !! size should be 3 + n(i)
  subroutine convXY_ZY(in_matrix, out_matrix, size)
    use mixeos_def
    
    real(dp), dimension(:,:), intent(in) :: in_matrix
    real(dp), dimension(:,:), intent(out) :: out_matrix
    integer, intent(in) :: size
    real(dp) :: conv(num_derivs,num_derivs) !! This matrix will do all the work
    integer :: i,j,k
    integer :: Zi = 3

    out_matrix(:,:) = 0.d0
    conv(:,:) = 0.d0
    conv(i_val,i_val) = 1.
    conv(i_dZ,i_dX) = 1./in_matrix(Zi,i_dX)
    conv(i_dY,i_dX) = -in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)
    conv(i_dY,i_dY) = 1.
    conv(i_dZ2,i_dX) = -in_matrix(Zi,i_dX2)/in_matrix(Zi,i_dX)**3
    conv(i_dZ2,i_dX2) = 1./in_matrix(Zi,i_dX)**2
    conv(i_dY2,i_dX) = (2.*in_matrix(Zi,i_dY)*in_matrix(Zi,i_dXdY)/in_matrix(Zi,i_dX)**2 &
                        - in_matrix(Zi,i_dY)**2 * in_matrix(Zi,i_dX2)/in_matrix(Zi, i_dX)**3 &
                        - in_matrix(Zi,i_dY2)/in_matrix(Zi,i_dX))
    conv(i_dY2,i_dX2) = (in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX))**2
    conv(i_dY2,i_dY2) = 1.
    conv(i_dY2,i_dXdY) = -2.*in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)
    conv(i_dZdY,i_dX) = -in_matrix(Zi,i_dXdY)/in_matrix(Zi,i_dX)**2 &
                        + in_matrix(Zi,i_dY)*in_matrix(Zi,i_dX2)/in_matrix(Zi,i_dX)**3
    conv(i_dZdY,i_dX2) = -in_matrix(Zi,i_dY)/in_matrix(Zi,i_dX)**2
    conv(i_dZdY,i_dXdY) = 1./in_matrix(Zi,i_dX)
    
    
    do i=1,size
       do j=1,num_derivs
          do k=1,num_derivs
             out_matrix(i,j) = out_matrix(i,j) + conv(j,k)*in_matrix(i,k)
          end do
       end do
    enddo
  end subroutine convXY_ZY
  

  !! This can be treated as Rho, T, P, Pvect, Evect, Svect, Rhovect, Evect, Svect
  subroutine convRhoT_PT(Rho, T, P, &
       Pvect_input, Evect_input, Svect_input, &
       Rhovect_output, Evect_output, Svect_output)
    use mixeos_def

    !! (Rho, T) --> (P, T)
    real(dp), intent(in) :: Rho, T, P
    real(dp), intent(in) :: Pvect_input(num_derivs), Evect_input(num_derivs), Svect_input(num_derivs)
    real(dp), intent(out) :: Rhovect_output(num_derivs), Evect_output(num_derivs), Svect_output(num_derivs)
    real(dp) :: data_matrix1(5,num_derivs), data_matrix2(5,num_derivs)
    
    data_matrix1(:,:) = 0.
    data_matrix1(1,i_val) = Rho
    data_matrix1(1,i_dRho) = 1d0
    data_matrix1(2,i_val) = T
    data_matrix1(2,i_dT) = 1d0
    data_matrix1(3,:) = Pvect_input
    data_matrix1(4,:) = Evect_input
    data_matrix1(5,:) = Svect_input
    
    call convXY_ZY(data_matrix1, data_matrix2, 5)
    Rhovect_output = data_matrix2(1,:)
    Evect_output = data_matrix2(4,:)
    Svect_output = data_matrix2(5,:)
  end subroutine convRhoT_PT

  subroutine convPT_RhoT(Rho, T, P, &
       Rhovect_input, Evect_input, Svect_input, &
       Pvect_output, Evect_output, Svect_output)
    use mixeos_def

    !! (P, T) --> (Rho, T)
    real(dp), intent(in) :: Rho, T, P
    real(dp), intent(in) :: Rhovect_input(num_derivs), Evect_input(num_derivs), Svect_input(num_derivs)
    real(dp), intent(out) :: Pvect_output(num_derivs), Evect_output(num_derivs), Svect_output(num_derivs)
    real(dp) :: data_matrix1(5,num_derivs), data_matrix2(5,num_derivs)
    
    data_matrix1(:,:) = 0.
    data_matrix1(1,i_val) = P
    data_matrix1(1,i_dP) = 1.
    data_matrix1(2,i_val) = T
    data_matrix1(2,i_dT) = 1.
    data_matrix1(3,:) = Rhovect_input
    data_matrix1(4,:) = Evect_input
    data_matrix1(5,:) = Svect_input

    call convXY_ZY(data_matrix1, data_matrix2, 5)
    Pvect_output = data_matrix2(1,:)
    Evect_output = data_matrix2(4,:)
    Svect_output = data_matrix2(5,:)
  end subroutine convPT_RhoT


  !! Convert Results vectors -> F vector
  subroutine ConvRes_F(Rho, log10Rho, T, log10T, Res, d_dlnRho, d_dlnT, Fvect)
    use mixeos_def
    use eos_def
    real(dp), intent(in) :: Rho, log10Rho, T, log10T
    real(dp), intent(in) :: res(num_eos_basic_results), &
         d_dlnRho(num_eos_basic_results), &
         d_dlnT(num_eos_basic_results)
    real(dp), intent(out) :: Fvect(num_Fderivs)
    real(dp) :: P, E, S
    real(dp) :: coeff_A, coeff_B, coeff_C
    
    P = exp(res(i_lnPgas))
    E = exp(res(i_lnE))
    S = exp(res(i_lnS))
    
    !! Constrain F, df/dRho, dF/dT using E, S, dE/dRho, dS/dRho
    Fvect(i_val) = E - T * S
!      Fvect(i_dRho) = P / Rho**2d0  !! Error 0.0185
!      Fvect(i_dRho) = res(i_dE_dRho) - T * res(i_dS_dRho)  !! error 0.0185
    Fvect(i_dRho) = (E / Rho) * d_dlnRho(i_lnE) &  !! 0.003
         - T * (S / Rho) * d_dlnRho(i_lnS)
    Fvect(i_dT) = - S
    
    !! Constraint #1:
    !! Require that C_v be maintained along the grid
    !! 
    Fvect(i_dT2) = - (1d0 / T) * res(i_Cv)  !! Constraing # 1 
         !- res(i_dS_dT)                    !! These degenerate terms should never appear elsewhere
         !- (S / T) * d_dlnT(i_lnS)         !! This degenerate term should not appear elsewhere

    !! Constraint #1b (I'm not sure if this is the most important one)
    !! Require that d_dlnT(i_Cv) be maintained, which will be smooth with C_v
    Fvect(i_dT3)     = (-1d0 / T) * Fvect(i_dT2) &
         - (1d0 / T**2d0) * d_dlnT(i_Cv)
           !- (1 / T) * d_dlnT(i_dS_dT)  !! 

    !! Constraint #1c (Again, I'm not sure if this is the most important
    !! requrie that d_dlnRho(i_Cv) be maintained.  
    Fvect(i_dRhodT2) = (-1d0 / (Rho * T)) * d_dlnRho(i_Cv)
         !- (1 / Rho) * d_dlnRho(i_dS_dT)
        ! - (1 / T) * d_dlnT(i_dS_dRho)
    
    
    Fvect(i_dRho2) = &!(P / Rho**3d0) * (d_dlnRho(i_lnPgas) - 2d0)
         (1/Rho)*d_dlnRho(i_dE_dRho) &
         - (T/Rho)*d_dlnRho(i_dS_dRho)

    !! This constraint is the + version of the quadratic.
    !!  The objective here is to maintain res(i_grad_ad)
!    coeff_A = (1d0 / res(i_grad_ad))
!    coeff_B = (-T*Rho**2d0 / P) 
!    coeff_C = (T/P) * Fvect(i_dT2) &
!         * (2 * Rho * Fvect(i_dRho) &
!             + Rho**2d0 * Fvect(i_dRho2))
!    Fvect(i_dRhodT) = (-coeff_B + sqrt(coeff_B**2d0 &
!                                       - 4d0 * coeff_A * coeff_C)) &
!                               / (2d0 * coeff_A)


!! This didn't work

    Fvect(i_dRhodT) = -res(i_dS_dRho)
!         (P / (Rho**2d0 * T)) * d_dlnT(i_lnPgas) !! This seems good
!         (P / (Rho**2d0 * T)) * &
!          (1d0 / res(i_grad_ad) + d_dlnT(i_lnS) * d_dlnRho(i_lnPgas) / 
    
    
    
    Fvect(i_dRho3) = 6d0 * P / Rho**4d0 &
         - (4d0 * P / Rho**4d0) * d_dlnRho(i_lnPgas) &
         + (P / Rho**4d0) * (d_dlnRho(i_chiRho) &
         - d_dlnRho(i_lnPgas) &
         + d_dlnRho(i_lnPgas)**2d0) !! Remarkably, this is good
    
    Fvect(i_dRho2dT) = - (1d0 / Rho) * d_dlnRho(i_dS_dRho)

    
  end subroutine ConvRes_F
  
  subroutine ConvF_derivs(Rho, T, Fvect, Pvect, Evect, Svect)
    use mixeos_def
    real(dp), intent(in) :: Rho, T, Fvect(num_Fderivs)
    real(dp), intent(out) :: Pvect(num_derivs), Svect(num_derivs), Evect(num_derivs)
    
    real(dp) :: RhoSQ, free, df_d, df_t, df_dd, df_dt, df_tt, &
         df_ddd, df_ddt, df_dtt, df_ttt
    
    RhoSQ = Rho * Rho
    
    free = Fvect(i_val)
    df_d = Fvect(i_dRho)
    df_t = Fvect(i_dT)
    
    df_dd = Fvect(i_dRho2)
    df_dt = Fvect(i_dRhodT)
    df_tt = Fvect(i_dT2)
    
    df_ddd = Fvect(i_dRho3)
    df_ddt = Fvect(i_dRho2dT)
    df_dtt = Fvect(i_dRhodT2)
    df_ttt = Fvect(i_dT3)
      
    Pvect(i_val) = RhoSQ * df_d
    Pvect(i_drho) = RhoSQ * df_dd + 2 * Rho * df_d
    Pvect(i_dt) = RhoSQ * df_dt
    Pvect(i_drho2) = RhoSQ * df_ddd + 4. * Rho * df_dd + 2. * df_d 
    Pvect(i_dt2) = RhoSQ * df_dtt
    Pvect(i_drhodt) = RhoSQ * df_ddt + 2. * Rho * df_dt
    
    Svect(i_val) = -df_t
    Svect(i_drho) = -df_dt
    Svect(i_dt) = -df_tt
    Svect(i_drho2) = -df_ddt
    Svect(i_dt2) = - df_ttt
    Svect(i_drhodt) = - df_dtt
    
    Evect(i_val) = free - T * df_t
    Evect(i_drho) = df_d - T * df_dt
    Evect(i_dt) = -T * df_tt
    Evect(i_drho2) = df_dd - T*df_ddt
    Evect(i_dt2) = -df_tt - T * df_ttt
    Evect(i_drhodt) = - T * df_dtt
  end subroutine ConvF_derivs
  
  !! This subroutine will convert the outputs from the results structure in the standard "eos_def.f" format
  !! to the straight up derivatives format that is used above
  !! and generally used in mesa.  This is generally useful because we are adding things in the general
  !! derivatives space and then we construct various thermodynamic quantities using these derivatives.
  subroutine convRES_lnderivs(res, d_dlnRho, d_dlnT, lnRho, lnT, lnPvect, lnEvect, lnSvect)
    use eos_def
    use mixeos_def

    real(dp), intent(in) :: res(num_eos_basic_results), d_dlnRho(num_eos_basic_results), &
                                    d_dlnT(num_eos_basic_results)
    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(out) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs)
    real(dp) :: d, S, E, T, P
    
    d = exp(lnRho)
    T = exp(lnT)
    P = exp(res(i_lnPgas))
    S = exp(res(i_lnS))
    E = exp(res(i_lnE))
    
    !! The MESA EOS reports inconsistent results between the results vector and d_dlnRho / d_dlnT vectors
    !! I've found that the results vector is the best to use whenever possible 
    
    lnPvect(i_val) = res(i_lnPgas)
    lnPvect(i_dlnRho) = d_dlnRho(i_lnPgas)
    !! This appears to be inferior: res(i_chiRho)
!    lnPvect(i_dlnT)   = d_dlnT(i_lnPgas)
    !! OLD value res(i_chiT)                     
    lnPvect(i_dlnRho2) = d_dlnRho(i_chiRho)
!    lnPvect(i_dlnT2) = d_dlnT(i_chiT)

          !lnPvect(i_dlnRhodlnT) = d_dlnRho(i_chiT)
    
    lnSvect(i_val) = res(i_lnS)
    lnSvect(i_dlnRho) = d_dlnRho(i_lnS) !(d / S) * res(i_dS_dRho)  
    lnSvect(i_dlnT) =   d_dlnT(i_lnS) !(T / S) * res(i_dS_dT)    
    lnSvect(i_dlnRho2) = (d/S) * d_dlnRho(i_dS_dRho) - lnSvect(i_dlnRho)**2d0 + lnSvect(i_dlnRho)
    lnSvect(i_dlnT2) =   (T/S) * d_dlnT(i_dS_dT)    -  lnSvect(i_dlnT)**2d0    + lnSvect(i_dlnT)
    lnSvect(i_dlnRhodlnT) = (d/S) * d_dlnT(i_dS_dRho) - lnSvect(i_dlnRho) * lnSvect(i_dlnT)
    
    lnEvect(i_val) = res(i_lnE)
    lnEvect(i_dlnRho) =  d_dlnRho(i_lnE) !(d / E) * res(i_dE_dRho)    
    lnEvect(i_dlnT) =    d_dlnT(i_lnE) !(T / E) * res(i_Cv)      
    lnEvect(i_dlnRho2) = (d/E) * d_dlnRho(i_dE_dRho) - lnEvect(i_dlnRho)**2d0 +  lnEvect(i_dlnRho)
    lnEvect(i_dlnT2) =   (T/E) * d_dlnT(i_Cv)        - lnEvect(i_dlnT)**2d0   +  lnEvect(i_dlnT)
    lnEvect(i_dlnRhodlnT) = (d/E) * d_dlnT(i_dE_dRho) - lnEvect(i_dlnRho) * lnEvect(i_dlnT)

    !! Constrain that res(i_grad_ad) stays the same
    lnPvect(i_dlnT) = (1d0 / res(i_grad_ad)) + lnSvect(i_dlnT) * lnPvect(i_dlnRho) / lnSvect(i_dlnRho)

    !! Constraint that d_dlnRho(i_grad_ad) stays the same
    lnPvect(i_dlnRhodlnT) = - d_dlnRho(i_grad_ad) / res(i_grad_ad)**2d0 &
         + lnSvect(i_dlnRhodlnT)*lnPvect(i_dlnRho)/lnSvect(i_dlnRho) &
         + lnSvect(i_dlnT) * lnPvect(i_dlnRho2)/lnSvect(i_dlnRho) &
         - lnSvect(i_dlnT) * lnPvect(i_dlnRho) * lnSvect(i_dlnRho2) / lnSvect(i_dlnRho)**2d0
    !! Constraint guarentees that d_dlnT(i_grad_ad) stays the same
    lnPvect(i_dlnT2) = - d_dlnT(i_grad_ad) / res(i_grad_ad)**2d0 &
         + lnSvect(i_dlnT2) * lnPvect(i_dlnRho) / lnSvect(i_dlnRho) &
         + lnSvect(i_dlnT) * lnPvect(i_dlnRhodlnT) / lnSvect(i_dlnRho) &
         - lnSvect(i_dlnT) * lnPvect(i_dlnRho) * lnSvect(i_dlnRhodlnT) / lnSvect(i_dlnRhodlnT)
  end subroutine convRES_lnderivs
  
  !! This is the inverse routine
  subroutine convlnderivs_RES(lnRho, lnT, lnPvect, lnEvect, lnSvect, res, d_dlnRho_c_T, d_dlnT_c_Rho)
    use eos_def
    use mixeos_def
    use const_lib

    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(in) :: lnPvect(num_derivs), lnEvect(num_derivs), lnSvect(num_derivs)
    real(dp), intent(out) :: res(num_eos_basic_results), d_dlnRho_c_T(num_eos_basic_results), &
                                     d_dlnT_c_Rho(num_eos_basic_results)
    
    real(dp) :: alpha, beta, Cv, Cp
    real(dp) :: lnP,lnE,lnS,P,E,S,d,T
    real(dp) :: dlnEdlnRho_ct,dlnEdlnT_cd, &
         dlnSdlnRho_ct, dlnSdlnT_cd,&
         dlnPdlnRho_ct,dlnPdlnT_cd
    real(dp) :: d2lnSdlnT2_cd, d2lnSdlnRhodlnT, d2lnSdlnRho2_ct, &
         d2lnEdlnRho2_ct,d2lnEdlnRhodlnT, d2lnEdlnT2_cd, &
         d2lnPdlnRho2_ct,d2lnPdlnRhodlnT, d2lnPdlnT2_cd,&
         dalphadlnRho_ct, dalphadlnT_cd,&
         dbetadlnRho_ct, dbetadlnT_cd,&
         dCvdlnRho_ct, dCvdlnT_cd,&
         dCpdlnRho_ct, dCpdlnT_cd
    
    

    res(:) = 0.
    d_dlnRho_c_T(:) = 0.
    d_dlnT_c_Rho(:) = 0.
    
    lnP = lnPvect(i_val)
    lnE = lnEvect(i_val)
    lnS = lnSvect(i_val)
    
    dlnPdlnRho_ct =       lnPvect(i_dlnRho)
    dlnPdlnT_cd =         lnPvect(i_dlnT)
    d2lnPdlnRho2_ct =     lnPvect(i_dlnRho2)
    d2lnPdlnT2_cd =       lnPvect(i_dlnT2)
    d2lnPdlnRhodlnT =     lnPvect(i_dlnRhodlnT)
    
    dlnEdlnRho_ct =       lnEvect(i_dlnRho)
    dlnEdlnT_cd =         lnEvect(i_dlnT)
    d2lnEdlnRho2_ct =     lnEvect(i_dlnRho2)
    d2lnEdlnT2_cd =       lnEvect(i_dlnT2)
    d2lnEdlnRhodlnT =     lnEvect(i_dlnRhodlnT)
    
    dlnSdlnRho_ct =       lnSvect(i_dlnRho)
    dlnSdlnT_cd =         lnSvect(i_dlnT)
    d2lnSdlnRho2_ct =     lnSvect(i_dlnRho2)
    d2lnSdlnT2_cd =       lnSvect(i_dlnT2)
    d2lnSdlnRhodlnT =     lnSvect(i_dlnRhodlnT)
    
    d = exp(lnRho)
    T = exp(lnT)
    P = exp(lnP)
    E = exp(lnE)
    S = exp(lnS)
    
    Cv = (E/T) * dlnEdlnT_cd
    dCvdlnRho_ct = (E / T) * dlnEdlnRho_ct * dlnEdlnT_cd &
         + (E / T) * d2lnEdlnRhodlnT
    dCvdlnT_cd = (E / T) * dlnEdlnT_cd**2 &
         - (E / T) * dlnEdlnT_cd + (E/T) * d2lnEdlnT2_cd
    
    alpha = dlnPdlnT_cd / (T*dlnPdlnRho_ct)
    dalphadlnRho_ct = d2lnPdlnRhodlnT / (T*dlnPdlnRho_ct) &
         - (dlnPdlnT_cd*d2lnPdlnRho2_ct) / (T*dlnPdlnRho_ct**2)
    dalphadlnT_cd = d2lnPdlnT2_cd / (T * dlnPdlnRho_ct) &
         - alpha - (dlnPdlnT_cd * d2lnPdlnRhodlnT) / (T * dlnPdlnRho_ct**2)
    
    beta = 1/(P * dlnPdlnRho_ct)
    dbetadlnRho_ct = - dlnPdlnRho_ct / (P * dlnPdlnRho_ct) &
         - d2lnPdlnRho2_ct / (P * dlnPdlnRho_ct)
    dbetadlnT_cd = - dlnPdlnT_cd / (P * dlnPdlnRho_ct) &
         - d2lnPdlnRhodlnT / (P * dlnPdlnRho_ct**2)
    
!    write(*,*) "[thermo]"
!    write(*,*) "dlnPdlnRho_ct: ", dlnPdlnRho_ct
!    write(*,*) "dlnPdlnT_cd: ", dlnPdlnT_cd
!    write(*,*) "dlnEdlnRho_ct: ", dlnEdlnRho_ct
!    write(*,*) "dlnEdlnT_cd: ", dlnEdlnT_cd
!    write(*,*) "d: ", d
!    write(*,*) "T: ", T
!    write(*,*) "P: ", P
!    write(*,*) "E: ", E
    
    
 !   Cp = (- (d/T)*dlnPdlnT_cd/dlnPdlnRho_ct) &
 !             *(E/d*dlnPdlnRho_ct+P/d**2d0*dlnPdlnRho_ct-P/d**2d0) &
 !         +E/T*dlnEdlnT_cd + P/(d*T)*dlnPdlnT_cd
    Cp = -(E/T) * dlnPdlnT_cd * dlnEdlnRho_ct / dlnPdlnRho_ct &
       + (E/T) * dlnEdlnT_cd &
       + (P/(d*T))*dlnPdlnT_cd/dlnPdlnRho_ct
    
    !! Rework this guy
    dCpdlnRho_ct = -(E/T)*dlnEdlnRho_ct*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct &
                   -(E/T)*d2lnPdlnRhodlnT*dlnEdlnRho_ct/dlnPdlnRho_ct &
                   -(E/T)*dlnPdlnT_cd*d2lnEdlnRho2_ct/dlnPdlnRho_ct &
                   +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct*d2lnPdlnRho2_ct/dlnPdlnRho_ct**2d0 &
                   +(E/T)*dlnEdlnRho_ct*dlnEdlnT_cd&
                   +(E/T)*d2lnEdlnRhodlnT &
                   +(P/(d*T))*dlnPdlnT_cd&
                   -(P/(d*T))*dlnPdlnT_cd/dlnPdlnRho_ct&
                   +(P/(d*T))*d2lnPdlnRhodlnT/dlnPdlnRho_ct&
                   -(P/(d*T))*dlnPdlnT_cd*d2lnPdlnRho2_ct/dlnPdlnRho_ct**2d0
!
!                   +(P/(d*T))*dlnPdlnRho_ct*dlnPdlnT_cd&
!                   -(P/(d*T))*dlnPdlnRho_ct*dlnPdlnT_cd &
!
!                   +(P/(d*T))*d2lnPdlnRhodlnT&
!                   -(P/(d*T))*d2lnPdlnRhodlnT &
!
!                   +(P/(d*T))*dlnPdlnT_cd&
!                   -(P/(d*T))*dlnPdlnT_cd !! See page 80 of notebook    
    dCpdlnT_cd =  -(E/T)*dlnEdlnT_cd*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  -(E/T)*d2lnPdlnT2_cd*dlnEdlnRho_ct/dlnPdlnRho_ct&
                  -(E/T)*dlnPdlnT_cd*d2lnEdlnRhodlnT/dlnPdlnRho_ct&
                  +(E/T)*dlnPdlnT_cd*dlnEdlnRho_ct*d2lnPdlnRhodlnT/dlnPdlnRho_ct**2d0&
                  +(E/T)*dlnEdlnT_cd**2&
                  -(E/T)*dlnEdlnT_cd&
                  +(E/T)*d2lnEdlnT2_cd&
                  +(P/(d*T))*dlnPdlnT_cd**2d0 / dlnPdlnRho_ct &
                  -(P/(d*T))*dlnPdlnT_cd / dlnPdlnRho_ct&
                  +(P/(d*T))*d2lnPdlnT2_cd / dlnPdlnRho_ct&
                  -(P/(d*T))*dlnPdlnT_cd*d2lnPdlnRhodlnT/dlnPdlnRho_ct**2d0!&
!
!                  +(P/(d*T))*dlnPdlnT_cd**2 &
!                  -(P/(d*T))*dlnPdlnT_cd**2&
!                  +(P/(d*T))*d2lnPdlnT2_cd&
!                  -(P/(d*T))*d2lnPdlnT2_cd&
!                  +(P/(d*T))*dlnPdlnT_cd&
!                  -(P/(d*T))*dlnPdlnT_cd
    res(i_lnPgas) = lnP
    d_dlnRho_c_T(i_lnPgas) = dlnPdlnRho_ct
    d_dlnT_c_Rho(i_lnPgas) = dlnPdlnT_cd
    res(i_lnE) = lnE
    d_dlnRho_c_T(i_lnE) = dlnEdlnRho_ct
    d_dlnT_c_Rho(i_lnE) = dlnEdlnT_cd
    res(i_lnS) = lnS
    d_dlnRho_c_T(i_lnS) = dlnSdlnRho_ct
    d_dlnT_c_Rho(i_lnS) = dlnSdlnT_cd
    
    res(i_Cp) = Cp
    d_dlnT_c_Rho(i_Cp) = dCpdlnT_cd
    d_dlnRho_c_T(i_Cp) = dCpdlnRho_ct
    
    res(i_grad_ad) = 1.d0/(-dlnSdlnT_cd*dlnPdlnRho_ct/dlnSdlnRho_ct + dlnPdlnT_cd)  !! Checked
    d_dlnRho_c_T(i_grad_ad) = (- res(i_grad_ad)**2d0) &
                                *(-d2lnSdlnRhodlnT*dlnPdlnRho_ct/dlnSdlnRho_ct &
                                  -dlnSdlnT_cd*d2lnPdlnRho2_ct/dlnSdlnRho_ct &
                                  +dlnSdlnT_cd*d2lnSdlnRho2_ct*dlnPdlnRho_ct/(dlnSdlnRho_ct**2d0) &
                                  + d2lnPdlnRhodlnT) 
    d_dlnT_c_Rho(i_grad_ad) = (- res(i_grad_ad)**2d0) &
                              * (-d2lnSdlnT2_cd*dlnPdlnRho_ct/dlnSdlnRho_ct &
                                 -dlnSdlnT_cd * d2lnPdlnRhodlnT/dlnSdlnRho_ct &
                                 +dlnSdlnT_cd * d2lnSdlnRhodlnT*dlnPdlnRho_ct/(dlnSdlnRho_ct**2d0) &
                                 +d2lnPdlnT2_cd)
    
    res(i_chiRho) = dlnPdlnRho_ct
    d_dlnRho_c_T(i_chiRho) = d2lnPdlnRho2_ct
    d_dlnT_c_Rho(i_chiRho) = d2lnPdlnRhodlnT
    
    res(i_chiT) = dlnPdlnT_cd
    d_dlnRho_c_T(i_chiT) = d2lnPdlnRhodlnT
    d_dlnT_c_Rho(i_chiT) = d2lnPdlnT2_cd
    
    res(i_Cv) = (E / T) * dlnEdlnT_cd
    d_dlnRho_c_T(i_Cv) = (E / T) * dlnEdlnRho_ct * dlnEdlnT_cd &
                       + (E / T) * d2lnEdlnRhodlnT
    d_dlnT_c_Rho(i_Cv) = (E / T) * dlnEdlnT_cd**2d0 &
                       - (E / T) * dlnEdlnT_cd &
                       + (E / T) * d2lnEdlnT2_cd
    
    res(i_dE_dRho) = dlnEdlnRho_ct * E / d
    d_dlnRho_c_T(i_dE_dRho) = (E / d) * dlnEdlnRho_ct**2 &
                            - (E / d)*dlnEdlnRho_ct &
                            + (E / d)*d2lnEdlnRho2_ct
    d_dlnT_c_Rho(i_dE_dRho) = (E/d) * dlnEdlnT_cd * dlnEdlnRho_ct &
                            + (E/d) * d2lnEdlnRhodlnT
    
    res(i_dS_dT) = (S / T) * dlnSdlnT_cd 
    d_dlnRho_c_T(i_dS_dt) = (S / T) * dlnSdlnRho_ct * dlnSdlnT_cd &
                          + (S / T) * d2lnSdlnRhodlnT  !! Equation Checked
    d_dlnT_c_Rho(i_dS_dt) = (S / T) * dlnSdlnT_cd**2d0 &
                          - (S / T) * dlnSdlnT_cd &
                          + (S / T) * d2lnSdlnT2_cd  !! Equation Checked

    res(i_dS_dRho) = (S / d) * dlnSdlnRho_ct
    d_dlnRho_c_T(i_dS_dRho) = (S / d) * dlnSdlnRho_ct**2d0 &
                            - (S / d)*dlnSdlnRho_ct &
                            + (S / d) * d2lnSdlnRho2_ct
    d_dlnT_c_Rho(i_dS_dRho) = (S / d) * dlnSdlnT_cd * dlnSdlnRho_ct &
                            + (S / d) * d2lnSdlnRhodlnT  !! Equation Checked
    
      !! This may be a function of Rho and T
!      res(i_mu) = aneos_species_tbl(index)%mu  
!      d_dlnT_c_Rho(i_mu) = 0.
!      d_dlnRho_c_T(i_mu) = 0.

!    res(i_lnfree_e) = 1d-20
!    d_dlnT_c_Rho(i_lnfree_e) = 0.
!    d_dlnRho_c_T(i_lnfree_e) = 0.
    
    res(i_gamma1) = dlnPdlnRho_ct-dlnSdlnRho_ct*dlnPdlnT_cd/dlnSdlnT_cd
    d_dlnRho_c_T(i_gamma1) = d2lnPdlnRho2_ct-d2lnSdlnRho2_ct*dlnPdlnT_cd/dlnSdlnT_cd&
                            -dlnSdlnRho_ct*d2lnPdlnRhodlnT/dlnSdlnT_cd &
                            +dlnSdlnRho_ct*d2lnSdlnRhodlnT*dlnPdlnT_cd/dlnSdlnT_cd**2
    d_dlnT_c_Rho(i_gamma1) = d2lnPdlnRhodlnT &
                            -d2lnSdlnRhodlnT*dlnPdlnT_cd/dlnSdlnT_cd&
                            -dlnSdlnRho_ct*d2lnPdlnT2_cd/dlnSdlnT_cd&
                            +dlnSdlnRho_ct*d2lnSdlnT2_cd*dlnPdlnT_cd/dlnSdlnT_cd**2    !! Page 87
    res(i_gamma3) = 1 - dlnSdlnRho_ct/dlnSdlnT_cd
    d_dlnT_c_Rho(i_gamma3) = -d2lnSdlnRho2_cT/dlnSdlnT_cd &
                            + dlnSdlnRho_ct*d2lnSdlnRhodlnT/dlnSdlnT_cd**2
    d_dlnRho_c_T(i_gamma3) = -d2lnSdlnRhodlnT/dlnSdlnT_cd &
                             +dlnSdlnRho_ct*d2lnSdlnT2_cd/dlnSdlnT_cd**2  !! Derived on page 87 of notebook
    
!      res(i_eta) = 0.
!      d_dlnT_c_Rho(i_eta) = 0.
!      d_dlnRho_c_T(i_eta) = 0.
    
  end subroutine convlnderivs_RES


  subroutine convRhoMatVolMat(RhoMat_in, VolMat_out, num_spec)
    use mixeos_def
    real(dp), dimension(:,:), intent(in) :: RhoMat_in
    real(dp), dimension(:,:), intent(out) :: VolMat_out
    integer, intent(in) :: num_spec

    VolMat_out(1:num_spec,i_val) = 1d0 / RhoMat_in(1:num_spec,i_val)
    VolMat_out(1:num_spec,i_dP) = -1d0 * VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec,i_dP)
    VolMat_out(1:num_spec,i_dT) = -1d0 * VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec,i_dT)
    VolMat_out(1:num_spec,i_dP2) = -2d0 * VolMat_out(1:num_spec,i_val) * VolMat_out(1:num_spec,i_dP)**2d0 &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dP2)
    VolMat_out(1:num_spec,i_dT2) = -2d0 * VolMat_out(1:num_spec,i_val) * VolMat_out(1:num_spec,i_dT)**2d0 &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dT2)
    VolMat_out(1:num_spec,i_dPdT) = -2d0 * VolMat_out(1:num_spec,i_val) &
         * VolMat_out(1:num_spec,i_dP) * VolMat_out(1:num_spec,i_dT) &
         - VolMat_out(1:num_spec,i_val)**2d0 * RhoMat_in(1:num_spec, i_dPdT)

  end subroutine convRhoMatVolMat



  subroutine convVolVecRhoVec(VolVec_in, RhoVec_out)
    use mixeos_def
    real(dp), dimension(:), intent(in) :: VolVec_in
    real(dp), dimension(:), intent(out) :: RhoVec_out

    RhoVec_out(i_val) = 1d0 / VolVec_in(i_val)
    RhoVec_out(i_dP) = -1d0 * RhoVec_out(i_val)**2d0 * VolVec_in(i_dP)
    RhoVec_out(i_dT) = -1d0 * RhoVec_out(i_val)**2d0 * VolVec_in(i_dT)
    RhoVec_out(i_dP2) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dP)**2d0 &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dP2)
    RhoVec_out(i_dT2) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dT)**2d0 &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dT2)
    RhoVec_out(i_dPdT) = -2d0 * RhoVec_out(i_val) * VolVec_in(i_dP) * VolVec_in(i_dT) &
         - RhoVec_out(i_val)**2d0 * VolVec_in(i_dPdT)
    
  end subroutine convVolVecRhoVec

  subroutine AddVolMix(VolMat_in, SMat_in, EMat_in, X_i, num_spec, &
       VolVect_out, EVect_out, SVect_out)
    use mixeos_def
    
    real(dp), dimension(num_spec,num_derivs), intent(in) :: VolMat_in, SMat_in, EMat_in
    real(dp), dimension(num_spec), intent(in) :: X_i
    integer, intent(in) :: num_spec
    real(dp), intent(out) :: VolVect_out(num_derivs), &
         EVect_out(num_derivs), SVect_out(num_derivs)
    integer :: i,j
    
    VolVect_out(:) = 0d0
    SVect_out(:) = 0d0
    EVect_out(:) = 0d0
    
    if(mixdbg) then
       write(*,*) "[AddVolMix/num_spec]", num_spec
    endif
    do i=1,num_spec       
       if(mixdbg) then
          write(*,*), X_i(i)
       endif
       if(X_i(i) .gt. 0d0) then
          do j=1,num_derivs
             VolVect_out(j) = VolVect_out(j) + X_i(i)*VolMat_in(i,j)
             EVect_out(j) = EVect_out(j) + X_i(i)*EMat_in(i,j)
             SVect_out(j) = SVect_out(j) + X_i(i)*SMat_in(i,j)
          enddo
       endif
    enddo

  end subroutine AddVolMix



  !! Check that these two routines are in fact inverse functions of each other
  
  !! Converts from the format [F, dFdX, dFdY, d2FdX2, d2FdY2, d2FdXdY] to 
  !!                      to  [lnF, dlnFdlnX, dlnFdlnY, d2lnFdlnX2, d2lnFdlnY2, d2lnFdlnXdlnY]
  subroutine convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, lnPvect, lnEvect, lnSvect)
    use mixeos_def

    real(dp), intent(in) :: Rho, T
    real(dp), intent(in) :: Pvect(6), Evect(6), Svect(6)
    real(dp), intent(out) :: lnPvect(6), lnEvect(6), lnSvect(6)

    call conv_vector(Pvect, lnPvect)
    call conv_vector(Evect, lnEvect)
    call conv_vector(Svect, lnSvect)
    
    contains

      subroutine conv_vector(Fvect, lnFvect)
        real(dp), intent(in) :: Fvect(6)
        real(dp), intent(out) :: lnFvect(6)

        lnFvect(i_val) = log(Fvect(i_val))
        lnFvect(i_dX) = (Rho / Fvect(i_val)) * Fvect(i_dX)
        lnFvect(i_dY) = (T / Fvect(i_val)) * Fvect(i_dY)
        lnFvect(i_dX2) = lnFvect(i_dX) - lnFvect(i_dX)**2 + (Rho**2 / Fvect(i_val)) * Fvect(i_dX2)
        lnFvect(i_dY2) = lnFvect(i_dY) - lnFvect(i_dY)**2 + (T**2 / Fvect(i_val)) * Fvect(i_dY2)
        lnFvect(i_dXdY) = - lnFvect(i_dX) * lnFvect(i_dY) + (Rho * T / Fvect(i_val)) * Fvect(i_dXdY)
      end subroutine conv_vector

  end subroutine convDerivslnDerivs


  !! Converts from the format [lnF, dlnFdlnX, dlnFdlnY, d2lnFdlnX2, d2lnFdlnY2, d2lnFdlnXdlnY]
  !!  to                      [F, dFdX, dFdY, d2FdX2, d2FdY2, d2FdXdY] to 

  subroutine convlnDerivsDerivs(lnRho, lnT, lnPvect, lnEvect, lnSvect, Pvect, Evect, Svect)
    use mixeos_def

    real(dp), intent(in) :: lnRho, lnT
    real(dp), intent(in) :: lnPvect(6), lnEvect(6), lnSvect(6)
    real(dp), intent(out) :: Pvect(6), Evect(6), Svect(6)
    
    real(dp) :: Rho, T

    Rho = exp(lnRho)
    T = exp(lnT)
    
    call conv_vector(lnPvect, Pvect)
    call conv_vector(lnEvect, Evect)
    call conv_vector(lnSvect, Svect)
    
  contains
    subroutine conv_vector(lnFvect, Fvect)
      real(dp), intent(in) :: lnFvect(6)
      real(dp), intent(out) :: Fvect(6)
      Fvect(i_val) = exp(lnFvect(i_val))
      Fvect(i_dX) = (Fvect(i_val) / Rho) * lnFvect(i_dX)
      Fvect(i_dY) = (Fvect(i_val) / T) * lnFvect(i_dY)
      Fvect(i_dX2) = (Fvect(i_val) / Rho**2 ) *lnFvect(i_dX2) &
                     - (1. / Rho) * Fvect(i_dX) &
                     + (1. / Fvect(i_val)) * (Fvect(i_dX)**2)
      Fvect(i_dY2) = (Fvect(i_val) / T**2) * lnFvect(i_dY2) &
                     - (1. / T) * Fvect(i_dY) &
                     + (1. / Fvect(i_val)) * (Fvect(i_dY)**2)
      Fvect(i_dXdY) = (Fvect(i_val) / (Rho * T)) * &
                        (lnFvect(i_dXdY) + lnFvect(i_dX) * lnFvect(i_dY))
    end subroutine conv_vector
  end subroutine convlnDerivsDerivs
  
  subroutine convDF_thermo(rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)
    use mixeos_def    
    real(dp), intent(in) :: rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3
    real(dp), dimension(:), pointer, intent(out) :: Pvect, Evect, Svect
    real(dp) :: RhoSQ
    
    RhoSQ = rho*rho
    Pvect(i_val) = RhoSQ * df_drho
    Pvect(i_drho) = RhoSQ * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = RhoSQ * d2f_drhodt
    Pvect(i_drho2) = RhoSQ * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = RhoSQ * d3f_drhodt2
    Pvect(i_drhodt) = RhoSQ * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_drhodt
    Svect(i_dt) = -d2f_dt2
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2
  end subroutine convDF_thermo
  

end module thermo
