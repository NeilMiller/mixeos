! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton, Neil Miller
!
!   This file is part of MESA.
!
!   MESA is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Library Public License as published
!   by the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   MESA is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU Library General Public License for more details.
!
!   You should have received a copy of the GNU Library General Public License
!   along with this software; if not, write to the Free Software
!   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
!
! ***********************************************************************
!!
!! Author : Neil Miller
!!
!! Date   : August, 2012
!!
!! Class  : base_eos
!!         
!! This class describes the interface for equation of states.
!! This allows the mixeos_mod to treat each equation of state the same
!! The additive volume method takes N equation of states (P, T) and mixes them 
!! together v(P,T) = sum(X_i * v_i(P,T))
!! we have defined a quantity called "weight" which is meant to describe if the eos
!! should be included in the mixture.  If the weight is not equal to zero, then there
!! should not be an error.  If the weight is zero and there is an error, then it 
!! isn't a problem.

module base_eos_mod  
  use const_def, only: dp
  use mixeos_def
  use inverse_mod
  implicit none

  integer, parameter, public :: ERR_RANGE = -5

  public :: base_eos
  type :: base_eos     
     private
     character(len=256) :: name
   contains
     
     procedure :: Destroy => Destroy_base
     
     procedure :: set_name
     procedure :: get_name
     procedure :: DT_get => DT_get_base 
     procedure :: PT_get => PT_get_base  !! The algorithm defined here assumes that you have overloaded DT_get
     procedure :: PT_get_PRhorange => PT_get_PRhorange_base
     procedure :: PT_get_Trange => PT_get_Trange_base  !! Not sure if we are going to need this function.  Keep it now
  end type base_eos

  type, extends(inverse) :: eos_DT_to_Pinverter
     class(base_eos), pointer :: eos_ptr
   contains
     procedure :: function_actual => eos_lnDlnT_to_lnP
     procedure :: get_ndim => eos_ndim
  end type eos_DT_to_Pinverter
  
contains
  subroutine Destroy_base(this)
    class(base_eos) :: this   
    write(*,*) "[Deallocating ", trim(this%name), " / base]"
  end subroutine Destroy_base
  
  subroutine DT_get_base(this, Rho, T, Pvect, Evect, Svect, weight, ierr)    
    class(base_eos) :: this
    real(dp), intent(in) :: Rho, T
    real(dp), intent(out) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    Pvect(:) = 0d0
    Evect(:) = 0d0
    Svect(:) = 0d0
    weight = 0d0
    ierr = -1
  end subroutine DT_get_base
  
  subroutine set_name(this, name)
    class(base_eos) :: this
    character(len=256) :: name
    this%name = name
  end subroutine set_name
  
  function get_name(this)
    class(base_eos) :: this
    character(len=256) :: get_name
    get_name = trim(this%name)
  end function get_name
  
  !! We assume that the DT is implemented directly, here is a default lookup
  !! you may want to override it.
  !! This could be a problem if the derived type depends on PT_get
  !! and then you could have an infinite loop
  subroutine PT_get_base(this, P, T, Rho, Pvect, Evect, Svect, weight, ierr)
    use const_def !! ln10
    class(base_eos), target :: this
    real(dp), intent(in) :: P, T
    real(dp), intent(out) :: Rho, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    class(base_eos), pointer :: eos_ptr
    type(eos_DT_to_Pinverter) :: DTinv    
    
    real(dp), target :: xvect(2)
    real(dp), pointer, dimension(:) :: x_ptr
    integer :: xvar_index, nstep
    real(dp) :: lnPtgt, lnRhoguess, lnRhomin, lnRhomax, lnPtol, lnRho
    real(dp) :: log10Rho1, log10Rho2, log10P1, log10P2, log10T
    integer :: y_ind

    eos_ptr => this

    !! If this function call is slowing everything down, then we should
    !! make it such that DTinv in a pointer member of the eos_base type    
    DTinv = ConstructEOS_DTinverter(eos_ptr)
    
    x_ptr(1:2) => xvect(1:2)
    x_ptr(2) = log(T)
    
    log10T = log10(T)
    call this%PT_get_PRhorange(log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
    if(ierr .ne. 0) return
    
    lnPtol = 1d-8
    lnRhomin = log10Rho1 * ln10
    lnRhomax = log10Rho2 * ln10
    lnRhoguess = (lnRhomin + lnRhomax) / 2d0
    y_ind = 1
    
    lnRho = DTinv%inverse_function(x_ptr, xvar_index, lnPtgt, y_ind, &
         lnRhoguess, lnRhomin, lnRhomax, lnPtol, ierr, nstep=nstep)
    
    if(ierr .ne. 0) then
       Rho = exp(lnRho)
       call this%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
    endif
  end subroutine PT_get_base
  
  subroutine PT_get_PRhorange_base(this, log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
    class(base_eos) :: this
    real(dp), intent(in) :: log10T
    real(dp), intent(out) :: log10P1, log10P2, log10Rho1, log10Rho2
    integer, intent(out) :: ierr
    ierr = -1
  end subroutine PT_get_PRhorange_base

  subroutine PT_get_Trange_base(this, log10P, log10T1, log10T2, ierr)
    class(base_eos) :: this
    real(dp), intent(in) :: log10P
    real(dp), intent(out) :: log10T1, log10T2
    integer, intent(out) :: ierr
    ierr = -1
  end subroutine PT_get_Trange_base
  
  !! These subroutines are members of the eos_DT_to_Pinverter
  subroutine eos_lnDlnT_to_lnP(this, x_ptr, y_val, y_ind, ierr, dydx_ptr)
    implicit none
    class(eos_DT_to_Pinverter) :: this
    real(dp), pointer, dimension(:), intent(in) :: x_ptr
    real(dp), intent(out) :: y_val
    integer, intent(in) :: y_ind
    integer, intent(out) :: ierr    
    real(dp), optional, pointer, dimension(:), intent(out) :: dydx_ptr
    
    real(dp) :: Rho, T, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    Rho = exp(x_ptr(1))
    T   = exp(x_ptr(2))
    call this%eos_ptr%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
    
    y_val = log(Pvect(i_val))
    if(present(dydx_ptr)) then
       dydx_ptr(1) = (Rho / Pvect(i_val)) * Pvect(i_drho)
       dydx_ptr(2) = (T / Pvect(i_val)) * Pvect(i_dt)
    endif
  end subroutine eos_lnDlnT_to_lnP
  
  subroutine eos_ndim(this, ndim_out)
    implicit none
    class(eos_DT_to_Pinverter) :: this
    integer, intent(out) :: ndim_out
    
    ndim_out = 2
  end subroutine eos_ndim
  
  function ConstructEOS_DTinverter(eos_ptr)
    class(base_eos), pointer, intent(in) :: eos_ptr
    type(eos_DT_to_Pinverter) :: ConstructEOS_DTinverter
    
    ConstructEOS_DTinverter%eos_ptr => eos_ptr
  end function ConstructEOS_DTinverter
  
end module base_eos_mod
