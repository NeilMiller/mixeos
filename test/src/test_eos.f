!! I am developing the module here so that it will take any of the subeos modules as arguments
!!  and produce visualizations so that the user can verify geometrically that it is working

module test_eos
  use mixeos_def
  use mixeos_lib
  implicit none

contains
    
  subroutine output_PT_data(eos_ptr, outdir)
    use base_eos_mod
    use const_def

    class(base_eos), intent(in), pointer :: eos_ptr
    character (len=256), intent(in) :: outdir

    character (len=256) :: filename
    integer, parameter :: nx = 256, ny = 256
    integer :: i,j,ierr, ios
    real(dp) :: logP, logT, P, T, Rho, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs)
    real(dp) :: logP_x(nx), logT_y(ny)
    real(dp) :: logRho_xy(nx,ny), logE_xy(nx,ny), logS_xy(nx,ny)
    real(dp) :: minlogP, maxlogP, dlogP, minlogT, maxlogT, dlogT, weight

    
    minlogP = 12d0
    maxlogP = 18d0
    dlogP = (maxlogP - minlogP) / (nx - 1)

    minlogT = 3d0
    maxlogT = 6.0d0
    dlogT = (maxlogT - minlogT) / (ny - 1)
    
    do i=1,nx
       logP_x(i) = minlogP + (i-1) * dlogP
    enddo

    do j=1,ny
       logT_y(j) = minlogT + (j-1) * dlogT
    enddo

    logRho_xy(:,:) = -100d0
    logE_xy(:,:) = -100d0
    logS_xy(:,:) = -100d0

    
    do i=1,nx
    do j=1,ny
       logP = logP_x(i)
       P = 1d1**logP
       
       logT = logT_y(j)
       T = 1d1**logT

       call eos_ptr%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
       

       if(ierr .eq. 0) then
          if(Rho .lt. 0d0) then
             write(*,*) "Negative density at P = ", P, "; T = ", T
             write(*,*) "Rho = ", Rho
             
             stop
             
          else
             if((Evect(i_val) .gt. 0d0) .and. (Svect(i_val) .gt. 0d0)) then
                logRho_xy(i,j) = log10(Rho)
                logE_xy(i,j) = log10(Evect(i_val))
                logS_xy(i,j) = log10(Svect(i_val))
             else if(Evect(i_val) .le. 0d0) then
                write(*,*) "Evect(i_val) = ", Evect(i_val)
                write(*,*) " P = ", P, "; T = ", T
             else if(Svect(i_val) .le. 0d0) then
                write(*,*) "Svect(i_val) = ", Svect(i_val)
                write(*,*) " P = ", P, "; T = ", T
             endif
          endif
       else
!          write(*,*) "Error at P = ", P, "; T = ", T
       end if
    enddo
    enddo

    filename = trim(outdir) // trim("eos.logP_x.data")
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')
    write(19,'(e14.6)') logP_x
    close(unit=19)

    filename = trim(outdir) // trim("eos.logT_y.data")
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')
    write(19,'(e14.6)') logT_y
    close(unit=19)

    filename = trim(outdir) // trim("eos.logRho_xy.data")
    call write_matrix(logRho_xy, filename)

    filename = trim(outdir) // trim("eos.logE_xy.data")
    call write_matrix(logE_xy, filename)

    filename = trim(outdir) // trim("eos.logS_xy.data")
    call write_matrix(logS_xy, filename)
    
  contains
    
    subroutine write_matrix(matrix, filename)
      real(dp), intent(in) :: matrix(nx,ny)
      character (len=256) :: filename
      
      open(unit=19,file=trim(filename),status='replace', &
           iostat=ios,action='write',form='formatted')       
      write(19,'(e20.6)') matrix
      close(unit=19)
    end subroutine write_matrix
    
  end subroutine output_PT_data
  
end module test_eos


program main
  use test_eos
  use ideal_eos_mod
  use const_def
  use const_lib
  implicit none
  

  type(ideal_eos), target :: my_eos
  class(base_eos), pointer :: eos_ptr
  character(len=256) :: name, out_dir, my_mesa_dir
  real(dp) :: mu
  integer :: ierr

  my_mesa_dir = "/Users/neil/research/mesa"
  call const_init(my_mesa_dir, ierr)
  
  name = "water"
  mu = 18.d0

  call my_eos%Construct(mu)
  call my_eos%set_name(name)

  eos_ptr => my_eos

  out_dir = "ideal_eos_data/"
  call output_PT_data(eos_ptr, out_dir)
  
  call my_eos%Destroy
end program main


