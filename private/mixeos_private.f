!! This should be used by mixeos_*.f lib subroutines that need to access the
!!  metal vector.  Since we have defined the private routines in different modules,
!!  we can't make these variables private within the mixeos_def 

module mixeos_private
  use base_eos_mod
  use mixeos_def, only: NUM_METALS
  use const_lib, only: dp
  implicit none
  
  integer :: ndef_metals = 0  !! Number of tables that have been intialized
  type (base_eos), pointer, dimension(:) :: metal_tbls  !! blank vector of tables
  
  !! The mixeos_chem_id serves 2 purposes.  
  !!  First, it tells you that there is a map between this element
  !!   and an element in the chem module
  !!  Second it tells you which
  !!  isotope this metal coresponds to in the chem vector.
  !! 
  integer :: mixeos_chem_id(NUM_METALS)

  !! using the net_iso vector, you can lookup the species index
  !!  since it is cheap to do that and there could be inconsistencies by
  !!  simultaneously having a mixeos_species_id vector, I have made that
  !!  design decision.
  

contains
  integer function which_eos(isotope_number)
    implicit none
    integer, intent(in) :: isotope_number
    integer :: i
    
    which_eos = 0
    do i=1,NUM_METALS
       if(mixeos_chem_id(i) .eq. isotope_number) then
          which_eos = i
          return
       end if
    end do
  end function which_eos

end module mixeos_private
