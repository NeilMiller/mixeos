module mixeos_def
  use const_def, only: dp
  implicit none
  
  logical, parameter :: mixdbg = .false.
  logical, parameter :: mixdbg0 = .false.
  logical, parameter :: mixdbg1 = .false.
  logical, parameter :: mixdbg_pressure_range = .false.
  logical, parameter :: mixdbg_PT_inverter = .false.
  
  !! INDEX constants
  integer, parameter :: i_P = 1
  integer, parameter :: i_E = 2
  integer, parameter :: i_S = 3
  
  integer, parameter :: i_val = 1
  
  integer, parameter :: i_dRho = 2
  integer, parameter :: i_dlnRho = 2
  integer, parameter :: i_dlnP = 2
  integer, parameter :: i_dP = 2
  integer, parameter :: i_dX = 2
  integer, parameter :: i_dZ = 2
  
  integer, parameter :: i_dT = 3
  integer, parameter :: i_dlnT = 3
  integer, parameter :: i_dY = 3
  
  integer, parameter :: i_dRho2 = 4
  integer, parameter :: i_dlnRho2 = 4
  integer, parameter :: i_dlnP2 = 4
  integer, parameter :: i_dP2 = 4
  integer, parameter :: i_dX2 = 4
  integer, parameter :: i_dZ2 = 4
  
  integer, parameter :: i_dT2 = 5
  integer, parameter :: i_dlnT2 = 5
  integer, parameter :: i_dY2 = 5
  
  integer, parameter :: i_dRhodT = 6
  integer, parameter :: i_dPdT = 6
  integer, parameter :: i_dlnRhodlnT = 6
  integer, parameter :: i_dlnPdlnT = 6
  integer, parameter :: i_dXdY = 6
  integer, parameter :: i_dZdY = 6
  
  integer, parameter :: i_dRho3 = 7
  integer, parameter :: i_dRho2dT = 8
  integer, parameter :: i_dRhodT2 = 9
  integer, parameter :: i_dT3 = 10
  
  !! size of derivative vectors : for allocation
  integer, parameter :: num_derivs = 6
  integer, parameter :: num_Fderivs = 10
  
  !! --TABLE PARAMETER BOUNDARIES
  
  !! EOS boundaries.
  !!! These parameters define the relationship: log10P = alpha * log10T + beta  
  !! Derived from (0,4.5) to (26,9.9)
  real(dp), parameter :: eos_minPTAlpha = 4.d0
  real(dp), parameter :: eos_minPTBeta = -18.d0
  
  !! These two are from the bottom relationship that Bill gave us
  !! defined via (5,2) and (17,5) 
  real(dp), parameter :: eos_maxPTAlpha = 4.d0  
  real(dp), parameter :: eos_maxPTBeta = -2.d0 ! -3. is the default but too tight
  !! Bill also uses (17,5) to (30,6) 
  
  real(dp), parameter :: mixeos_minlog10P = -15d0
  real(dp), parameter :: mixeos_maxlog10P = 35d0
  
  !! Don't fool with these values unless you know what you are doing
  !! The code will assume the values are good.
  real(dp) :: mixeos_minlog10T = 2.d0
  real(dp) :: mixeos_maxlog10T = 8.2d0
  
  !! -- PARAMETER EPSILON
  real(dp), parameter :: BOUNDARY_BUFFER = 0.1
  real(dp), parameter :: mixeos_epsilon = 1d-10
!  real(dp), parameter :: mixeos_logP_tol = 1d-6
!  real(dp), parameter :: mixeos_other_tol = 1d-6
  
  !! -- MAX ITERATION
  integer, parameter :: mixeos_getPT_maxiter = 100  !! Not using?
  integer, parameter :: mixeos_getRhoT_maxiter = 100
  
  !! --ERROR CODES
  !!   PARAMETER ERROR
  integer, parameter :: MIXEOSERR_MINPRESSURE = -180
  integer, parameter :: MIXEOSERR_MAXPRESSURE = -181
  integer, parameter :: MIXEOSERR_MINTEMPERATURE = -182
  integer, parameter :: MIXEOSERR_MAXTEMPERATURE = -183
  
  !!   MEMORY / USAGE RELATED ERRORS
  integer, parameter :: MIXEOSERR_MEM = -32   
  integer, parameter :: MIXEOSERR_OUTOFBOUND = -16
  integer, parameter :: MIXEOSERR_ACCESS = -17
  integer, parameter :: MIXEOSERR_ARGS = -18
  
end module mixeos_def

