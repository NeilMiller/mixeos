program main
  use base_eos_mod
  use ideal_eos_mod
  implicit none
  
  real(8) :: mu = 18d0
  class(base_eos), pointer :: my_eos
  type(ideal_eos), target :: my_ideal_eos
  real(8) :: P, T, Rho
  
  my_ideal_eos = construct_ideal_eos(mu)
  my_eos => my_ideal_eos
  
  Rho = 1d0
  T = 1d3

  
end program main
