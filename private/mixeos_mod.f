module mixeos_mod
  use const_def, only: dp
  use interp1dfunction_mod
  use base_eos_mod
  implicit none

  integer, parameter :: i_rho_flag = 1
  integer, parameter :: i_which_other = 2
  integer, parameter :: i_handle = 3
  integer, parameter :: i_species = 4
  integer, parameter :: i_eos_calls = 5
  integer, parameter :: n_iparams = 5
  
  integer, parameter :: n_rparams = 6
  integer, parameter :: i_other = 1
  integer, parameter :: i_fixed_log_input = 2
  integer, parameter :: i_Z = 3
  integer, parameter :: i_X = 4
  integer, parameter :: i_abar = 5
  integer, parameter :: i_zbar = 6
  
  !! The size of the mixeos_list  
  integer, parameter :: NUM_METALS = 5  
  type,private :: eos_entry
     class (base_eos), pointer :: eos_ptr 
     integer :: isotope_number = 0 !! What isotope number does this species correspond to?
  end type eos_entry
  type(eos_entry), private :: mixeos_list(NUM_METALS) 
  integer, private :: ndef_metals = 0 !! number of defined eos tables

  type(interp1dfunction), private :: mixeos_minlogP_by_logT, mixeos_maxlogP_by_logT
  logical, private :: mixeos_minlogP_bound = .false. , mixeos_maxlogP_bound = .false.
  integer, private, parameter :: NUMBOUNDTEMP = 1000
  
  !! TODO for future programmers
  !! Use the extend the inverse object to determine Rho(P,T) instead of using
  !! Bill's rootfinding routines
  
  !! It seems like there may be some redundancy in this code.
  !!  Can we clean this up?
  !!  I don't want to delete code that is useful

contains
  
  integer function which_eos(isotope_number)
    implicit none
    integer, intent(in) :: isotope_number
    integer :: i

    which_eos = 0
    do i=1,ndef_metals
       if(mixeos_list(i)%isotope_number .eq. isotope_number) then
          which_eos = i
          return
       end if
    end do
  end function which_eos

  subroutine mixeos_get_eos(index, eos_ptr, ierr)
    integer, intent(in) :: index
    class(base_eos), pointer, intent(out) :: eos_ptr
    integer, intent(out) :: ierr
    
    if(mixeos_list(index)%isotope_number .ne. 0) then
       eos_ptr => mixeos_list(index)%eos_ptr
       ierr = 0
    else
       ierr = -1
    end if
  end subroutine mixeos_get_eos
  
  subroutine mixeos_add_eos(eos_ptr, isotope_number, ierr)
    class(base_eos), pointer, intent(in) :: eos_ptr
    integer, intent(in) :: isotope_number
    integer, intent(out) :: ierr
    
    if(isotope_number .le. 0) then
       ierr = -1
       write(*,*) "mixeos_add_eos ERROR"
       write(*,*) "You need to make sure that the name you provide to mixeos_add_eos"
       write(*,*) "matches the value found in the isotope name vector"
       write(*,*) " initialized inside isotopes.data by chem_init"          
       stop
    elseif(ndef_metals .lt. NUM_METALS) then
       ndef_metals = ndef_metals + 1
       mixeos_list(ndef_metals)%eos_ptr => eos_ptr
       mixeos_list(ndef_metals)%isotope_number = isotope_number
       ierr = 0
       write(*,*) "[mixeos_add_eos] Added ", trim(eos_ptr%get_name()), " at index = ", ndef_metals
    else
       ierr = -1
       write(*,*) "You are attempting to add too many metals to the table"
       write(*,*) "It is easy to fix - go to mixeos_mod and increase NUM_METALS"
       write(*,*) " or just don't use so many metals"
       stop
    end if
    
    call build_PT_boundary_grid(ierr)
  end subroutine mixeos_add_eos
  
  subroutine mixeos_clean_list
    integer :: i
    
    do i=1, ndef_metals
       if(associated(mixeos_list(i)%eos_ptr)) then
          call mixeos_list(i)%eos_ptr%Destroy()
          deallocate(mixeos_list(i)%eos_ptr)
          nullify(mixeos_list(i)%eos_ptr)
       endif
    enddo
    
  end subroutine mixeos_clean_list
  
  !! I believe that the get_res_PT code works
  
  !! This routine determines the minimum and maximum pressure that are valid for a given temperature
  !! It is my understanding that this is primarily used by build_PT_boundary_grid 
  !! Perhaps it should be contained?
  subroutine get_pressure_range( & 
       T, log10T, log10P1, log10P2, ierr)
    use base_eos_mod
    use mixeos_def
    use const_lib, only: dp
    implicit none
    
    real(dp), intent(in) :: T, log10T
    real(dp), intent(out) :: log10P1, log10P2
    integer, intent(out) :: ierr
    
    class (base_eos), pointer :: current_metal_tbl
    real(dp) :: m_log10P1, m_log10P2, m_log10Rho1, m_log10Rho2
    integer :: i
    
    log10P1 = mixeos_minlog10P
    log10P2 = mixeos_maxlog10P
    
    ierr = 0
    
    !! Regardless of anything, only work within the temperature range
    if(log10T .lt. mixeos_minlog10T) then
       ierr = MIXEOSERR_MINTEMPERATURE
       return
    endif
    
    if(log10T .gt. mixeos_maxlog10T) then
       ierr = MIXEOSERR_MAXTEMPERATURE
       return
    endif
    
    !! Always include Hydrogen and Helium bound
    log10P1 = eos_minPTAlpha * log10T + eos_minPTBeta + 0.0001
    log10P2 = eos_maxPTAlpha * log10T + eos_maxPTBeta - 0.0001
    if(mixdbg_pressure_range) then
       write(*,*) "H/He logPRange: ", log10P1, log10P2
    endif
    

    do i=1, NUM_METALS 
       if(mixeos_list(i)%isotope_number .gt. 0) then  !! If the metal is defined it is important
          call mixeos_list(i)%eos_ptr%PT_get_PRhorange(log10T, m_log10P1, m_log10P2, m_log10Rho1, m_log10Rho2, ierr)
          if(ierr .ne. 0) then
             write(*,*) "There was an error looking up the PRho range for subeos "
             write(*,*) "logT = ", log10T
             write(*,*) "EOS name: ", mixeos_list(i)%eos_ptr%get_name()
          else
             if(mixdbg_pressure_range) then
                write(*,*) " eos logP range: ", m_log10P1, m_log10P2, ierr
             endif
             
             if(m_log10P1 .gt. log10P1) then
                log10P1 = m_log10P1
             endif
             
             if(m_log10P2 .lt. log10P2) then
                log10P2 = m_log10P2
             endif             
          endif
       endif
    end do
    
  end subroutine get_pressure_range
  
  !! USe the 
  subroutine build_PT_boundary_grid(ierr)
    use const_lib, only: dp, ln10
    use interp_1d_lib
    use mixeos_def
    implicit none
    
    integer, intent(out) :: ierr
    integer :: ierr_par = 0
    
    real(dp) :: T, dlogT, log10T, log10P1, log10P2
    integer :: i
    
    
    real(dp), target :: minP_logT_vect(NUMBOUNDTEMP), &
                        maxP_logT_vect(NUMBOUNDTEMP), &
                        minlogP_vect(NUMBOUNDTEMP), &
                        maxlogP_vect(NUMBOUNDTEMP)
    real(dp), pointer :: logTv_ptr(:), &
                         logPv_ptr(:)
    integer :: minlogP_count = 0, maxlogP_count = 0

    ierr = 0
    dlogT = (mixeos_maxlog10T - mixeos_minlog10T) / (real(NUMBOUNDTEMP) - 1d0)
    do i=1,NUMBOUNDTEMP
       
       log10T = mixeos_minlog10T + dlogT * (real(i - 1))
       T = 1d1**log10T
       ierr_par = 0
       ierr = 0
       call get_pressure_range( &
            T, log10T, log10P1, log10P2, ierr_par)
       
       !! CHECK this algorithm!! Does it make sense?
       if(ierr_par .eq. 0) then
          minlogP_count = minlogP_count + 1
          minP_logT_vect(minlogP_count) = log10T
          minlogP_vect(minlogP_count) = log10P1

          maxlogP_count = maxlogP_count + 1
          maxP_logT_vect(maxlogP_count) = log10T
          maxlogP_vect(maxlogP_count) = log10P2           
       endif
       
    enddo

    if(minlogP_count .ge. 3) then
       logTv_ptr => minP_logT_vect(1:minlogP_count)
       logPv_ptr => minlogP_vect(1:minlogP_count)
       call mixeos_minlogP_by_logT%Construct(logTv_ptr, logPv_ptr, &
            minlogP_count, ierr)
       if(mixdbg_pressure_range) then
          write(*,*) "minlogP points"
          do i=1,minlogP_count
             write(*,*) logTv_ptr(i), logPv_ptr(i)
          enddo
       endif
       mixeos_minlogP_bound = .true.
    else
       write(*,*) "Not enough points to determine a minimum boundary"
       write(*,*) "Using minlogP = ", mixeos_minlog10P
       mixeos_minlogP_bound = .false.
    endif

    if(maxlogP_count .ge. 3) then
       logTv_ptr => maxP_logT_vect(1:maxlogP_count)
       logPv_ptr => maxlogP_vect(1:maxlogP_count)
       
       call mixeos_maxlogP_by_logT%Construct(logTv_ptr, logPv_ptr, &
            maxlogP_count, ierr)
       if(mixdbg_pressure_range) then
          write(*,*) "maxlogP points"
          do i=1,maxlogP_count
             write(*,*) logTv_ptr(i), logPv_ptr(i)
          enddo
       endif
       mixeos_maxlogP_bound = .true.
    else
       write(*,*) "Not enough points to determine a maximum boundary"
       write(*,*) "Using maxlogP = ", mixeos_maxlog10P
       mixeos_maxlogP_bound = .false.
    endif
  end subroutine build_PT_boundary_grid
  
  !! Custom boundary?
  !!  - we can build a tighter relationship
  
  !! Am I using this correctly?
  !! Perhaps seperate into two functions since there isn't really any common code
  subroutine PT_find_domain(Xval, log10X, X_is_P, log10Y1, log10Y2, ierr)
    use mixeos_def
    use const_lib, only:dp, ln10
    implicit none
    
    !! Check to make sure that we are in a valid range
    integer, intent(in) :: X_is_P !! 1 if X == P, 0 if X == T
    real(dp), intent(in) :: Xval, log10X
    real(dp), intent(out) :: log10Y1, log10Y2 
    integer, intent(out) :: ierr
    
    real(dp) :: log10T, log10P, rel_minlogT, rel_maxlogT, &
         rel_minlogP, rel_maxlogP
    
    ierr = 0
    

    !! CHECK that we are correctly treating the doing_P flag
    !! I changed this from doing_P eq 1
    if(X_is_P .eq. 0) then
       log10T = log10X
       if(log10T .lt. mixeos_minlog10T) then
          ierr = MIXEOSERR_MINTEMPERATURE
          return
       elseif(log10T .gt. mixeos_maxlog10T) then
          ierr = MIXEOSERR_MAXTEMPERATURE
          return
       endif
       
       log10Y1 = mixeos_minlog10P
       log10Y2 = mixeos_maxlog10P
       
       if(mixeos_minlogP_bound) then
          call mixeos_minlogP_by_logT%get_domain(rel_minlogT, rel_maxlogT, ierr)
          if((log10T .gt. rel_minlogT) .and. (log10T .lt. rel_maxlogT)) then
             call mixeos_minlogP_by_logT%interp(log10T, log10Y1, ierr)
             if(ierr .ne. 0) then
                write(*,*) "Unexpected interpolation error"
             endif
          endif
       endif
       
       if(mixeos_maxlogP_bound) then
          call mixeos_maxlogP_by_logT%get_domain(rel_minlogT, rel_maxlogT, ierr)
          if((log10T .gt. rel_minlogT) .and. (log10T .lt. rel_maxlogT)) then
             call mixeos_maxlogP_by_logT%interp(log10T, log10Y2, ierr)
             if(ierr .ne. 0) then
                write(*,*) "Unexpected interpolation error"
             endif
          endif
       endif
    else
       log10P = log10X
       if(mixeos_minlogP_bound) then
          call mixeos_minlogP_by_logT%get_range(rel_minlogP, rel_maxlogP, ierr)          
       else
          rel_minlogP = mixeos_minlog10P
       end if       
       if(log10P .lt. rel_minlogP) then
          ierr = MIXEOSERR_MINPRESSURE
          return
       endif
       
       if(mixeos_maxlogP_bound) then
          call mixeos_maxlogP_by_logT%get_range(rel_minlogP, rel_maxlogP, ierr)
       else
          rel_maxlogP = mixeos_maxlog10P
       endif
       if(log10P .gt. rel_maxlogP) then
          ierr = MIXEOSERR_MAXPRESSURE
          return
       endif
       
       log10Y1 = mixeos_minlog10T !! These values may be a bit crazy
       log10Y2 = mixeos_maxlog10T
       
       if(mixeos_maxlogP_bound) then
          call mixeos_maxlogP_by_logT%get_range(rel_minlogP, rel_maxlogP, ierr)
          if((log10P .ge. rel_minlogP) .and. (log10P .le. rel_maxlogP)) then
             call mixeos_maxlogP_by_logT%inv(log10P, log10Y1, ierr)
          end if
       end if
       
       if(mixeos_minlogP_bound) then
          call mixeos_minlogP_by_logT%get_range(rel_minlogP, rel_maxlogP, ierr)
          if((log10P .ge. rel_minlogP) .and. (log10P .le. rel_maxlogP)) then
             call mixeos_minlogP_by_logT%inv(log10P, log10Y2, ierr)
          endif
       end if
    endif
    
    log10Y1 = log10Y1 + BOUNDARY_BUFFER
    log10Y2 = log10Y2 - BOUNDARY_BUFFER
    
  end subroutine PT_find_domain
  
  
  subroutine PT_find_range(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       Xval, log10X, which_other, other_value, X_is_P, &
       log10Y1, log10Y2, other1, other2, ierr, extended_range)
    use interp_1d_lib
    use mixeos_def
    use const_lib, only:dp, ln10
    use eos_def
    
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    integer, intent(in) :: X_is_P !! 1 if X == P, 0 if X == T
    integer, intent(in) :: species
    integer, pointer, dimension(:) :: chem_id, net_iso
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: Xval, log10X
    real(dp), intent(out) :: log10Y1, log10Y2, other1, other2
    integer, intent(out) :: ierr
    integer, intent(in), optional :: extended_range !! Set this to 1 if you want 
    
    real(dp), pointer, dimension(:) :: res, &
         d_dlnRho_c_T, d_dlnT_c_Rho
    integer :: ierr1 = -1, ierr2 = -1
    
    real(dp) :: T, log10T, P, log10P, Rho

    !! Maybe we use this for binomial search, maybe not
    real(dp) :: logY_test, other_test, dother_dlogY

    !! Use the relationship given from mixeos_min_logPbound_vect, mixeos_max_logPbound_vect
    !! to get a tighter boundaries here.  It is still likely that the values in the boundary
    !! are not actually valid points
    
    allocate(res(num_eos_basic_results))
    allocate(d_dlnRho_c_T(num_eos_basic_results))
    allocate(d_dlnT_c_Rho(num_eos_basic_results))

    call PT_find_domain(Xval, log10X, X_is_P, log10Y1, log10Y2, ierr)
    
    if(mixdbg .and. (ierr .ne. 0)) then
       write(*,*) "[PT_find_range] PT_find_domain failed: ", ierr
       write(*,*) "Xval = ", Xval
       write(*,*) "log10X = ", log10X
       write(*,*) "log10Y1 = ", log10Y1
       write(*,*) "log10Y2 = ", log10Y2
    endif
    
!    write(*,*) "Determined logYrange: ", log10Y1, log10Y2
    
    !! Do we want to actually evaluate the end points?
    !! Or do we want to just do a search through bisection?
    call setup_variables(log10X, log10Y1)
    
    if(mixdbg) then
       write(*,*) "Setup variables low"
       write(*,*) " P : ", P, log10P
       write(*,*) " T : ", T, log10T
    endif
    if(.not. associated(net_iso)) then
       write(*,*) "[PT_find_range] net_iso not associated"
    endif

    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr1)
    
    if(ierr1 .eq. 0) then
       if(which_other .eq. 0) then
          other1 = log10(Rho)
       else
          other1 = res(which_other)       
       end if
    else
       ierr = ierr1
       write(*,*) "FAIL to find good bounds!!!"
       write(*,*) "ierr = ", ierr, " for min range"
    endif
    
!    write(*,*) "called get_res_PT"

    call setup_variables(log10X, log10Y2)
    
    if(mixdbg) then
       write(*,*) "Setup variables high"
       write(*,*) " P : ", P, log10P
       write(*,*) " T : ", T, log10T
    endif
    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr2)
    
    if(ierr2 .eq. 0) then
       if(which_other .eq. 0) then
          other2 = log10(Rho)
       else
          other2 = res(which_other)
       end if
    else
       ierr = ierr2
       write(*,*) "FAIL to find good bounds!!!"
       write(*,*) "ierr = ", ierr, " for max range"
    endif
    
    if(ierr .eq. 0) then
       if((other_value - other1) * (other_value - other2) .gt. 0d0) then
          !! The function did work, but the value is not between the two outer boundaries
          if(mixdbg) then
             write(*,*) "PT_find_range not in range: ", ierr
             write(*,*) "other_value = ", other_value
             write(*,*) "other1 = ", other1
             write(*,*) "other2 = ", other2
             write(*,*) "which_other = ", which_other
             if(X_is_P .eq. 1) then
                write(*,*) "logP = ", log10X
             else
                write(*,*) "logT = ", log10X
             endif
          end if
          ierr = MIXEOSERR_OUTOFBOUND
       end if
    end if
    
    deallocate(res)
    deallocate(d_dlnRho_c_T)
    deallocate(d_dlnT_c_Rho)
  contains
    subroutine setup_variables(log10X_input, log10Y_input)
      real(dp) :: log10X_input, log10Y_input
      if(X_is_P .eq. 0) then
         log10P = log10Y_input
         P = 1d1**log10P
         log10T = log10X_input
         T = 1d1**log10T
      else
         log10P = log10X_input
         P = 1d1**log10P
         log10T = log10Y_input
         T = 1d1**log10T
      endif      
    end subroutine setup_variables
    
  end subroutine PT_find_range
  
  !! THIS is the meat of things.  
  !! Species are mixed via additive volume so constant (P,T)
  
  subroutine get_res_PT(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       P, log10P, T, log10T, Rho,  &
       res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
    use eos_def
    use eos_lib, only: eosPT_get, eosDT_get_Rho
    use mixeos_def
    use base_eos_mod
    use thermo
    use const_def, only: arg_not_provided
    use chem_def
    
    implicit none
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in):: xa(:) !! mass fractions
    real(dp), intent(in) :: P, log10P, T, log10T    
    real(dp), intent(out) :: Rho
    real(dp), intent(out), dimension(:) :: res_output, &
         d_dlnRho_c_T_output, d_dlnT_c_Rho_output
    integer, intent(out) :: ierr
    
    real(dp), pointer, dimension(:) ::  & !! Need the target attribute here too
         Pvect, lnPvect, &
         Svect, lnSvect, &
         Evect, lnEvect
    real(dp), target, dimension(num_derivs, NUM_METALS+1) :: & !! Need the target attribute
         RhoMat_PT, VolMat_PT, SMat_PT, EMat_PT
    real(dp), dimension(num_derivs) ::  &  
         VolVect_mixPT, RhoVect_mixPT, EVect_mixPT, SVect_mixPT, &
         Pvect_mixRhoT, Evect_mixRhoT, Svect_mixRhoT, &
         lnPVect_mixRhoT, lnEVect_mixRhoT, lnSVect_mixRhoT
    
    real(dp) :: eos_res(num_eos_basic_results), &
         eos_dlnRho_dlnPgas_const_T, &
         eos_d_dlnRho_dlnT_const_Pgas, &
         eos_d_dlnRho_const_T(num_eos_basic_results), &
         eos_d_dlnT_const_Rho(num_eos_basic_results), &
         hhe_Rho, hhe_log10Rho, hhe_lnRho, lnP, lnT
    real(dp) :: eos_Fvect(num_Fderivs)
    real(dp) :: hhe_Pvect(num_derivs), hhe_lnPvect(num_derivs), &
         hhe_Evect(num_derivs), hhe_lnEvect(num_derivs), &
         hhe_Svect(num_derivs), hhe_lnSvect(num_derivs)
    
    integer :: i
    class (base_eos), pointer :: current_eos_ptr
    real(dp), pointer, dimension(:) :: current_Rhovect_PT, current_Evect_PT, current_Svect_PT
    
    !! X_i is the vector that is passed to AddVolMix
    !! The code below must ensure that the xa vector is correctly
    !! mapped to the X_i vector 
    real(dp), target, dimension(NUM_METALS+1) :: X_i  
    real(dp) :: lnRho, logRhoGuess, logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2
!    real(dp) :: helm_res(num_helm_results)
    integer :: actual_eos_calls
    
    real(dp) :: Xsum_other, Xarg, Yarg, Zarg, abar_arg, zbar_arg
    real(dp) ::  log10P1, log10P2
    integer :: X_is_P

    integer :: cid
    real(dp) :: tmp_x(species), tmp_y(species), tmp_z(species), xsum_eos, weight
    
    !! Intuitive classification of zones
    !! In the "safe" zone, every eos in the mixture should return without an error
    !! In the "grey" zone, some eos calls may return an error, but should have weight zero
    !!  at least one eos must not return an error
    logical :: safe_zone = .false.  
    logical :: grey_zone = .false.

    res_output(:) = 0d0
    d_dlnRho_c_T_output(:) = 0d0
    d_dlnT_c_Rho_output(:) = 0d0

    grey_zone = .false.
    safe_zone = .false.
    if((log10T .gt. mixeos_minlog10T) .and. (log10T .lt. mixeos_maxlog10T)) then
       X_is_P = 0
       call PT_find_domain(T, log10T, X_is_P, log10P1, log10P2, ierr)
       if((log10P1 .lt. log10P) .and. (log10P2 .gt. log10P)) then
          safe_zone = .true.
!       elseif((mixeos_minlog10P .lt. log10P) .and. (mixeos_maxlog10P .gt. log10P)) then
!          grey_zone = .true.  
       endif
    else
!       write(*,*) "Red Zone"
!       write(*,*) "grey_zone = ", grey_zone
!       write(*,*) "safe_zone = ", safe_zone
    endif
    

    !! If we are outside of the safe bounds, then we should still
    !! attempt by calling the normal eos
    if((.not. safe_zone) .and. (.not. grey_zone)) then  
       Xarg     = X
       Zarg     = Z
       abar_arg = abar
       zbar_arg = zbar
       ierr     = 0

       call eosPT_get(eos_handle, Zarg, Xarg, abar_arg, zbar_arg, &
            species, chem_id, net_iso, tmp_x, &
            P, log10P, T, log10T, &
            hhe_Rho, hhe_log10Rho, eos_dlnRho_dlnPgas_const_T, eos_d_dlnRho_dlnT_const_Pgas, &
            eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, ierr)     

       lnRho = log(hhe_Rho)
       lnT = log(T)
!       call ConvRes_F(hhe_Rho, hhe_log10Rho, T, log10T, &
!            eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, &
!            eos_Fvect)
!       call ConvF_derivs(hhe_Rho, T, eos_Fvect, &
!            hhe_Pvect, hhe_Evect, hhe_Svect)      
!       call convDerivslnDerivs(hhe_Rho, T, hhe_Pvect, hhe_Evect, hhe_Svect, &
!            hhe_lnPvect, hhe_lnEvect, hhe_lnSvect)
!       call convlnderivs_RES(lnRho, lnT, hhe_lnPvect, hhe_lnEvect, hhe_lnSvect, &
!            res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output)

       res_output = eos_res
       d_dlnRho_c_T_output = eos_d_dlnRho_const_T
       d_dlnT_c_Rho_output = eos_d_dlnT_const_Rho
       if(ierr .eq. 0) then
          return
       else
          write(*,*) "[get_res_PT] error ", log10P, log10T
          write(*,*) " log10T range: ", mixeos_minlog10T, mixeos_maxlog10T
          if((log10T .gt. mixeos_minlog10T) .and. (log10T .lt. mixeos_maxlog10T)) then
             write(*,*) " safe log10P range: ", log10P1, log10P2
             write(*,*) " grey log10P range: ", mixeos_minlog10P, mixeos_maxlog10P
          endif
          ierr = MIXEOSERR_OUTOFBOUND
          return
       endif
    else
    
!    write(*,*) "Inside get_res_PT", num_derivs
    
       allocate(Pvect(num_derivs), lnPvect(num_derivs), Svect(num_derivs), &
            lnSvect(num_derivs), Evect(num_derivs), lnEvect(num_derivs))
    
!    write(*,*) "variables allocated"
!    write(*,*) "Cleared out output variables"

       RhoMat_PT(:,:) = 1.d0
       EMat_PT(:,:) = 1.d0
       SMat_PT(:,:) = 1.d0
       X_i(:) = 0d0
       
!    write(*,*) "Zeroed out matrix"

       lnP = log(P)
       lnT = log(T)    
    !! Call the standard eosPT_get(P,T) -> Rho, results
       hhe_Rho = 0d0
       hhe_log10Rho = 0d0
       hhe_lnRho = 0d0
       eos_res(:) = 0d0
       eos_d_dlnRho_const_T(:) = 0d0
       eos_d_dlnT_const_Rho(:) = 0d0
        
!    write(*,*) "set eos_results to zero"

       X_i(1) = 0d0
!    write(*,*) net_iso(ih1), net_iso(ihe4)
       if(.not. associated(net_iso)) then
          write(*,*) "Net iso is not allocated"
       endif
       current_Rhovect_PT => RhoMat_PT(1,1:num_derivs)
       current_Evect_PT   => EMat_PT(1,1:num_derivs)
       current_Svect_PT   => SMat_PT(1,1:num_derivs)
       
       ierr = 0
       
       if(log10T .lt. mixeos_minlog10T) then
          ierr = MIXEOSERR_MINTEMPERATURE
          return
       endif
    
       if(log10T .gt. mixeos_maxlog10T) then
          ierr = MIXEOSERR_MAXTEMPERATURE
          write(*,*) "mixeos logT too big!! : logT = ", log10T, " limit ", mixeos_maxlog10T
          return
       endif
       
       Xsum_other = 0d0
       do i=1, ndef_metals
          if(net_iso(mixeos_list(i)%isotope_number) .eq. 0) then
             X_i(i+1) = 0d0
          else
             X_i(i+1) =  xa(net_iso(mixeos_list(i)%isotope_number))
             Xsum_other = Xsum_other + X_i(i+1)
          endif
       end do
       X_i(1) = 1d0 - Xsum_other
    
       
       if(X_i(1) .gt. 0d0) then
          
       !! Xarg, Yarg, Zarg is the composition for the sake of the "normal" eos
       !! It is important that a correct abar_arg and zbar_arg are calculated
          Xarg = xa(net_iso(ih1)) / X_i(1)
          Yarg = xa(net_iso(ihe4)) / X_i(1)
          Zarg = (1d0 - Xarg - Yarg)

          tmp_x(1:species) = 0d0
          tmp_y(1:species) = 0d0
          tmp_z(1:species) = 0d0
          do i=1,species
             cid = chem_id(i)
             if(which_eos(cid) .eq. 0) then             
                tmp_x(i) = xa(i)
                tmp_z(i) = chem_isos%Z(cid)
                tmp_y(i) = xa(i) / chem_isos% Z_plus_N(cid)
             endif
          enddo
       
          xsum_eos = sum(tmp_x(1:species))  !! This could be less than one in this case
          abar_arg = xsum_eos/sum(min(1d0,max(tmp_y(1:species),1.0d-50)))
          zbar_arg = abar * sum(tmp_y(1:species)*tmp_z(1:species))/xsum_eos
          
       !! IT IS ABSOLUTELY IMPORTANT THAT ALL OF THESE ARGUMENTS ARE RIGHT
       !! MAKE SURE THAT Z IS ZERO WHEN PASSED TO THE MESA EOS MODULE SINCE
       !! WE ARE PERFORMING ADDITIVE VOLUME.  I THINK THE ISSUE IS THAT THE
       !! ABAR AND ZBAR PARAMETERS AFFECT THE RESULTS OF THE TABLE AND IF THESE
       !! ARE MISCONFIGURED THEN YOU GET A PROBLEM.
       
          tmp_x = tmp_x * (1d0 / xsum_eos) !! This is a version of the xa vector
       !! that only includes the elements that are in the regular eos
       !! and is normalized

       !! if my calculation for abar and zbar don't work use the following
!       subroutine get_composition_info(
!     >      num_isos, chem_id, x, xh, xhe, 
!     >      abar, zbar, z2bar, ye, mass_correction,
!     >      sumx, skip_partials, dabar_dx, dzbar_dx, dmc_dx)
          call eosPT_get(eos_handle, Zarg, Xarg, abar_arg, zbar_arg, &
               species, chem_id, net_iso, tmp_x, &
               P, log10P, T, log10T, &
               hhe_Rho, hhe_log10Rho, eos_dlnRho_dlnPgas_const_T, eos_d_dlnRho_dlnT_const_Pgas, &
               eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, ierr)
       
          if(ierr .ne. 0) then
             write(*,*) "eosPT_get failed"
             write(*,*) "P input: ", P
             write(*,*) "log10P input: ", log10P
             write(*,*) "T input: ", T
             write(*,*) "log10T input: ", log10T
             return
          endif
       
!       write(*,*) "[get_res_PT] eosPT_get res: ", eos_res
!       write(*,*) "[get_res_PT] eosPT_get ierr: ", ierr
       
          hhe_lnRho = log(hhe_Rho)
          if(mixdbg) then
             write(*,*) "[Mixeos_mod]"
             write(*,*) "hhe_Rho: ", hhe_Rho
             write(*,*) "hhe_log10Rho: ", hhe_log10Rho
          endif
          
          call convRES_lnderivs(eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, &
               hhe_lnRho, lnT, hhe_lnPvect, hhe_lnEvect, hhe_lnSvect)
          call convlnDerivsDerivs(hhe_lnRho, lnT, hhe_lnPvect, hhe_lnEvect, hhe_lnSvect, &
               hhe_Pvect, hhe_Evect, hhe_Svect)
          
!       call ConvRes_F(hhe_Rho, hhe_log10Rho, T, log10T, &
!            eos_res, eos_d_dlnRho_const_T, eos_d_dlnT_const_Rho, &
!            eos_Fvect)
!       call ConvF_derivs(hhe_Rho, T, eos_Fvect, &
!            hhe_Pvect, hhe_Evect, hhe_Svect)

          call convRhoT_PT(hhe_Rho, T, P, &
               hhe_Pvect, hhe_Evect, hhe_Svect, & !! These have (Rho, T) independent
               current_Rhovect_PT, current_Evect_PT, current_Svect_PT)
       
!       write(*,*) "[H/He RhoVect] ", current_Rhovect_PT
       else
       !! These are weighted with zero weight
       !! It is good to have this be a non-zero number though so that
       !! if the pipeline inverts them we don't have NaN seeping in
          current_Rhovect_PT(:) = 1d0
          current_Evect_PT(:) = 1d0
          current_Svect_PT(:) = 1d0
       endif
       
       if(mixdbg) then
          if(ierr .ne. 0) then
             write(*,*) "We've got a problem"
          endif
       endif
       
       if(ierr .ne. 0) then
          return
       endif
    
    !! Call get(P,T) -> Rho, results for each of the component species
       metal_loop : do i=1, ndef_metals
          if(mixeos_list(i)%isotope_number .ne. 0) then
             current_eos_ptr => mixeos_list(i)%eos_ptr
             current_Rhovect_PT => RhoMat_PT((i+1), 1:num_derivs)
             current_Evect_PT   => EMat_PT((i+1), 1:num_derivs)
             current_Svect_PT   => SMat_PT((i+1), 1:num_derivs)
             
             if(X_i(i+1) .gt. 0.d0) then  
                !! Should we also check that the h5table is initialized???
                call current_eos_ptr%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
                X_i(i+1) = X_i(i+1) * weight
                if(mixdbg) then
                   write(*,*) "[mixeos/h5_get_Rho results]", i
                   write(*,*) "P: ", P
                   write(*,*) "T: ", T
                   write(*,*) "Rho: ", Rho
                   write(*,*) "Pvect: ", Pvect
                   write(*,*) "Evect: ", Evect
                   write(*,*) "Svect: ", Svect
                   write(*,*) "ierr: ", ierr
                endif
             
                if(ierr .eq. 0) then
                   call convRhoT_PT(Rho, T, P, &
                        Pvect, Evect, Svect, & 
                        current_Rhovect_PT, current_Evect_PT, current_Svect_PT)
                elseif(grey_zone .and. (weight .eq. 0d0)) then
                   current_Rhovect_PT(1:num_derivs) = 1d0
                   current_Evect_PT(1:num_derivs) = 1d0
                   current_Svect_PT(1:num_derivs) = 1d0
                   ierr = 0
                   !                exit metal_loop  
                else
                   !! There really is a problem
                   exit metal_loop
                endif
             endif
          end if
       
       enddo metal_loop
    
    !! Re normalize the mass fractions
    !!  since some of the equations of state may have failed
       if(grey_zone) then
          xsum_eos = sum(X_i(1:(ndef_metals+1)))
          X_i(1:(ndef_metals+1)) = X_i(1:(ndef_metals+1)) / xsum_eos
       endif
    
       if(ierr .eq. 0) then
          
          if(mixdbg) then
             write(*,*) "[mixeos PT deriv matrix]"
             write(*,*) "[mixeos PT/RhoMat] "
             do i=1,NUM_METALS+1
                write(*,*) RhoMat_PT(i,1:num_derivs)
             enddo
             write(*,*) "[mixeos_PT/EMat] "
             do i=1,NUM_METALS+1
                write(*,*) EMat_PT(i,1:num_derivs)
             enddo
             write(*,*) "[mixeos_PT/SMat] "
             do i=1,NUM_METALS+1
                write(*,*) SMat_PT(i,1:num_derivs)
             end do
          endif
          call convRhoMatVolMat(RhoMat_PT, VolMat_PT, NUM_METALS+1)
          if(mixdbg)  then
             write(*,*) "[mixeos_PT/VolMat]"
             do i=1,NUM_METALS+1
                write(*,*) VolMat_PT(i,1:num_derivs)
             enddo
          endif
          call AddVolMix(VolMat_PT, SMat_PT, EMat_PT, X_i, NUM_METALS+1, &
               VolVect_mixPT, EVect_mixPT, SVect_mixPT)
          if(mixdbg) then
             write(*,*) "[mixeos_PT/AddVolMix Vector] "
             write(*,*) "Vol: ", VolVect_mixPT
             write(*,*) "E: ", EVect_mixPT
             write(*,*) "S: ", SVect_mixPT
          endif
          
          call convVolVecRhoVec(VolVect_mixPT, RhoVect_mixPT)
          
          if(mixdbg) then
             write(*,*) "[mixeos_PT/RhoVect_PT]: ", RhoVect_mixPT
          endif
          
          Rho = RhoVect_mixPT(i_val)
          lnRho = log(Rho)
          
          if(mixdbg) then
             write(*,*) "Rho = ", Rho
             write(*,*) "lnRho = ", lnRho
          endif
          
          call convPT_RhoT(Rho, T, P, &
               Rhovect_mixPT, EVect_mixPT, SVect_mixPT, &
               PVect_mixRhoT, EVect_mixRhoT, SVect_mixRhoT)
          
          if(mixdbg) then
             write(*,*) "[mixeos_PT/Convert PT -> RhoT]"
             write(*,*) "PVecRhoT: ", PVect_mixRhoT
             write(*,*) "EvecRhoT: ", EVect_mixRhoT
             write(*,*) "SvecRhoT: ", SVect_mixRhoT
          endif
          
          call convDerivslnDerivs(Rho,T,PVect_mixRhoT, EVect_mixRhoT, SVect_mixRhoT, &
               lnPVect_mixRhoT, lnEVect_mixRhoT, lnSVect_mixRhoT)
          
          if(mixdbg) then
             write(*,*) "[mixeos_PT/Convert RhoT -> lnDerivsRhoT]"
             write(*,*) "lnPVectRhoT: ", lnPVect_mixRhoT
             write(*,*) "lnEVectRhoT: ", lnEVect_mixRhoT
             write(*,*) "lnSVectRhoT: ", lnSVect_mixRhoT
          endif
       
          call convlnderivs_RES(lnRho, lnT, &
               lnPvect_mixRhoT, lnEvect_mixRhoT, lnSvect_mixRhoT, &
               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output)
       
       !!! THIS IS A CHEAP TRICK - TODO - FIX
       !!! I am setting the vector variables that are forgotten by the mixing EOS with the H/He EOS values
       !!! Presumably this is not the right way to do it.
          
!       res_output(i_mu) = eos_res(i_mu)
!       d_dlnRho_c_T_output(i_mu) = eos_d_dlnRho_const_T(i_mu)
!       d_dlnT_c_Rho_output(i_mu) = eos_d_dlnT_const_Rho(i_mu)

!       res_output(i_lnfree_e) = eos_res(i_lnfree_e)
!       d_dlnRho_c_T_output(i_lnfree_e) = eos_d_dlnRho_const_T(i_lnfree_e)
!       d_dlnT_c_Rho_output(i_lnfree_e) = eos_d_dlnT_const_Rho(i_lnfree_e)

!       res_output(i_eta) = eos_res(i_eta)
!       d_dlnRho_c_T_output(i_eta) = eos_d_dlnRho_const_T(i_eta)
!       d_dlnT_c_Rho_output(i_eta) = eos_d_dlnT_const_Rho(i_eta)       

       !! IDEA - if we are desperate we could use everything except P from the origonal EOS or PES
       !!  - we can relax this one variable at a time to figure out what is messing things up
       
          res_output(4:num_eos_basic_results) = eos_res(4:num_eos_basic_results)
          d_dlnRho_c_T_output(4:num_eos_basic_results) = eos_d_dlnRho_const_T(4:num_eos_basic_results)
          d_dlnT_c_Rho_output(4:num_eos_basic_results) = eos_d_dlnT_const_Rho(4:num_eos_basic_results)
          

       else
          res_output(:) = 0d0
          d_dlnRho_c_T_output = 0d0
          d_dlnT_c_Rho_output = 0d0
       endif
       
       deallocate(Pvect, lnPvect, Evect, lnEvect, Svect, lnSvect)
    endif
  contains 
    subroutine print_res(res)
      use eos_lib, only: eos_result_names
      real(dp), intent(in) :: res(num_eos_basic_results)
      character (len=8) :: names(num_eos_basic_results)
      integer :: i
      call eos_result_names(names) 
      do i=1,num_eos_basic_results
         write(*,*) names(i), " = ", res(i)
      enddo
    end subroutine print_res
  end subroutine get_res_PT
  
  
  !! Root finding routine that calls get_res_PT
  !! lnP is the independent coordinate
  !! Generalize this routine so that it works for any eos value - 
  !! if the which_other = 0, then Val is lnRho
  
  real(dp) function PT_DVal(log10X,dValdlog10X,lrpar,rpar,lipar,ipar,ierr)
    use mixeos_def    
    use eos_def, only: num_eos_basic_results, i_lnPgas
    use chem_def, only: num_chem_isos
    use const_lib, only: ln10
    implicit none
    real(dp), intent(in) :: log10X
    real(dp), intent(out) :: dValdlog10X
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(in) :: lrpar, lipar
    integer, intent(out) :: ierr
    
    integer :: eos_handle
    real(dp) :: Z, X,abar,zbar
    integer :: species
    integer, pointer, dimension(:) :: chem_id, net_iso
    real(dp), pointer :: xa(:)
    
    real(dp) :: log10P, P, T, log10T, other_value, log10var2, var2
    real(dp), pointer :: Rho
    real(dp), pointer, dimension(:) :: &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output
    
    integer :: which_other = 0
    integer :: X_is_P
    integer :: i = 1 !! integer index 
    integer :: j = 1 !! double index
    
    ierr = 0

    !! Get variables from ipar {
    i = 1
    eos_handle = ipar(i) ; i = i+1
    which_other = ipar(i) ; i = i+1
    species = ipar(i) ; i = i+1
    X_is_P = ipar(i) ; i = i+1
    chem_id => ipar(i:(i+species-1)) ; i = i+species
    net_iso => ipar(i:(i+num_chem_isos-1)) ; i = i+num_chem_isos
    !! }

    !! Get input variables from rpar {
    j = 1
    Z = rpar(j) ; j = j+1
    X = rpar(j) ; j = j+1
    abar = rpar(j) ; j = j+1
    zbar = rpar(j) ; j = j+1
    var2 = rpar(j) ; j = j+1    
    log10var2 = log10(var2)
    other_value = rpar(j) ; j = j+1
    xa => rpar(j:(j+species-1)) ; j = j+species
    Rho => rpar(j) ; j = j+1
    res_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    d_dlnRho_c_T_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    d_dlnT_c_Rho_output => rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
    !! }       

    if(mixdbg_PT_inverter) then
       write(*,*) "[PT_DVal] which_other = ", which_other
       write(*,*) "[PT_DVal] X_is_P = ", X_is_P
    end if
    
    if(X_is_P .eq. 1) then
       P = 1d1**log10X
       log10P = log10X

       T = var2
       log10T = log10var2
    else
       T = 1d1**log10X
       log10T = log10X

       P = var2
       log10P = log10var2
    endif
    
    if(mixdbg_PT_inverter) then
       write(*,*) "[PT_DVAL] -> get_res_PT"
       write(*,*) "   P = ", P, " ; log10P = ", log10P
       write(*,*) "   T = ", T, " ; log10T = ", log10T
    endif
    
    call get_res_PT(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, T, log10T, Rho,  &
         res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)

!    write(*,*) "  ierr = ", ierr
!    write(*,*) "  res_output: ", res_output
    
    if(ierr .ne. 0) then
       dValdlog10X = 1.
       write(*,*) "[PT_DVAL]: Why is this happening?"
       if(ierr .eq. MIXEOSERR_MINPRESSURE) then
          PT_DVal = -1d0
       else
          PT_DVal = 1d0
       endif
    else
       if(X_is_P .eq. 1) then
          if(which_other .eq. 0) then
             dValdlog10X = 1d0 / d_dlnRho_c_T_output(i_lnPgas)
             PT_DVal = log10(Rho) - other_value
          else
             PT_DVal = res_output(which_other) - other_value
             dValdlog10X = ln10 * d_dlnRho_c_T_output(which_other) / d_dlnRho_c_T_output(i_lnPgas)
          endif
       else
          if(which_other .eq. 0) then
             PT_DVal = log10(Rho) - other_value
             dValdlog10X = - d_dlnT_c_Rho_output(i_lnPgas) / d_dlnRho_c_T_output(i_lnPgas)
          else
             PT_DVal = res_output(which_other) - other_value
             dValdlog10X = ln10 * ((-d_dlnT_c_Rho_output(i_lnPgas) / d_dlnRho_c_T_output(i_lnPgas)) * &
                  d_dlnRho_c_T_output(which_other) + d_dlnT_c_Rho_output(which_other))
          endif
       end if
    endif
  end function PT_DVal
  
  subroutine get_res(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       Rho, log10Rho, T, log10T, &
       res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    use chem_def
    use eos_def
    use eos_lib
    use mixeos_def
!    use thermo, only: convlnderivs_RES, convDerivslnDerivs
    use num_lib
    implicit none
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: Rho, log10Rho, T, log10T
    real(dp), intent(out) :: res(num_eos_basic_results)
    ! partial derivatives of the basic results wrt lnd and lnT
    ! d_dlnRho_c_T(i) = d(res(i))/dlnd|T
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results) 
    ! d_dlnT(i) = d(res(i))/dlnT|Rho
    real(dp),  intent(out) :: d_dlnT_c_Rho(num_eos_basic_results) 
    
    integer, intent(out) :: ierr
    
    real(dp) :: lnRho, lnT, dlnRhodlnP, P
    real(dp) ::  log10Pguess, log10P1, log10P2, other1, other2, other_value
    
    integer :: lrpar, lipar, which_other
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, log10P, tmp_Rho
    integer :: X_is_P
    
    real(dp) :: res_output(num_eos_basic_results), &
         d_dlnRho_c_T_output(num_eos_basic_results), &
         d_dlnT_c_Rho_output(num_eos_basic_results)
    

    !! Overall algorithm
    !! 1. Look up the pressure range 

    which_other = 0
    other_value = log10Rho
    
    if(.not. associated(net_iso)) then
       write(*,*) "[mixeos_mod/get_res] net_iso not associated"
    endif

    X_is_P = 0
    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         T, log10T, which_other, other_value, X_is_P, &
         log10P1, log10P2, other1, other2, ierr)


    
    if(ierr .ne. 0) then
!       write(*,*) "[get_res] PT_find_range failed with ierr = ", ierr, " logT = ", log10T
!       write(*,*) "          which_other = ", which_other
!       write(*,*) "          other_value = ", other_value
!       write(*,*) "          other range: ", other1, other2
       write(*,*) "[mixeos @ logRho=", log10Rho, ";logT=", log10T, "] out of safe bounds, using regular eos"

       ierr = 0
       call eosDT_get( &
              eos_handle, Z, X, abar, zbar, &
              species, chem_id, net_iso, xa, &
              Rho, log10Rho, T, log10T, &
              res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)

       !! Attempt to recover at this point by asking the eos for values outside the safe zone
       !!  
       !! This code is attempting to find a broader zone for the rootfind, but
       !! I should just call the normal eos at this point
!       if(other1 .gt. log10Rho) then
!          log10P1 = mixeos_minlog10P
!          P = 1d1**log10P1
!          call get_res_PT(eos_handle, Z, X, abar, zbar, &
!               species, chem_id, net_iso, xa, &
!               P, log10P1, T, log10T, tmp_Rho, &
!               res_output, d_dlnRho_c_T_output, d_dlnT_c_Rho_output, ierr)
!          if(tmp_Rho .lt. log10Rho) then
!             other1 = tmp_Rho
!             ierr = 0
!          endif
!       elseif(other2 .lt. log10Rho) then
!          log10P2 = mixeos_maxlog10P
!          P = 1d1**log10P2
!          call get_res_PT(eos_handle, Z, X, abar, zbar, &
!               species, chem_id, net_iso, xa, &
!               P, log10P2, T, log10T, tmp_Rho, &
!               res_output, d_dlnRho_C_T_output, d_dlnT_c_Rho_output, ierr)
!          if(tmp_Rho .gt. log10Rho) then
!             other2 = tmp_Rho
!             ierr = 0
!          end if
!       endif
    else
       if(mixdbg1) then 
          write(*,*) "[get_res] PT_find_range success for logT = ", log10T
          write(*,*) "          which_other = ", which_other
          write(*,*) "          other_value = ", other_value
          write(*,*) "          other range: ", other1, other2
          write(*,*) "         log10P range: ", log10P1, log10P2
       endif
!    endif

!    write(*,*) "[get_res] PT_find_range results: "
!    write(*,*) "  log10P1 = ", log10P1
!    write(*,*) "  log10P2 = ", log10P2
!    write(*,*) "  other1 = ", other1
!    write(*,*) "  other2 = ", other2
!    write(*,*) "  ierr = ", ierr
!    write(*,*)

    !! 2. call a safe root find procedure
       X_is_P = 1
          
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       
       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       
       lnRho = log(Rho)
       
       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = 0 ; i = i+1 !! Which_other = 0
       ipar(i) = species ; i = i+1
       ipar(i) = X_is_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
          
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = T ; j = j+1
       rpar(j) = log10Rho ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       
       log10PGuess = (log10P1 + log10P2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
       if(mixdbg1) then
          write(*,*) "[get_res]: Calling safe root find: ", log10PGuess, log10P1, log10P2, dVal1, dVal2
       endif
       log10P = safe_root_with_initial_guess(PT_DVal, log10PGuess, log10P1, log10P2, dVal1, dVal2, &
            mixeos_getRhoT_maxiter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       !! SOMETHING IS WRONG 
       !! SET OMP_NUM_THREADS = 1
       !! not sure if this is a mesa problem or gfortran
       !! The following line should not be necessary
       j = species + 7
          !! Obviously its a bad thing to have in there
          
       tmp_Rho = rpar(j) ; j = j+1
       res = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnRho_c_T = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnT_c_Rho = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results
       deallocate(rpar)
       deallocate(ipar)       
       
!       write(*,*) "[get_res] res: ", res
!       write(*,*) "  ierr = ", ierr
    endif



  end subroutine get_res

  subroutine get_Rho(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       log10T, which_other, other_value, &
       logRho_tol, other_tol, max_iter, logRho_guess,  &
       logRho_bnd1, logRho_bnd2, other_at_bnd1, other_at_bnd2, &
       logRho_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, &
       ierr)    
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: arg_not_provided, dp
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: log10T ! log10 of temperature

    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logRho_tol
    
    integer, intent(in) :: max_iter ! max number of Newton iterations        
    
    real(dp), intent(in) :: logRho_guess ! log10 of density
    real(dp), intent(in) :: logRho_bnd1, logRho_bnd2 ! bounds for logRho
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    real(dp) :: log10P1, log10P2, lnRho_bnd1, lnRho_bnd2
    
    real(dp), intent(out) :: logRho_result ! log10 of density

    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    real(dp) :: Rho, T

    real(dp) :: logPgas_tol , logPgas_result
!    real(dp) :: helm_res(num_helm_results)

    ierr = 0
    logPgas_tol = logRho_tol

    call PT_get_P(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         log10T, which_other, other_value, other_tol, logPgas_tol, &
         max_iter, logPgas_result, Rho, logRho_result, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    
  end subroutine get_Rho
  
  subroutine get_T(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       log10Rho, which_other, other_value, &
       logT_tol, other_tol, max_iter, logT_guess, & 
       logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
       logT_result, res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: arg_not_provided
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, intent(in), pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: log10Rho ! log10 of density
    integer, intent(in) :: which_other ! from eos_def.  e.g., i_lnE
    real(dp), intent(in) :: other_value ! desired value for the other variable
    real(dp), intent(in) :: other_tol
    
    real(dp), intent(in) :: logT_tol
    integer, intent(in) :: max_iter ! max number of iterations        
    
    real(dp), intent(in) :: logT_guess ! log10 of temperature
    real(dp), intent(in) :: logT_bnd1, logT_bnd2 ! bounds for logT
    ! if don't know bounds, just set to arg_not_provided (defined in const_def)
    real(dp), intent(in) :: other_at_bnd1, other_at_bnd2 ! values at bounds
    ! if don't know these values, just set to arg_not_provided (defined in const_def)
    
    real(dp), intent(out) :: logT_result ! log10 of temperature
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    
    integer, intent(out) :: eos_calls
    integer, intent(out) :: ierr ! 0 means AOK.
    
    logical, parameter :: doing_Rho = .false.
    
    real(dp) :: res1(num_eos_basic_results), d_dlnRho_c_T1(num_eos_basic_results), d_dlnT_c_Rho1(num_eos_basic_results)
    real(dp) :: res2(num_eos_basic_results), d_dlnRho_c_T2(num_eos_basic_results), d_dlnT_c_Rho2(num_eos_basic_results)
    real(dp) :: Rho, T, log10T, local_other_at_bnd1, local_other_at_bnd2, local_logT_bnd1, local_logT_bnd2

    ierr = 0

    !! Determine values at boundaries other_value (logTlow, logThigh)
    !! 
    !! Run do_safe_get_Rho_T

    if(logT_bnd1 .eq. arg_not_provided) then
!       local_logT_bnd1 = accessor%minlog10T
    else
       local_logT_bnd1 = logT_bnd1
    endif

    if(logT_bnd2 .eq. arg_not_provided) then
!       local_logT_bnd2 = accessor%maxlog10T
    else
       local_logT_bnd2 = logT_bnd2
    endif
    
    if(other_at_bnd1.eq. arg_not_provided) then
       Rho = 10d0**log10Rho
       T = 10d0**local_logT_bnd1
       log10T = local_logT_bnd1
       call get_res(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res1, d_dlnRho_c_T1, d_dlnT_c_Rho1, ierr)
       local_other_at_bnd1 = res1(which_other)
    else
       local_other_at_bnd1 = other_at_bnd1
    endif
    
    if(other_at_bnd2 .eq. arg_not_provided) then
       Rho = 10d0**log10Rho
       T = 10d0**local_logT_bnd2
       log10T = local_logT_bnd2
       call get_res(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            Rho, log10Rho, T, log10T, &
            res2, d_dlnRho_c_T2, d_dlnT_c_Rho2, ierr)
       local_other_at_bnd2 = res2(which_other)
    else
       local_other_at_bnd2 = other_at_bnd2
    endif
    
    if(((local_other_at_bnd1.lt.other_value).and.(local_other_at_bnd2.gt.other_value)).or. &
         ((local_other_at_bnd1.gt.other_value).and.(local_other_at_bnd2.lt.other_value))) then
    
!       logT_guess = (local_logT_bnd1 + local_logT_bnd2) / 2.d0
    
       call do_safe_get_Rho_T(eos_handle, Z, X, abar, zbar, &
            species, chem_id, net_iso, xa, &
            log10Rho, &
            which_other, other_value, doing_Rho, &
            logT_guess, logT_result, &
            local_logT_bnd1, local_logT_bnd2, local_other_at_bnd1, local_other_at_bnd2, &
            logT_tol, other_tol, max_iter, &
            res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    else
       write(*,*) "target other_value: ", other_value
       write(*,*) "other1: ", local_other_at_bnd1
       write(*,*) "other2: ", local_other_at_bnd2
       ierr = MIXEOSERR_OUTOFBOUND
    endif
  end subroutine get_T
  
  !! This routine is called by get_Rho and get_T
  subroutine do_safe_get_Rho_T( &
       eos_handle, Zcomp, Xcomp, abar, zbar, &
       species, chem_id, net_iso, xa, &
       the_other_log, & 
       which_other, other_value, doing_Rho, & 
       initial_guess, x, xbnd1, xbnd2, other_at_bnd1, other_at_bnd2, & 
       xacc, yacc, ntry, & 
       res, d_dlnRho_c_T, d_dlnT_c_Rho, eos_calls, ierr)
    use chem_def
    use eos_def
    use num_lib
    use const_def
    use mixeos_def
    implicit none

    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Zcomp, Xcomp, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    integer, intent(in) :: which_other
    real(dp), intent(in) :: other_value
    logical, intent(in) :: doing_Rho
    real(dp), intent(in) :: initial_guess ! for x
    real(dp), intent(out) :: x ! if doing_Rho, then logRho, else logT
    real(dp), intent(in) :: the_other_log
    real(dp), intent(in) :: xbnd1, xbnd2, other_at_bnd1, other_at_bnd2
    real(dp), intent(in) :: xacc, yacc ! tolerances
    integer, intent(in) :: ntry ! max number of iterations        
    real(dp), intent(out) :: res(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnRho_c_T(num_eos_basic_results)
    real(dp), intent(out) :: d_dlnT_c_Rho(num_eos_basic_results)
    integer, intent(out) :: eos_calls, ierr
    
    integer :: i, j, lrpar, lipar
    real(dp), parameter :: dx = 0.1d0
    integer, pointer :: ipar(:)
    real(dp), pointer :: rpar(:)
    real(dp) :: rho, T, xb1, xb3, y1, y3, dfdx, f, logRho, logT

    ierr = 0

    eos_calls = 0
    x = initial_guess
    
    if (doing_Rho) then
       rho = arg_not_provided
       T = 10**the_other_log
    else
       T = arg_not_provided
       rho = 10**the_other_log
    end if
    
    lipar = n_iparams + species + num_chem_isos
    lrpar = n_rparams + species + num_eos_basic_results*3
    allocate(ipar(lipar),rpar(lrpar))
    if(doing_Rho) then
       ipar(i_rho_flag) = 1
    else
       ipar(i_rho_flag) = 0
    endif
    ipar(i_which_other) = which_other
    ipar(i_handle) = eos_handle
    ipar(i_species) = species
    ipar(i_eos_calls) = eos_calls   !! set to zero above
    i = n_iparams + 1
    ipar(i:(i+species-1)) = chem_id(1:species) ; i = i + species
    ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos


    rpar(i_other) = other_value
    rpar(i_fixed_log_input) = the_other_log
    rpar(i_Z) = Zcomp
    rpar(i_X) = Xcomp
    rpar(i_abar) = abar
    rpar(i_zbar) = zbar
    i = n_rparams + 1
    rpar(i:(i+species-1)) = xa

    xb1 = xbnd1; xb3 = xbnd2
    y1 = other_at_bnd1 - other_value
    y3 = other_at_bnd2 - other_value
    
    x = safe_root_with_initial_guess(  &
         get_f_df, initial_guess, xb1, xb3, y1, y3, ntry,  &
         xacc, yacc, lrpar, rpar, lipar, ipar, ierr)
    if (ierr /= 0) then
       write(*, *) 'do_safe_get_Rho_T: safe_root returned ierr', ierr
!       write(*, *) trim(alert_message)
       return
    end if
    
    i = n_rparams + species
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    eos_calls = ipar(i_eos_calls)
    
    deallocate(ipar, rpar)

  end subroutine do_safe_get_Rho_T

  !! This is the function that is passed to safe_root_with_initial_guess
  real(dp) function get_f_df(x, dfdx, lrpar, rpar, lipar, ipar, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_def, only: ln10
    implicit none

    integer, intent(in) :: lrpar, lipar
    real(dp), intent(in) :: x
    real(dp), intent(out) :: dfdx
    real(dp), intent(inout), pointer :: rpar(:)
    integer, intent(inout), pointer :: ipar(:)
    integer, intent(out) :: ierr    

    logical :: doing_Rho
    integer :: which_other, eos_handle, i, species
    real(dp) :: Zcomp, Xcomp, abar, zbar
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer :: xa(:)
    real(dp), pointer, dimension(:) :: res, d_dlnRho_c_T, d_dlnT_c_Rho
    real(dp) :: Rho, T, log10Rho, log10T, other, fixed_log_input
    
    ierr = 0
    
    doing_Rho = (ipar(i_rho_flag) /= 0)
    which_other = ipar(i_which_other)
    eos_handle = ipar(i_handle)
    species = ipar(i_species)
    i = n_iparams + 1
    chem_id => ipar(i:(i+species-1)) ; i = i+species
    net_iso => ipar(i:(i+num_chem_isos-1)) ; i = i+num_chem_isos
    
    i = 0
    other = rpar(i_other) 
    fixed_log_input = rpar(i_fixed_log_input)
    Zcomp = rpar(i_Z)
    Xcomp = rpar(i_X)
    abar = rpar(i_abar)
    zbar = rpar(i_zbar)
    
    
    if(doing_Rho) then
       log10Rho = x
       Rho = 1d1**log10Rho
       log10T = fixed_log_input
       T = 1d1**log10T !arg_not_provided
    else
       log10T = x
       T = 1d1**log10T
       log10Rho = fixed_log_input
       Rho = 1d1**log10Rho !arg_not_provided
    endif
    
    call get_res( &
         eos_handle, Zcomp, Xcomp, abar, zbar, &
         species, chem_id, net_iso, xa, &
         Rho, log10Rho, T, log10T, &
         res, d_dlnRho_c_T, d_dlnT_c_Rho, ierr)
    if (ierr /= 0) then
       return
    end if
    ipar(i_eos_calls) = ipar(i_eos_calls)+1
    
    i = n_rparams
    res = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnRho_c_T = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results
    d_dlnT_c_Rho = rpar(i+1:i+num_eos_basic_results); i = i+num_eos_basic_results



    get_f_df = res(which_other) - other
    
    if (doing_Rho) then
       dfdx = d_dlnRho_c_T(which_other)*ln10
    else
       dfdx = d_dlnT_c_Rho(which_other)*ln10
    end if
    
  end function get_f_df
  
  !! This function takes (Temperature, and an other quantity and tries to find Pressure)  
  subroutine PT_get_P(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logT, which_other, other_value, other_tol, logPgas_tol, &
       max_iter, & !logPgas_guess, logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2, &
       logPgas_result, Rho, log10Rho, res, d_dlnRho_const_T, d_dlnT_const_Rho, & !helm_res, &
       eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_lib, only: dp, arg_not_provided
    use num_lib
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: logT, other_value, other_tol, logPgas_tol ! , &
!         logPgas_guess, logPgas_bnd1, logPgas_bnd2, other_at_bnd1, other_at_bnd2
    integer, intent(in) :: which_other, max_iter
    
    real(dp), intent(out) :: logPgas_result, Rho, log10Rho
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_const_T(num_eos_basic_results), &
         d_dlnT_const_Rho(num_eos_basic_results)
!    real(dp), intent(out) :: helm_res(num_helm_results)
    integer, intent(out) :: eos_calls, ierr
    
    real(dp) :: T, log10T, log10P, lnP1, lnP2, log10P1, log10P2, other1, other2
    
    integer :: lrpar, lipar
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, log10PGuess
    integer :: X_is_P
    
    !! Overall algorithm
    !! 1. Look up the pressure range 
    T = 1d1**logT
    log10T = logT
    X_is_P = 0
    
    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         T, log10T, which_other, other_value, X_is_P, &
         log10P1, log10P2, other1, other2, ierr)
    
    !! 2. call a safe root find procedure
    if(ierr .eq. 0) then !! Only proceed if there is no error condition

       X_is_P = 1
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       

       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       

       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = which_other ; i = i+1
       ipar(i) = species ; i = i+1
       ipar(i) = X_is_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
       
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = T ; j = j+1
       rpar(j) = other_value ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       

       log10PGuess = (log10P1 + log10P2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
!       write(*,*) "PT_get_P: Calling safe root find: ", log10PGuess, log10P1, log10P2, dVal1, dVal2
       log10P = safe_root_with_initial_guess(PT_DVal, log10PGuess, log10P1, log10P2, dVal1, dVal2, &
            mixeos_getRhoT_maxiter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       Rho = rpar(j) ; j = j+1
       log10Rho = log10(Rho)
       res = rpar(j:(j+num_eos_basic_results-1))   ;   j = j+num_eos_basic_results
       d_dlnRho_const_T = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnT_const_Rho = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results

       deallocate(rpar)
       deallocate(ipar)       
    endif
    
  end subroutine PT_get_P


  !! There may be a more optimal way of solving this problem
  !!  The method I have in mind here is to use 
  subroutine PT_get_T(eos_handle, Z, X, abar, zbar, &
       species, chem_id, net_iso, xa, &
       logP, which_other, other_value, other_tol, logT_tol, &
       max_iter, & ! logT_guess, logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2, &
       logT_result, Rho, log10Rho, res, d_dlnRho_const_T, d_dlnT_const_Rho, & !helm_res, &
       eos_calls, ierr)
    use chem_def
    use eos_def
    use mixeos_def
    use const_lib, only: dp, arg_not_provided
    use num_lib
    
    integer, intent(in) :: eos_handle
    real(dp), intent(in) :: Z, X, abar, zbar
    integer, intent(in) :: species
    integer, pointer :: chem_id(:), net_iso(:)
    real(dp), pointer, intent(in) :: xa(:)
    real(dp), intent(in) :: logP, other_value, other_tol, logT_tol ! , &
                  ! logT_guess, logT_bnd1, logT_bnd2, other_at_bnd1, other_at_bnd2
    integer, intent(in) :: which_other, max_iter
    
    real(dp), intent(out) :: logT_result, Rho, log10Rho
    real(dp), intent(out) :: res(num_eos_basic_results), &
         d_dlnRho_const_T(num_eos_basic_results), &
         d_dlnT_const_Rho(num_eos_basic_results)
!    real(dp), intent(out) :: helm_res(num_helm_results)
    integer, intent(out) :: eos_calls, ierr

    integer :: X_is_P 

    integer :: lrpar, lipar
    integer, pointer, dimension(:) :: ipar
    real(dp), pointer, dimension(:) :: rpar
    integer :: i=1, j=1
    real(dp) :: dVal1, dVal2, lnRho
    real(dp) :: P, log10P, log10Tguess, log10T, log10T1, log10T2, other1, other2

    P = 1d1**logP
    log10P = logP
    X_is_P = 1
    
    call PT_find_range(eos_handle, Z, X, abar, zbar, &
         species, chem_id, net_iso, xa, &
         P, log10P, which_other, other_value, X_is_P, &
         log10T1, log10T2, other1, other2, ierr)
    
    
    !! 2. call a safe root find procedure
    if(ierr .eq. 0) then !! Only proceed if there is no error condition
       
       X_is_P = 0
       lrpar = 7 + species + 3*num_eos_basic_results
       lipar = 4 + species + num_chem_isos
       
       allocate(rpar(lrpar))
       allocate(ipar(lipar))
       
       lnRho = log(Rho)

       ipar(:)= 0
       i = 1
       ipar(i) = eos_handle ; i = i+1
       ipar(i) = which_other ; i = i+1
       ipar(i) = species ; i = i+1
       ipar(i) = X_is_P ; i = i+1
       ipar(i:(i+species-1)) = chem_id(1:species) ; i = i+species
       ipar(i:(i+num_chem_isos-1)) = net_iso(1:num_chem_isos) ; i = i + num_chem_isos
       
       rpar(:) = 0d0
       j = 1
       rpar(j) = Z ; j = j+1
       rpar(j) = X ; j = j+1
       rpar(j) = abar ; j = j+1
       rpar(j) = zbar ; j = j+1
       rpar(j) = P ; j = j+1
       rpar(j) = other_value ; j = j+1
       rpar(j:(j+species-1)) = xa ; j = j+species
       
       
       log10TGuess = (log10T1 + log10T2) / 2d0 
       dVal1 = other1 - other_value
       dVal2 = other2 - other_value
       
       log10T = safe_root_with_initial_guess(PT_DVal, log10TGuess, log10T1, log10T2, dVal1, dVal2, &
            max_iter, mixeos_epsilon, mixeos_epsilon, &
            lrpar, rpar, lipar, ipar, ierr)
       
       logT_result = log10T
       Rho = rpar(j) ; j = j+1
       log10Rho = log10(Rho)
       res = rpar(j:(j+num_eos_basic_results-1)) ; j = j+num_eos_basic_results
       d_dlnRho_const_T = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results
       d_dlnT_const_Rho = rpar(j:(j+num_eos_basic_results-1)) ;  j = j+num_eos_basic_results
    endif

  end subroutine PT_get_T

end module mixeos_mod
