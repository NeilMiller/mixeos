# plot the origonal PT diagram

# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :log10Rho
  attr_accessor :log10E
  attr_accessor :log10S
  attr_accessor :grad_ad
  attr_accessor :d_dlnRho_lnPgas
  attr_accessor :d_dlnT_lnPgas
  attr_accessor :d_dlnRho_lnE
  attr_accessor :d_dlnT_lnE
  attr_accessor :d_dlnRho_lnS
  attr_accessor :d_dlnT_lnS

  attr_accessor :dlnEdlnRho
  attr_accessor :dlnEdlnT
  attr_accessor :d2lnEdlnRho2
  attr_accessor :d2lnEdlnT2
  attr_accessor :d2lnEdlnRhodlnT

  attr_accessor :dlnSdlnRho
  attr_accessor :dlnSdlnT
  attr_accessor :d2lnSdlnRho2
  attr_accessor :d2lnSdlnT2
  attr_accessor :d2lnSdlnRhodlnT

  def initialize(data_dir)
        
    read_image_Xs(data_dir, 'log10Pvect.data', 'log10(P)')
    read_image_Ys(data_dir, 'log10Tvect.data', 'log10(T)')
    
    @log10Rho = read_image_data(data_dir, 'log10Rhomat')
    @log10E = read_image_data(data_dir, 'log10Emat')
    @log10S = read_image_data(data_dir, 'log10Smat')
    @grad_ad = read_image_data(data_dir, 'gradad_mat')
#    @d_dlnRho_lnPgas = read_image_data(data_dir, 'PT_d_dlnRho_lnPgas')
#    @d_dlnT_lnPgas = read_image_data(data_dir, 'PT_d_dlnT_lnPgas')
#    @d_dlnRho_lnE = read_image_data(data_dir, 'PT_d_dlnRho_lnE')
#    @d_dlnT_lnE = read_image_data(data_dir, 'PT_d_dlnT_lnE')
#    @d_dlnRho_lnS = read_image_data(data_dir, 'PT_d_dlnRho_lnS')
#    @d_dlnT_lnS = read_image_data(data_dir, 'PT_d_dlnT_lnS')

#    @dlnEdlnRho = read_image_data(data_dir, 'PT_dlnEdlnRho')
#    @dlnEdlnT = read_image_data(data_dir, 'PT_dlnEdlnT')
#    @d2lnEdlnRho2 = read_image_data(data_dir, 'PT_d2lnEdlnRho2')
#    @d2lnEdlnT2 = read_image_data(data_dir, 'PT_d2lnEdlnT2')
#    @d2lnEdlnRhodlnT = read_image_data(data_dir, 'PT_d2lnEdlnRhodlnT')

#    @dlnSdlnRho = read_image_data(data_dir, 'PT_dlnSdlnRho')
#    @dlnSdlnT = read_image_data(data_dir, 'PT_dlnSdlnT')
#    @d2lnSdlnRho2 = read_image_data(data_dir, 'PT_d2lnSdlnRho2')
#    @d2lnSdlnT2 = read_image_data(data_dir, 'PT_d2lnSdlnT2')
#    @d2lnSdlnRhodlnT = read_image_data(data_dir, 'PT_d2lnSdlnRhodlnT')

  end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
    
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'Fig05_PTorig/'
    
    
    
    t.def_figure('log10Rho') { log10Rho }
    t.def_figure('log10E') { log10E }
    t.def_figure('log10S') { log10S }
#    t.def_figure('grad_ad') { grad_ad }
    #      t.def_figure('d_dlnRho_lnPgas') { d_dlnRho_lnPgas }
#      t.def_figure('d_dlnT_lnPgas') { d_dlnT_lnPgas }
#      t.def_figure('d_dlnRho_lnE') { d_dlnRho_lnE }
#      t.def_figure('d_dlnT_lnE') { d_dlnT_lnE }
#      t.def_figure('d_dlnRho_lnS') { d_dlnRho_lnS }
#      t.def_figure('d_dlnT_lnS') {d_dlnT_lnS }

    t.def_figure('dlnEdlnRho') { dlnEdlnRho }
    t.def_figure('dlnEdlnT') { dlnEdlnT }
    t.def_figure('d2lnEdlnRho2') { d2lnEdlnRho2 }
    t.def_figure('d2lnEdlnT2') { d2lnEdlnT2 }
    t.def_figure('d2lnEdlnRhodlnT') { d2lnEdlnRhodlnT }

    t.def_figure('dlnSdlnRho') { dlnSdlnRho }
    t.def_figure('dlnSdlnT') { dlnSdlnT }
    t.def_figure('d2lnSdlnRho2') { d2lnSdlnRho2 }
    t.def_figure('d2lnSdlnT2') { d2lnSdlnT2 }
    t.def_figure('d2lnSdlnRhodlnT') { d2lnSdlnRhodlnT }

    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05

    @minlogP = 8.0
    @maxlogP = 16.0
    @minlogT = 2.0
    @maxlogT = 6.0
    

#      @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
#      @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
#      @LowMasslog10TProfile1 = @LowMassProfile1[1]

#      @LowMassProfile2 = Dvector.read("planet_data/RhoTset02.dat")
#      @LowMasslog10RhoProfile2 = @LowMassProfile2[0]
#      @LowMasslog10TProfile2 = @LowMassProfile2[1]

#      @LowMassProfile3 = Dvector.read("planet_data/RhoTset03.dat")
#      @LowMasslog10RhoProfile3 = @LowMassProfile3[0]
#      @LowMasslog10TProfile3 = @LowMassProfile3[1]

#      @HD80606Profile = Dvector.read("planet_data/RhoTset04.dat")
#      @HD80606log10RhoProfile = @HD80606Profile[0]
#      @HD80606log10TProfile = @HD80606Profile[1]
 
    @HD80606Profile = Dvector.read("planet_data/logPT_hd80606.dat")
    @HD80606log10PProfile = @HD80606Profile[0]
    @HD80606log10TProfile = @HD80606Profile[1]
    
    @JUPPROFILE = Dvector.read("planet_data/Jup_Ad.dat")
    @JUPlog10PProfile = @JUPPROFILE[2]
    @JUPlog10TProfile = @JUPPROFILE[3]
    @JUPlog10RhoProfile = @JUPPROFILE[4]
    
    @SATPROFILE = Dvector.read("planet_data/Sat.dat")
    @SATlog10PProfile = @SATPROFILE[2]
    @SATlog10TProfile = @SATPROFILE[3]
    @SATlog10RhoProfile = @SATPROFILE[4]

    @IDEOSBND = Dvector.read("eoslogPrange.dat")
    @IDEOSBND_logT    = @IDEOSBND[0]
    @IDEOSBND_logPmin = @IDEOSBND[1]
    @IDEOSBND_logPmax = @IDEOSBND[2]
    
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
    
  end
  
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
  
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
  
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
  
  # plot routines
  
  def log10Rho
    image_plot('d' => d, 'zs' => d.log10Rho, 'title' => 'log10 Rho ', 'interpolate' => false,
               'z_lower' => -2, 'z_upper' => 2, 'interpolate' => false)
    
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
        t.show_polyline(@IDEOSBND_logPmax, @IDEOSBND_logT, Black)
        t.show_polyline(@IDEOSBND_logPmin, @IDEOSBND_logT, Black)
#          t.show_polyline(@LowMasslog10RhoProfile2, @LowMasslog10TProfile2, Red)
#          t.show_polyline(@LowMasslog10RhoProfile3, @LowMasslog10TProfile3, Green)
#          t.show_polyline(@HD80606log10RhoProfile, @HD80606log10TProfile, Black)
        }
      end
    end

  def log10E
    image_plot('d' => d, 'zs' => d.log10E, 'title' => 'log10E ', 'interpolate' => false,
               'z_lower' => 10, 'z_upper' => 14, 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end

  def log10S
    image_plot('d' => d, 'zs' => d.log10S, 'title' => 'log10S ', 'interpolate' => false,
               'z_lower' => 8.4, 'z_upper' => 8.8, 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end  

  def grad_ad
    image_plot('d' => d, 'zs' => d.grad_ad, 'title' => 'grad_ad ', 'interpolate' => false,
               'z_lower' => 0, 'z_upper' => 5)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end  

  def d_dlnRho_lnPgas
    image_plot('d' => d, 'zs' => d.d_dlnRho_lnPgas, 'title' => 'dlnPgas/dlnRho ', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end  

  def d_dlnT_lnPgas
    image_plot('d' => d, 'zs' => d.d_dlnT_lnPgas, 'title' => 'dlnPgas/dlnT ', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d_dlnRho_lnE
    image_plot('d' => d, 'zs' => d.d_dlnRho_lnE, 'title' => 'dlnE/dlnRho ', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d_dlnT_lnE
    image_plot('d' => d, 'zs' => d.d_dlnT_lnE, 'title' => 'dlnE/dlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d_dlnRho_lnS
    image_plot('d' => d, 'zs' => d.d_dlnRho_lnS, 'title' => 'dlnS/dlnRho ', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d_dlnT_lnS
    image_plot('d' => d, 'zs' => d.d_dlnT_lnS, 'title' => 'dlnS/dlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  #lnE


  def dlnEdlnRho
    image_plot('d' => d, 'zs' => d.dlnEdlnRho, 'title' => 'dlnE/dlnRho', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def dlnEdlnT
    image_plot('d' => d, 'zs' => d.dlnEdlnT, 'title' => 'dlnE/dlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnEdlnRho2
    image_plot('d' => d, 'zs' => d.d2lnEdlnRho2, 'title' => 'd2lnE/dlnRho2', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnEdlnT2
    image_plot('d' => d, 'zs' => d.d2lnEdlnT2, 'title' => 'd2lnE/dlnT2', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnEdlnRhodlnT
    image_plot('d' => d, 'zs' => d.d2lnEdlnRhodlnT, 'title' => 'd2lnE/dlnRhodlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 


  #lnS


  def dlnSdlnRho
    image_plot('d' => d, 'zs' => d.dlnSdlnRho, 'title' => 'dlnS/dlnRho', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def dlnSdlnT
    image_plot('d' => d, 'zs' => d.dlnSdlnT, 'title' => 'dlnS/dlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnSdlnRho2
    image_plot('d' => d, 'zs' => d.d2lnSdlnRho2, 'title' => 'd2lnS/dlnRho2', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnSdlnT2
    image_plot('d' => d, 'zs' => d.d2lnSdlnT2, 'title' => 'd2lnS/dlnT2', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def d2lnSdlnRhodlnT
    image_plot('d' => d, 'zs' => d.d2lnSdlnRhodlnT, 'title' => 'd2lnS/dlnRhodlnT', 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [@minlogP,@maxlogP,@maxlogT,@minlogT]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@HD80606log10PProfile, @HD80606log10TProfile, Blue)
        t.show_polyline(@JUPlog10PProfile, @JUPlog10TProfile, Purple)
        t.show_polyline(@SATlog10PProfile, @SATlog10TProfile, Yellow)
      }
    end
  end 

  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end

  def pressure
    image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
                 'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
      # We would like to overplot some profile here.
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do
      @bounds = [-4,2.5,2,10]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        #        clip_press_image
        t.line_width=2.5
        t.show_polyline(@LowMasslog10RhoProfile, @LowMasslog10TProfile, Blue)
#      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
        #                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
      }
    end 
  end

end


MY_plot.new('plot.data/test_basic_eos')
