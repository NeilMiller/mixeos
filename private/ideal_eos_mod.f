!! Author : Neil Miller
!!
!! Date   : September, 2012
!! 
!! Class  : ideal_eos
!!  
!!   Object oriented Ideal Gas equation of state
!!
!!   This is an analytic equation of state - the ideal gas law.
!!   the purpose of this implementation is to compare it to the
!!   h5table implementation.
!!   

module ideal_eos_mod
  use base_eos_mod
  use const_def, only: dp, ln10
  implicit none
  
  public :: ideal_eos, construct_ideal_eos
  
  real(dp), private, parameter :: N_A = 6.d23
  real(dp), private, parameter :: k_pc   = 1.38d-16
  real(dp), private, parameter :: h_pc = 6.6e-27 
!  real(dp), private, parameter :: h_pc = 1.05457e-27
  real(dp), private, parameter :: ig_minlog10T = 0.99d0
  real(dp), private, parameter :: ig_maxlog10T = 20d0
  real(dp), private, parameter :: ig_log10Tbnd = 0.3
  
  real(dp), private, parameter :: ig_minlog10P = -15d0
  real(dp), private, parameter :: ig_maxlog10P = 27d0
  real(dp), private, parameter :: ig_EPSILON = 1d-2
  
  type, extends(base_eos) :: ideal_eos
     private
     real(dp) :: mu, lnalpha, m, alpha, lnN_A_mu
   contains
     procedure :: Construct => construct_ideal_eos

     procedure :: DT_get => DT_get_ideal
     procedure :: PT_get => PT_get_ideal
     procedure, private :: DT_getP => DT_getP_ideal
     procedure, private :: PT_getD => PT_getD_ideal
     procedure :: PT_get_PRhorange => PT_get_PRhorange_ideal
     procedure :: PT_get_Trange => PT_get_Trange_ideal
  end type ideal_eos
  
contains
  subroutine DT_getP_ideal(this, Rho, T, P)
    class(ideal_eos)      :: this
    real(dp), intent(in)  :: Rho, T
    real(dp), intent(out) :: P
    
    P = (k_pc * T) * Rho * (N_A / this%mu)
  end subroutine DT_getP_ideal
  
  subroutine PT_getD_ideal(this, P, T, Rho)
    class(ideal_eos)      :: this
    real(dp), intent(in)  :: P, T
    real(dp), intent(out) :: Rho
    
    Rho = (P * this%mu / N_A) / (k_pc * T)
  end subroutine PT_getD_ideal
  
  subroutine PT_get_ideal(this, P, T, Rho, Pvect, Evect, Svect, weight, ierr)
    class(ideal_eos), target :: this
    real(dp), intent(in) :: P, T
    real(dp), intent(out) :: Rho, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    Rho = (P * this%mu / N_A) / (k_pc * T)
    if(Rho .lt. 0) then 
       write(*,*) "P = ", P
       write(*,*) "mu = ", this%mu
       write(*,*) "N_A = ", N_A
       write(*,*) "k_pc = ", k_pc
       write(*,*) "T = ", T
    endif
    call this%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
  end subroutine PT_get_ideal
  
  subroutine DT_get_ideal(this, Rho, T, Pvect, Evect, Svect, weight, ierr)
    class(ideal_eos) :: this
    real(dp), intent(in) :: Rho, T
    real(dp), intent(out) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    real(dp) :: f_val, df_drho, df_dt, &
         d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    real(dp) :: lnRho, lnT, logT, rho2, rho3, T2
    
    ierr = 0
    weight = 1d0
    !! Logs are calculated just once
    lnRho = log(Rho)
    lnT = log(T)
    logT = log10(T)
    
    if((logT .lt. ig_minlog10T) .or. (logT .gt. ig_maxlog10T)) then
       ierr = -9
       weight = 0d0
       return
    elseif(logT .lt. (ig_minlog10T + ig_log10Tbnd)) then
       weight = (logT - ig_minlog10T) / ig_log10Tbnd
    elseif(logT .gt. (ig_maxlog10T - ig_log10Tbnd)) then
       weight = (ig_maxlog10T - logT) / ig_log10Tbnd
    else
       weight = 1d0
    endif
    
    !! Powers are calculated just once
    rho2 = rho*rho
    rho3 = rho2 * rho    
    T2 = T*T
    
    !! Get free energy derivatives of the ideal EOS
    f_val = -(N_A / this%mu) * k_pc * T * ( - lnRho - this%lnN_A_mu - this%lnalpha + 1.5 * lnT + 1.)
    df_drho =  (N_A / this%mu) * k_pc * T / rho
    df_dt = -(N_A / this%mu) * k_pc * ( - lnRho - this%lnN_A_mu - this%lnalpha + 1.5 * lnT + 2.5)
    d2f_drho2 = -(N_A / this%mu) * k_pc * T / rho2
    d2f_dt2 =  -(N_A / this%mu) * 1.5 * k_pc  / T
    d2f_drhodt =  (N_A / this%mu) * k_pc / rho
    d3f_drho3 = (N_A / this%mu) * 2.d0 * k_pc * T / rho3
    d3f_drho2dt = -(N_A / this%mu) * k_pc / rho2
    d3f_drhodt2 = 0.d0
    d3f_dt3 = (N_A / this%mu) * 1.5 * k_pc / T2
    d4f_drho2dt2 = 0.d0  

    !! Convert the Free Energy Derivatives into P, E, S & derivatives
    Pvect(i_val) = Rho2 * df_drho
    Pvect(i_drho) = Rho2 * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = Rho2 * d2f_drhodt
    Pvect(i_drho2) = Rho2 * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = Rho2 * d3f_drhodt2
    Pvect(i_drhodt) = Rho2 * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_drhodt
    Svect(i_dt) = -d2f_dt2
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2

    if(Svect(i_val) .lt. 0d0) then
       weight = 0d0
       ierr = -10
    elseif(log10(Svect(i_val)) .lt. 1d0) then
       weight = Svect(i_val) / 1d1
       ierr = 0d0
    end if
    
  end subroutine DT_get_ideal

  subroutine PT_get_PRhorange_ideal(this, log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
    class(ideal_eos) :: this
    real(dp), intent(in) :: log10T
    real(dp), intent(out) :: log10P1, log10P2, log10Rho1, log10Rho2
    integer, intent(out) :: ierr
    
    real(dp) :: maxlogRho, Rho, T, P, tight_maxlogP, weight
    real(dp) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs)
    
    ierr = 0
!    write(*,*) "[ideal_eos: PT_get_PRhorange_ideal] log10T = ", log10T
    if((log10T .lt. ig_maxlog10T) .and. (log10T .gt. ig_minlog10T)) then
       
       log10P1 = ig_minlog10P
       P = 1d1**log10P1
       T = 1d1**log10T
       call this%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
       log10Rho1 = log10(Rho)
!       write(*,*) "log10T = ", log10T
!       write(*,*) "log10P1 = ", log10P1
!       write(*,*) "log10Rho1 = ", log10Rho1
       
       maxlogRho = (2.5 - this%lnN_A_mu - this%lnalpha + 1.5 * log10T * ln10) / ln10 - ig_EPSILON
       log10Rho2 = maxlogRho
       Rho = 1d1**maxlogRho
       call this%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
       P = Pvect(i_val)
       tight_maxlogP = log10(P)
       log10P2 = min(tight_maxlogP, ig_maxlog10P)
       if(log10P1 .gt. log10P2) then
          ierr = ERR_RANGE
       endif
    else
       log10P1 = 0d0
       log10P2 = 0d0
       log10Rho1 = 0d0
       log10Rho2 = 0d0
       ierr = ERR_RANGE
    end if
  end subroutine PT_get_PRhorange_ideal
  
  subroutine PT_get_Trange_ideal(this, log10P, log10T1, log10T2, ierr)
    class(ideal_eos) :: this
    real(dp), intent(in) :: log10P
    real(dp), intent(out) :: log10T1, log10T2
    integer, intent(out) :: ierr
    
    ierr = 0
    if((log10P .lt. ig_maxlog10P) .and. (log10P .gt. ig_minlog10P)) then
       
       log10T1 = max(ig_minlog10T, ((log10P*ln10 - 2.5 - log(k_pc) + this%lnalpha) / 2.5 / ln10)) + ig_EPSILON
       log10T2 = ig_maxlog10T
       
       if(log10T1 .gt. log10T2) then
          ierr = ERR_RANGE
       endif
    else
       log10T1 = ig_maxlog10T
       log10T2 = ig_maxlog10T
       ierr = ERR_RANGE
    end if
  end subroutine PT_get_Trange_ideal
  
  !! Should the constructor create a pointer?
  !! will this table disappear if the initial function disappears?
  !! Perhaps we could get around that too
  subroutine construct_ideal_eos(this, mu)
    class(ideal_eos) :: this
    real(dp) :: mu
    
    this%mu = mu
    this%m = mu / N_A
    this%alpha = (h_pc / (2 * 3.14 * this%m * k_pc)**0.5)**3.d0
    this%lnalpha = log(this%alpha)
    this%lnN_A_mu = log(N_A / this%mu)
  end subroutine construct_ideal_eos
end module ideal_eos_mod
