!! Author : Neil Miller
!!
!! Date   : September, 2012
!! 
!! Class  : P,E,S eos 
!!  
!!   Object oriented Ideal Gas equation of state
!!
!!   This is an analytic equation of state - the ideal gas law.
!!   the purpose of this implementation is to compare it to the
!!   h5table implementation.
!!

module pes_eos_mod
  use base_eos_mod
  use data_2dtable
  use const_def, only: dp, ln10
  implicit none
  
  public :: ideal_eos, construct_ideal_eos
  
  real(dp), private, parameter :: N_A = 6.d23
  real(dp), private, parameter :: k_pc   = 1.38d-16
  real(dp), private, parameter :: h_pc = 6.6e-27 
  
  
  type, extends(base_eos) :: pes_eos
     private
     
     type(data_2dtbl) :: PES_table
   contains
     procedure :: Construct => construct_pes_eos
     procedure :: Destroy => destroy_pes_eos

     procedure :: DT_get => DT_get_PES
     procedure :: PT_get => PT_get_PES
     procedure, private :: DT_getP => DT_getP_PES
     procedure, private :: PT_getD => PT_getD_PES
     procedure :: PT_get_PRhorange => PT_get_PRhorange_PES
     procedure :: PT_get_Trange => PT_get_Trange_PES
  end type pes_eos
  
contains
  subroutine DT_getP_PES(this, Rho, T, P)
    class(pes_eos)      :: this
    real(dp), intent(in)  :: Rho, T
    real(dp), intent(out) :: P
    
    P = (k_pc * T) * Rho * (N_A / this%mu)
  end subroutine DT_getP_PES
  
  subroutine PT_getD_PES(this, P, T, Rho)
    class(pes_eos)      :: this
    real(dp), intent(in)  :: P, T
    real(dp), intent(out) :: Rho
    
    Rho = (P * this%mu / N_A) / (k_pc * T)
  end subroutine PT_getD_PES
  
  subroutine PT_get_PES(this, P, T, Rho, Pvect, Evect, Svect, weight, ierr)
    class(pes_eos), target :: this
    real(dp), intent(in) :: P, T
    real(dp), intent(out) :: Rho, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    Rho = (P * this%mu / N_A) / (k_pc * T)
    if(Rho .lt. 0) then 
       write(*,*) "P = ", P
       write(*,*) "mu = ", this%mu
       write(*,*) "N_A = ", N_A
       write(*,*) "k_pc = ", k_pc
       write(*,*) "T = ", T
    endif
    call this%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
  end subroutine PT_get_PES
  
  subroutine DT_get_PES(this, Rho, T, Pvect, Evect, Svect, weight, ierr)
    class(pes_eos) :: this
    real(dp), intent(in) :: Rho, T
    real(dp), intent(out) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    real(dp) :: f_val, df_drho, df_dt, &
         d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    real(dp) :: lnRho, lnT, logT, rho2, rho3, T2
    
    ierr = 0
    weight = 1d0
    !! Logs are calculated just once
    lnRho = log(Rho)
    lnT = log(T)
    logT = log10(T)
    
    if((logT .lt. ig_minlog10T) .or. (logT .gt. ig_maxlog10T)) then
       ierr = -9
       weight = 0d0
       return
    elseif(logT .lt. (ig_minlog10T + ig_log10Tbnd)) then
       weight = (logT - ig_minlog10T) / ig_log10Tbnd
    elseif(logT .gt. (ig_maxlog10T - ig_log10Tbnd)) then
       weight = (ig_maxlog10T - logT) / ig_log10Tbnd
    else
       weight = 1d0
    endif
    
    !! Powers are calculated just once
    rho2 = rho*rho
    rho3 = rho2 * rho    
    T2 = T*T
    
    !! Get free energy derivatives of the ideal EOS
    f_val = -(N_A / this%mu) * k_pc * T * ( - lnRho - this%lnN_A_mu - this%lnalpha + 1.5 * lnT + 1.)
    df_drho =  (N_A / this%mu) * k_pc * T / rho
    df_dt = -(N_A / this%mu) * k_pc * ( - lnRho - this%lnN_A_mu - this%lnalpha + 1.5 * lnT + 2.5)
    d2f_drho2 = -(N_A / this%mu) * k_pc * T / rho2
    d2f_dt2 =  -(N_A / this%mu) * 1.5 * k_pc  / T
    d2f_drhodt =  (N_A / this%mu) * k_pc / rho
    d3f_drho3 = (N_A / this%mu) * 2. * k_pc * T / rho3
    d3f_drho2dt = -(N_A / this%mu) * k_pc / rho2
    d3f_drhodt2 = 0.
    d3f_dt3 = (N_A / this%mu) * 1.5 * k_pc / T2
    d4f_drho2dt2 = 0.    

    !! Convert the Free Energy Derivatives into P, E, S & derivatives
    Pvect(i_val) = Rho2 * df_drho
    Pvect(i_drho) = Rho2 * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = Rho2 * d2f_drhodt
    Pvect(i_drho2) = Rho2 * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = Rho2 * d3f_drhodt2
    Pvect(i_drhodt) = Rho2 * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_dt2
    Svect(i_dt) = -d2f_drhodt
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2

    if(Svect(i_val) .lt. 0d0) then
       weight = 0d0
       ierr = -10
    elseif(log10(Svect(i_val)) .lt. 1d0) then
       weight = Svect(i_val) / 1d1
       ierr = 0d0
    end if
    
  end subroutine DT_get_PES

  subroutine PT_get_PRhorange_PES(this, log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
    class(pes_eos) :: this
    real(dp), intent(in) :: log10T
    real(dp), intent(out) :: log10P1, log10P2, log10Rho1, log10Rho2
    integer, intent(out) :: ierr
    
    real(dp) :: maxlogRho, Rho, T, P, tight_maxlogP, weight
    real(dp) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs)

    ierr = 0
    if((log10T .lt. ig_maxlog10T) .and. (log10T .gt. ig_minlog10T)) then
    
       log10P1 = ig_minlog10P
       P = 1d1**log10P1
       T = 1d1**log10T
       call this%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
       log10Rho1 = log10(Rho)
       
       maxlogRho = (2.5 - this%lnN_A_mu - this%lnalpha + 1.5 * log10T * ln10) / ln10 - ig_EPSILON
       log10Rho2 = maxlogRho
       Rho = 1d1**maxlogRho
       call this%DT_get(Rho, T, Pvect, Evect, Svect, weight, ierr)
       P = Pvect(i_val)
       tight_maxlogP = log10(P)
       log10P2 = min(tight_maxlogP, ig_maxlog10P)
       if(log10P1 .gt. log10P2) then
          ierr = ERR_RANGE
       endif
    else
       ierr = ERR_RANGE
    end if
  end subroutine PT_get_PRhorange_PES
  
  subroutine PT_get_Trange_PES(this, log10P, log10T1, log10T2, ierr)
    class(pes_eos) :: this
    real(dp), intent(in) :: log10P
    real(dp), intent(out) :: log10T1, log10T2
    integer, intent(out) :: ierr
    
    ierr = 0
    if((log10P .lt. ig_maxlog10P) .and. (log10P .gt. ig_minlog10P)) then
  
       log10T1 = max(ig_minlog10T, ((log10P*ln10 - 2.5 - log(k_pc) + this%lnalpha) / 2.5 / ln10)) + ig_EPSILON
       log10T2 = ig_maxlog10T
       
       if(log10T1 .gt. log10T2) then
          ierr = ERR_RANGE
       endif
    else
       log10T1 = ig_maxlog10T
       log10T2 = ig_maxlog10T
       ierr = ERR_RANGE
    end if
  end subroutine PT_get_Trange_PES
  
  
  subroutine construct_pes_eos(this, filename, Xsize, Ysize)
    class(pes_eos) :: this

    integer, intent(in) :: Xsize, Ysize
    integer :: NSurf = 3
    character(len=256), intent(in) :: filename
    character(len=256) :: name, Xname, Yname
    
    name = "PES_table"
    Xname = "lnRho"
    Yname = "lnT"
    
    call this%PES_table%Construct(Xsize, Ysize, NSurf, &
         filename, name, Xname, Yname, ierr)
  end subroutine construct_pes_eos
  
  subroutine destroy_pes_eos(this)
    class(pes_eos) :: this
    
    call this%PES_table%Destroy
    
  end subroutine destroy_pes_eos

end module pes_eos_mod
