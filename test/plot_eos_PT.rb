# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :logRho
  attr_accessor :logE
  attr_accessor :logS

  def initialize(data_dir)
        
      read_image_Xs(data_dir, 'eos.logP_x.data', 'log10(P)')
      read_image_Ys(data_dir, 'eos.logT_y.data', 'log10(T)')
      
      @logRho = read_image_data(data_dir, 'eos.logRho_xy')
      @logE   = read_image_data(data_dir, 'eos.logE_xy')
      @logS   = read_image_data(data_dir, 'eos.logS_xy')
    
    end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
    
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'aneos_water'
    
    t.def_figure('logRho') { logRho }
    t.def_figure('logE')   { logE }
    t.def_figure('logS')   { logS }
     
    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05
    
#    @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
#    @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
#    @LowMasslog10TProfile1 = @LowMassProfile1[1]
    
    
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
    
  end
  
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
    
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
    
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
    
  # plot routines
  
  def logRho
    image_plot('d' => d, 'zs' => d.logRho, 'title' => 'logRho ', 'interpolate' => false, 
               'z_lower' => -5, 'z_upper' => 10, 'interpolate' => false)
  end

  def logE
    image_plot('d' => d, 'zs' => d.logE, 'title' => 'logE ', 'interpolate' => false,
               'z_lower' => 8.5, 'z_upper' => 13.0, 'interpolate' => false)
  end

  def logS
    image_plot('d' => d, 'zs' => d.logS, 'title' => 'logS ', 'interpolate' => false,
               'z_lower' => 6.0, 'z_upper' => 9.0, 'interpolate' => false)
  end  
  
  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end
  
end


MY_plot.new('ideal_eos_data')
