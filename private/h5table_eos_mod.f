! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton, Neil Miller
!
!   This file is part of MESA.
!
!   MESA is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Library Public License as published
!   by the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   MESA is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU Library General Public License for more details.
!
!   You should have received a copy of the GNU Library General Public License
!   along with this software; if not, write to the Free Software
!   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
!
! ***********************************************************************
!!
!! Author : Neil Miller
!! 
!! Date   : September 2012
!!
!! Object oriented version - to replace the old version
!! h5table module
!!  This is the helmholtz free energy tabular / interpolation routines
!!   for a specific substance such as water.  The EOS should be generated
!!   from other software such as ANEOS and put into a format that can
!!   be read here.  

module h5table_eos_mod
  use base_eos_mod
  use mixeos_def  !! Parameters, global to mixeos 
  use const_def, only: dp
  use interp1dfunction_mod
  
  implicit none
  
  !! Parameters only for the h5table
  logical, private, parameter :: h5dbg = .false.
  
  !! Error conditions should be public (do we need all of these?)
  integer, parameter :: H5TBLERR_FILE = -64
  integer, parameter :: H5TBLERR_OUTBOUND = -65
  integer, parameter :: H5TBLERR_BADIND = -66
  integer, parameter :: H5TBLERR_REGFULL = -67
  integer, parameter :: H5TBLERR_BADASSOC = -68
  
  integer, parameter :: H5TBLERR_MAXRHO = -69
  integer, parameter :: H5TBLERR_MINRHO = -70
  integer, parameter :: H5TBLERR_MAXPRESSURE = MIXEOSERR_MAXPRESSURE
  integer, parameter :: H5TBLERR_MINPRESSURE = MIXEOSERR_MINPRESSURE
  integer, parameter :: H5TBLERR_MAXTEMPERATURE = MIXEOSERR_MAXTEMPERATURE
  integer, parameter :: H5TBLERR_MINTEMPERATURE = MIXEOSERR_MINTEMPERATURE
  integer, parameter :: H5TBLERR_NOPRESSURESOL = -75
  
  integer, parameter :: H5TBLERR_NDERMIX = -80
  
!!! Indexes for Biquintic interpolation
  integer, private, parameter :: H5TBL_NDERIV = 9
  integer, private, parameter :: TBL_VAL = 1
  integer, private, parameter :: TBL_DX = 2
  integer, private, parameter :: TBL_DY = 3
  integer, private, parameter :: TBL_DX2 = 4
  integer, private, parameter :: TBL_DY2 = 5
  integer, private, parameter :: TBL_DXDY = 6
  integer, private, parameter :: TBL_DX2DY = 7
  integer, private, parameter :: TBL_DXDY2 = 8
  integer, private, parameter :: TBL_DX2DY2 = 9
  
!!! Parameters for a rootfind: Max iterations and Epsilon
  integer, private, parameter :: H5TBL_ITERMAX = 100
  double precision, private, parameter :: H5TBL_EPSILON = 1d-8
  
  !! How many points do we want to have for the Pressure & Temperature constraint?
  integer, private, parameter :: H5NP_BND = 30 
!  integer, private, parameter :: H5NT_BND = 30
  double precision, parameter :: H5TBL_BNDBUFFER = 0.1
  
  type, extends(base_eos) :: h5table_eos
     
     character(len=256), private :: datafilename
     integer, private :: imax, jmax
     real(dp), private :: minlnRho, maxlnRho, minlnT, maxlnT, dlnRho, dlnT, &
          minlog10Rho, maxlog10Rho, minlog10T, maxlog10T
     
     real(dp), dimension(:), pointer, private :: Rho_v, T_v, lnRho_v, lnT_v, &
          dRho_v, dT_v, dRho2_v, dT2_v, dRho_i, dRho2_i, dRho3_i, dT_i, dT2_i, dT3_i
     
     real(dp), dimension(:,:,:), pointer, private :: Fij  !!  (9, imax, jmax) 
     
     real(dp), private :: minlogRhoAlpha = 0., minlogRhoBeta = -5., &
          maxlogRhoAlpha = 0., maxlogRhoBeta = 5.
     real(dp), private :: lg_border = 0.2
     
     !! Boundary relationships
     !! Potentially we could imagine a possible case when the user would pass one of these
     !! objects to the h5table_eos, which would allow the relation to be more arbitrary
     type(interp1dfunction) :: minlogP_by_logT, maxlogP_by_logT, &
                               minlogRho_by_logT, maxlogRho_by_logT
     
     logical, private :: IsSetup = .false. !! Has the table's members been allocated?
     
   contains
     !! Memory Management & Setup 
     procedure :: Construct => construct_h5tbl_eos
     procedure :: Destroy => free_h5table
     procedure :: slice_h5table 
     
     !! Required procedures for actually using the class
     procedure :: DT_get => safe_h5_DT_get
     procedure :: PT_get => safe_h5_PT_get !! Use the default one provided by base_eos_mod
     procedure :: PT_get_PRhorange => PT_get_PRhorange_h5tbl
     procedure :: PT_get_Trange    => PT_get_Trange_h5tbl  
     
     !! Private procedures
     procedure, private :: h5_DT_get
  end type h5table_eos
  
contains
  
  !! setup_h5tbl performs two functions
  !!  (1) allocate members of tbl pointer
  !!  (2) load data from filename
  subroutine construct_h5tbl_eos(this, Xsize, Ysize, filename, name, ierr)
    use const_def, only: ln10, dp
    implicit none
    
    class (h5table_eos) :: this  !! This should already be allocated
    character (len=256), intent(in) :: filename, name
    integer, intent(in) :: Xsize, Ysize
    integer, intent(out) :: ierr ! 0 means OK
    
    integer :: ios, fid, i, j
    real(dp) :: Rhoval, Tval, lnRhoval, lnTval
    real(dp), parameter :: grid_epsilon = 1e-5
    real(dp) :: FvectIn(H5TBL_NDERIV)
    
    
    write(*,*) "Setting up table ", name

    ierr = 0
    
    this%imax = Xsize
    this%jmax = Ysize
    this%datafilename = filename
    call this%set_name(name)

    write(*,*) "Allocating table internal memory"
    call alloc_1d_array(this%Rho_v, Xsize)
    call alloc_1d_array(this%lnRho_v, Xsize)
    call alloc_1d_array(this%dRho_v, Xsize)
    call alloc_1d_array(this%dRho2_v, Xsize)
    call alloc_1d_array(this%dRho_i, Xsize)
    call alloc_1d_array(this%dRho2_i, Xsize)
    call alloc_1d_array(this%dRho3_i, Xsize)
    call alloc_1d_array(this%T_v, Ysize)      
    call alloc_1d_array(this%lnT_v, Ysize)
    call alloc_1d_array(this%dT_v, Ysize)
    call alloc_1d_array(this%dT2_v, Ysize)
    call alloc_1d_array(this%dT_i, Ysize)
    call alloc_1d_array(this%dT2_i, Ysize)
    call alloc_1d_array(this%dT3_i, Ysize)
    allocate(this%Fij(H5TBL_NDERIV, Xsize, Ysize), stat=ierr)
    if(ierr /= 0) then
       write(*,*) 'failure to allocate memory for storing table'
    end if

    ios = 0
    ierr = 0
    fid = 20

    write(*,*) "Opening file - ", trim(filename)
    open(unit=fid, file=trim(filename), action='read', &
         status ='old', iostat=ios, form="unformatted")
    
    !! The format of the filename MUST be
    !!  Rho T Fij, dFdrho, dFdT, d2FdX2, d2FdY2, d2FdXdy, d3FdX2dY, d3FdXdY2, d4FdX2dY2.  
    !!  There should also be Xsize * Ysize lines to the file
    do i=1,Xsize
       do j=1,Ysize
          read(fid) lnRhoVal, lnTval, FvectIn
          this%Fij(1:H5TBL_NDERIV,i,j) = FvectIn
          
          RhoVal = exp(lnRhoVal)
          Tval = exp(lnTVal)
          if(isnan(RhoVal) .or. &
               isnan(Tval) .or. &
               isnan(this%Fij(TBL_VAL,i,j)) .or. &
               isnan(this%Fij(TBL_DX,i,j)) .or. &
               isnan(this%Fij(TBL_DY,i,j)) .or. &
               isnan(this%Fij(TBL_DX2,i,j)) .or. &
               isnan(this%Fij(TBL_DY2,i,j)) .or. &
               isnan(this%Fij(TBL_DXDY,i,j)) .or. &
               isnan(this%Fij(TBL_DX2DY,i,j)) .or. &
               isnan(this%Fij(TBL_DXDY2,i,j)) .or. &
               isnan(this%Fij(TBL_DX2DY2,i,j))) then
             write(*,*) "NaN found - this is unnacceptable"  
             write(*,*) "Make sure that the table has NO NaN values"
             write(*,*) "  in it before you pass it to the h5table"
             stop
          endif

          this%Rho_v(i) = RhoVal
          this%T_v(j) = Tval
          this%lnRho_v(i) = lnRhoVal
          this%lnT_v(j) = lnTval
       enddo
    enddo

    close(unit=fid)

    this%minlnRho = minval(this%lnRho_v)
    this%maxlnRho = maxval(this%lnRho_v)
    this%minlnT = minval(this%lnT_v)
    this%maxlnT = maxval(this%lnT_v)
    this%minlog10Rho = this%minlnRho / ln10
    this%maxlog10Rho = this%maxlnRho / ln10
    this%minlog10T = this%minlnT / ln10
    this%maxlog10T = this%maxlnT / ln10
    this%dlnRho = (this%maxlnRho - this%minlnRho) / (Xsize - 1)
    this%dlnT = (this%maxlnT - this%minlnT) / (Ysize - 1)      

    write(*,*) "H5 table spans log10Rho : ", this%minlnRho / ln10, this%maxlnRho / ln10
    write(*,*) "               log10T   : ", this%minlnT / ln10, this%maxlnT / ln10
    
!!! Set up the difference vectors and their inverses
    do i=1,Xsize-1
       this%dRho_v(i) = this%Rho_v(i+1) - this%Rho_v(i)
       this%dRho2_v(i) = this%dRho_v(i) * this%dRho_v(i)
       this%dRho_i(i) = 1d0 / this%dRho_v(i)
       this%dRho2_i(i) = this%dRho_i(i) * this%dRho_i(i)
       this%dRho3_i(i) = this%dRho_i(i) * this%dRho2_i(i)
    enddo
    do j=1,Ysize-1
       this%dT_v(j) = this%T_v(j+1) - this%T_v(j)
       this%dT2_v(j) = this%dT_v(j) * this%dT_v(j)
       this%dT_i(j) = 1d0 / this%dT_v(j)
       this%dT2_i(j) = this%dT_i(j) * this%dT_i(j)
       this%dT3_i(j) = this%dT_i(j) * this%dT2_i(j)
    enddo

 !! Check spacing of Rho_v to make sure that it is logrithmically
 !! spaced with seperation dlnRho.  Note a common reason these might be mispaced
 !! would be if the user provides an inccorect Xsize or Ysize
    do i=2,Xsize  
       if(abs(log(this%Rho_v(i) / this%Rho_v(i-1)) - this%dlnRho) .gt. grid_epsilon) then
          write(*,*) "There is something wrong with the grid or the dimensions of the table"
          write(*,*) "i : ", i
          write(*,*) "Rho_v(i) : ", this%Rho_v(i)
          write(*,*) "Rho_v(i-1) : ", this%Rho_v(i-1)
          write(*,*) "dlnRho : ", this%dlnRho
          stop
       endif
    enddo
    
 !! Check spacing of T_v that it is logrithmically spaced with seperation dlnT
    do j=2,Ysize
       if(abs(log(this%T_v(j) / this%T_v(j-1)) - this%dlnT) .gt. grid_epsilon) then 
          write(*,*) " There is something wrong with the grid or the dimensions of the table"
          write(*,*) "j : ", j
          write(*,*) "T_v(j) : ", this%T_v(j)
          write(*,*) "T_v(j-1) : ", this%T_v(j-1)
          write(*,*) "dlnT : ", this%dlnT
          stop
       endif
    enddo

    !! Default cuts to the table
    write(*,*) "Slicing table with default parameters"
    this%IsSetup = .true.
    call this%slice_h5table(this%minlogRhoAlpha, this%minlogRhoBeta, &
         this%maxlogRhoAlpha, this%maxlogRhoBeta)

    write(*,*) "Construct_h5tbl_eos DONE"
    
  contains      
    subroutine alloc_1d_array(ptr,sz)
      real(dp), dimension(:), pointer :: ptr
      integer, intent(in) :: sz        
      allocate(ptr(sz),stat=ierr)
      if (ierr /= 0) then 
         write(*,*) 'failure in attempt to allocate Aneos_Table storage'
      endif
    end subroutine alloc_1d_array

  end subroutine construct_h5tbl_eos

  
  !! This routine allows you to make deeper cuts as necessary.  You aren't required
  !! to call this because the constructor does
  subroutine slice_h5table(this, minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)
    implicit none

    class (h5table_eos) :: this
    real(dp), intent(in) :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta
    
    real(dp) :: dlogT, Rho, T
    real(dp) :: log10T
    integer :: ierr
    
    real(dp), target :: minlogPvect(H5NP_BND), maxlogPvect(H5NP_BND), &
         minlogRhovect(H5NP_BND), maxlogRhovect(H5NP_BND), &
         log10Tvect(H5NP_BND)
    real(dp), pointer, dimension(:) :: logPvect_ptr, logTvect_ptr, logRhovect_ptr
    real(dp) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs)
    real(dp) :: safe_minlog10T, safe_maxlog10T
    integer :: i
    
    this%minlogRhoAlpha = minlogRhoAlpha
    this%minlogRhoBeta = minlogRhoBeta
    this%maxlogRhoAlpha = maxlogRhoAlpha
    this%maxlogRhoBeta = maxlogRhoBeta
    
    !! Clean up Memory, then we can use it again
!    call this%minlogP_by_logT%Destroy
!    call this%maxlogP_by_logT%Destroy
!    call this%minlogRho_by_logT%Destroy
!    call this%maxlogRho_by_logT%Destroy

    safe_minlog10T = this%minlog10T + H5TBL_BNDBUFFER
    safe_maxlog10T = this%maxlog10T - H5TBL_BNDBUFFER
    dlogT = (safe_maxlog10T - safe_minlog10T) / H5NP_BND
    do i=1,H5NP_BND           
       log10T = safe_minlog10T + dlogT * (i-1)
       log10Tvect(i) = log10T
       T = 1d1**log10T
       
       minlogRhovect(i) = max(this%minlog10Rho, minlogRhoAlpha * log10T + minlogRhoBeta)
       maxlogRhovect(i) = min(this%maxlog10Rho, maxlogRhoAlpha * log10T + maxlogRhoBeta)
       
       Rho = 1d1**(minlogRhovect(i) + H5TBL_BNDBUFFER)
       call this%h5_DT_get(Rho, T, Pvect, Evect, Svect, ierr)
       if(ierr .eq. 0) then
          minlogPvect(i) = log10(Pvect(i_val))
       else
          write(*,*) "h5table returned an error!! Fix it"
          stop
       end if
       
       Rho = 1d1**(maxlogRhovect(i) - H5TBL_BNDBUFFER)
       call this%h5_DT_get(Rho, T, Pvect, Evect, Svect, ierr)
       if(ierr .eq. 0) then
          maxlogPvect(i) = log10(Pvect(i_val))
       else
          write(*,*) "h5table returned an error!! Fix it"
          stop
       end if
       
    enddo
    

    logTvect_ptr => log10Tvect
    
    logPvect_ptr => minlogPvect
    logRhovect_ptr => minlogRhovect
    call this%minlogP_by_logT%Construct(logTvect_ptr, logPvect_ptr, H5NP_BND, ierr)
    call this%minlogRho_by_logT%Construct(logTvect_ptr, logRhovect_ptr, H5NP_BND, ierr)
    
    logPvect_ptr => maxlogPvect
    logRhovect_ptr => maxlogRhovect
    call this%maxlogP_by_logT%Construct(logTvect_ptr, logPvect_ptr, H5NP_BND, ierr)
    call this%maxlogRho_by_logT%Construct(logTvect_ptr, logRhovect_ptr, H5NP_BND, ierr)
    
  end subroutine slice_h5table

  !! free_h5table
  !! This subroutine deallocates the pointers that were allocated by the constructor
  subroutine free_h5table(this)
    implicit none
    class (h5table_eos) :: this

    call free_1D(this%Rho_v)
    call free_1D(this%lnRho_v)
    call free_1D(this%dRho_v)
    call free_1D(this%dRho2_v)
    call free_1D(this%dRho_i)
    call free_1D(this%dRho2_i)
    call free_1D(this%dRho3_i)
    
    call free_1D(this%T_v)
    call free_1D(this%lnT_v)
    call free_1D(this%dT_v)
    call free_1D(this%dT2_v)
    call free_1D(this%dT_i)      
    call free_1D(this%dT2_i)
    call free_1D(this%dT3_i)
    if(associated(this%Fij)) then
       deallocate(this%Fij)
       nullify(this%Fij)
    endif
    
    call this%minlogP_by_logT%Destroy()
    call this%maxlogP_by_logT%Destroy()
    call this%minlogRho_by_logT%Destroy()
    call this%maxlogRho_by_logT%Destroy()
    
  contains
    subroutine free_1D(vector)
      real(dp), pointer, dimension(:) :: vector
      if(associated(vector)) then 
         deallocate(vector)
         nullify(vector)
      endif
    end subroutine free_1D
  end subroutine free_h5table
  
  
  subroutine PT_get_PRhorange_h5tbl(this, log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)
    implicit none
    
    class (h5table_eos) :: this
    real(dp), intent(in) :: log10T
    real(dp), intent(out) :: log10P1, log10P2, log10Rho1, log10Rho2
    integer, intent(out) :: ierr

    if((log10T .lt. this%maxlog10T) .and. (log10T .gt. this%minlog10T)) then
       call this%minlogP_by_logT%interp(log10T, log10P1, ierr)
       if(ierr .ne. 0) then
          write(*,*) "minlogP_by_logT - Unexpected interpolation failure!"
          stop
       end if
       
       call this%minlogRho_by_logT%interp(log10T, log10Rho1, ierr)
       if(ierr .ne. 0) then
          write(*,*) "minlogRho_by_logT - Unexpected interpolation failure!"
          stop
       end if

       call this%maxlogP_by_logT%interp(log10T, log10P2, ierr)
       if(ierr .ne. 0) then
          write(*,*) "maxlogP_by_logT - Unexpected interpolation failure!"
          stop
       end if

       call this%maxlogRho_by_logT%interp(log10T, log10Rho2, ierr)
       if(ierr .ne. 0) then
          write(*,*) "maxlogRho_by_logT - Unexpected interpolation failure!"
          stop
       end if
    endif

  end subroutine PT_get_PRhorange_h5tbl
  
  !! Note this algorithm is making an assumption that the edges are monotonic in Pressure
  !! Depending on how you setup your cuts, this could potentially not be the case
  subroutine PT_get_Trange_h5tbl(this, log10P, log10T1, log10T2, ierr)
    class(h5table_eos) :: this
    real(dp), intent(in) :: log10P
    real(dp), intent(out) :: log10T1, log10T2
    integer, intent(out) :: ierr
    
    real(dp) :: log10P_lb, log10P_rb, log10P_lt, log10P_rt    
    real(dp) :: log10Tvect(4), log10Ttmp
    integer :: log10Tcount = 0
    
    call this%minlogP_by_logT%get_range(log10P_lb, log10P_lt, ierr)
    call this%maxlogP_by_logT%get_Range(log10P_rb, log10P_rt, ierr)

    if((log10P .gt. log10P_lb) .and. (log10P .lt. log10P_rb)) then
       log10Tcount = log10Tcount + 1
       log10Tvect(log10Tcount) = this%minlog10T
    endif
    
    if((log10P .lt. log10P_lt) .and. (log10P .gt. log10P_lb)) then
       log10Tcount = log10Tcount + 1
       call this%minlogP_by_logT%inv(log10P, log10Ttmp, ierr)
       log10Tvect(log10Tcount) = log10Ttmp
    endif

    if((log10P .gt. log10P_lt) .and. (log10P .lt. log10P_rt)) then
       log10Tcount = log10Tcount + 1
       log10Tvect(log10Tcount) = this%maxlog10T
    endif

    if((log10P .lt. log10P_rt) .and. (log10P .gt. log10P_rb)) then
       log10Tcount = log10Tcount + 1
       call this%maxlogP_by_logT%inv(log10P, log10Ttmp, ierr)
       log10Tvect(log10Tcount) = log10Ttmp
    endif

    if(log10Tcount .ge. 2) then
       log10T1 = minval(log10Tvect(1:log10Tcount)) + H5TBL_BNDBUFFER
       log10T2 = maxval(log10Tvect(1:log10Tcount)) - H5TBL_BNDBUFFER
       ierr = 0
    else
       if(log10P .lt. log10P_rt) then
          ierr = H5TBLERR_MINPRESSURE
       else
          ierr = H5TBLERR_MAXPRESSURE
       endif
    endif
    
  end subroutine PT_get_Trange_h5tbl
  
  !! The safe_h5_DT_get checks to see if the Rho,T is within the range 
  !!  then it calls h5_DT_get.  Otherwise it will return with a nonzero
  !!  ierr and weight=0
  subroutine safe_h5_DT_get(this, Rho, T, Pvect, Evect, Svect, weight, ierr)
    implicit none
    
    class (h5table_eos) :: this
    real(dp), intent(in) :: Rho, T
    real(dp), intent(out) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr
    
    real(dp) :: log10P1, log10P2, log10Rho1, log10Rho2, log10T, log10Rho

    log10Rho = log10(Rho)
    log10T = log10(T)
    
    Pvect(:) = 1d-100
    Evect(:) = 1d-100
    Svect(:) = 1d-100

    write(*,*) "safe_h5_DT_get: Rho = ", Rho, "; T = ", T
    
    ierr = 0
    weight = 1d0
    if(log10T .gt. (this%maxlog10T - H5TBL_BNDBUFFER)) then
       weight = 0d0
       ierr = H5TBLERR_MAXTEMPERATURE
    elseif(log10T .gt. (this%maxlog10T - this%lg_border)) then
       weight = (this%maxlog10T - log10T) / (this%lg_border - H5TBL_BNDBUFFER)
       ierr = 0
    elseif(log10T .lt. (this%minlog10T + H5TBL_BNDBUFFER)) then
       weight = 0d0
       ierr = H5TBLERR_MINTEMPERATURE
    elseif(log10T .lt. (this%minlog10T + this%lg_border)) then
       weight = (log10T - this%minlog10T) / (this%lg_border - H5TBL_BNDBUFFER)
       ierr = 0
    endif
    
    if(ierr .ne. 0) return
    !! These functions already have buffer in place already
    call this%PT_get_PRhorange(log10T, log10P1, log10P2, log10Rho1, log10Rho2, ierr)

    if(log10Rho .lt. log10Rho1) then
       weight = 0d0
       ierr = H5TBLERR_MINRHO
    elseif(log10Rho .lt. (log10Rho1 + this%lg_border)) then
       weight = weight * (log10Rho - log10Rho1) / this%lg_border 
       ierr = 0
    elseif(log10Rho .gt. log10Rho2) then
       weight = 0d0
       ierr = H5TBLERR_MAXRHO
    elseif(log10Rho .gt. (log10Rho2 - this%lg_border)) then
       weight = weight * (log10Rho2 - log10Rho) / this%lg_border
       ierr = 0
    end if

    if(ierr .ne. 0) then 
       call this%h5_DT_get(Rho, T, Pvect, Evect, Svect, ierr)
    endif
  end subroutine safe_h5_DT_get

!! DT_get : takes the (Rho, T) cordinates
!!   (1) lookup the (Rho_index, T_index) using the logrithmic hash
!!   (2) Use the Timmes biquintic iterpolation code - inline here

!! IMPORTANT : we are assuming that the user (private method)
!!   is actually using this within a region where it will return
!!   a value.  It is the job of the safe_h5_DT_get routine
!!   to make sure that it isn't really called outside of the
!!   valid range
  subroutine h5_DT_get(this, Rho, T, Pvect, Evect, Svect, ierr)
    implicit none
    
    class (h5table_eos), intent(in) :: this
    real(dp), intent(in) :: Rho, T
    real(dp), intent(out) :: Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs)
    integer, intent(out) :: ierr
    
    integer :: dI, tI
    real(dp) :: lnRho, lnT, RhoSQ, log10Rho, log10T
    
    !! Define Variables for helm Holtz interpolation routine
    real(dp) :: fi(36) 
    real(dp) :: z
    real(dp) :: xt, xd, mxt, mxd
    
    real(dp) :: si0t, si1t, si2t, si0mt, si1mt, si2mt, &
         si0d, si1d, si2d, si0md, si1md, si2md, &
         dsi0t, dsi1t, dsi2t, dsi0mt, dsi1mt, dsi2mt, &
         dsi0d, dsi1d, dsi2d, dsi0md, dsi1md, dsi2md, &
         ddsi0t, ddsi1t, ddsi2t, ddsi0mt, ddsi1mt, ddsi2mt, &
         ddsi0d, ddsi1d, ddsi2d, ddsi0md, ddsi1md, ddsi2md, &
         dddsi0t, dddsi1t, dddsi2t, dddsi0mt, dddsi1mt, dddsi2mt, &
         dddsi0d, dddsi1d, dddsi2d, dddsi0md, dddsi1md, dddsi2md

    real(dp) :: free, df_d, df_t, df_dd, df_tt, df_dt, &
         df_ddd, df_ddt, df_dtt, df_ttt

 !! Define the types for the statement functions below
    real(dp) psi0, dpsi0, ddpsi0, dddpsi0,  &
         psi1, dpsi1, ddpsi1, dddpsi1, &
         psi2, dpsi2, ddpsi2, dddpsi2!, &

    !! The following statement functions are used in helm_interp.dek      
    !..quintic hermite polynomial statement functions
    !..psi0 and its derivatives
    psi0(z)    = z**3 * ( z * (-6.0d0*z + 15.0d0) - 10.0d0) + 1.0d0
    dpsi0(z)   = z**2 * ( z * (-30.0d0*z + 60.0d0) - 30.0d0)
    ddpsi0(z)  = z* ( z*( -120.0d0*z + 180.0d0) - 60.0d0)
    dddpsi0(z) = z*( -360.0d0*z + 360.0d0) - 60.0d0

 !..psi1 and its derivatives
    psi1(z)    = z* (z**2 * ( z * (-3.0d0*z + 8.0d0) - 6.0d0) + 1.0d0)
    dpsi1(z)   = z*z * ( z * (-15.0d0*z + 32.0d0) - 18.0d0) +1.0d0
    ddpsi1(z)  = z * (z * (-60.0d0*z + 96.0d0) -36.0d0)
    dddpsi1(z) = z * (-180.0d0*z + 192.0d0) - 36.0d0

 !..psi2  and its derivatives
    psi2(z)    = 0.5d0*z*z*( z* ( z * (-z + 3.0d0) - 3.0d0) + 1.0d0)
    dpsi2(z)   = 0.5d0*z*( z*(z*(-5.0d0*z + 12.0d0) - 9.0d0) + 2.0d0)
    ddpsi2(z)  = 0.5d0*(z*( z * (-20.0d0*z + 36.0d0) - 18.0d0) +2.0d0)
    dddpsi2(z) = 0.5d0*(z * (-60.0d0*z + 72.0d0) - 18.0d0)
    
    if(.not.this%IsSetup) then
       write(*,*), "You need to call the procedure construct_h5tbl_eos"
       write(*,*), " - this will allocate the member function memory"
       stop
    endif
    
    lnRho = log(Rho)
    log10Rho = log10(Rho)
    lnT = log(T)
    log10T = log10(T)
    RhoSQ = Rho*Rho
    
    ierr = 0
    
    if((lnRho .lt. this%minlnRho) .or. (lnRho .gt. this%maxlnRho) &
         .or. (lnT .lt. this%minlnT) .or. (lnT .gt. this%maxlnT)) then
       write(*,*) "How is this possible?"
       write(*,*) "lnRho = ", lnRho, "lnT = ", lnT
       write(*,*) "lnRho range = ", this%minlnRho, this%maxlnRho
       write(*,*) "lnT   range = ", this%minlnT,   this%maxlnT
       ierr = -1
    else
       
       dI = get_Rho_index(lnRho)
       tI = get_T_index(lnT)
       if(h5dbg) then
          write(*,*) "Rho range: ", this%Rho_v(dI), this%Rho_v(dI+1)
          write(*,*) "T range: ", this%T_v(tI), this%T_v(tI+1)
          write(*,*) "dRho_v: ", this%dRho_v(dI)
          write(*,*) "dRho2_v: ", this%dRho2_v(dI)
          write(*,*) "dRho_i: ", this%dRho_i(dI)
          write(*,*) "dRho2_i: ", this%dRho2_i(dI)
          write(*,*) "dRho3_i: ", this%dRho3_i(dI)
       endif
       
       !! See this file for more information about the implementation
       include 'helm_interp.dek'
    end if
  contains
  
  !! Hash functions.  The grid must actually be logrithmically spaced
  !!  we also check that this is in fact the case when the grid is
  !!  begin loaded.  
    integer function get_Rho_index(lnRho)
      implicit none
      real(dp) :: lnRho
      get_Rho_index = floor((lnRho - this%minlnRho) / this%dlnRho) + 1 !! Start at index 1
    end function get_Rho_index
    
    integer function get_T_index(lnT)
      implicit none
      real(dp) :: lnT
      get_T_index = floor((lnT - this%minlnT) / this%dlnT) + 1 !! start at index 1
    end function get_T_index
    
    real(dp) function h5(w0t, w1t, w2t, w0mt, w1mt, w2mt, w0d, &
         w1d, w2d, w0md, w1md, w2md)
      implicit none
      real(dp), intent(in) :: w0t, w1t, w2t, w0mt, w1mt, w2mt, &
           w0d, w1d, w2d, w0md, w1md, w2md
      
      h5 = fi(1)  *w0d*w0t   + fi(2)  *w0md*w0t &
         + fi(3)  *w0d*w0mt  + fi(4)  *w0md*w0mt &
         + fi(5)  *w0d*w1t   + fi(6)  *w0md*w1t  &
         + fi(7)  *w0d*w1mt  + fi(8)  *w0md*w1mt &
         + fi(9)  *w0d*w2t   + fi(10) *w0md*w2t  &
         + fi(11) *w0d*w2mt  + fi(12) *w0md*w2mt &
         + fi(13) *w1d*w0t   + fi(14) *w1md*w0t  &
         + fi(15) *w1d*w0mt  + fi(16) *w1md*w0mt &
         + fi(17) *w2d*w0t   + fi(18) *w2md*w0t &
         + fi(19) *w2d*w0mt  + fi(20) *w2md*w0mt &
         + fi(21) *w1d*w1t   + fi(22) *w1md*w1t &
         + fi(23) *w1d*w1mt  + fi(24) *w1md*w1mt &
         + fi(25) *w2d*w1t   + fi(26) *w2md*w1t &
         + fi(27) *w2d*w1mt  + fi(28) *w2md*w1mt &
         + fi(29) *w1d*w2t   + fi(30) *w1md*w2t &
         + fi(31) *w1d*w2mt  + fi(32) *w1md*w2mt &
         + fi(33) *w2d*w2t   + fi(34) *w2md*w2t &
         + fi(35) *w2d*w2mt  + fi(36) *w2md*w2mt 
    end function h5
  end subroutine h5_DT_get
  
  subroutine safe_h5_PT_get(this, P, T, Rho, Pvect, Evect, Svect, weight, ierr)
    class (h5table_eos), target :: this
    real(dp), intent(in) :: P, T
    real(dp), intent(out) :: Rho, Pvect(num_derivs), Evect(num_derivs), Svect(num_derivs), weight
    integer, intent(out) :: ierr

    real(dp) :: log10P, log10T, log10P_low, log10P_high, &
         log10Rho_low, log10Rho_high, log10T_low, log10T_high

    log10P = log10(P)
    log10T = log10(T)
    
    !! Remember that this function depends on the user making good cuts
    call this%PT_get_Trange(log10P, log10T_low, log10T_high, ierr)
    if(ierr .ne. 0) return

    if(log10T_low .gt. log10T) then
       ierr = H5TBLERR_MINTEMPERATURE
       return
    elseif(log10T_high .lt. log10T) then
       ierr = H5TBLERR_MAXTEMPERATURE
       return
    endif
    !! If we get to this point, then we may be able to assume that
    !! we might be able to find the coordinates (log10P, log10T)
    
    !! Pass this along to inheritted procedure after doing checks
    call this%base_eos%PT_get(P, T, Rho, Pvect, Evect, Svect, weight, ierr)
  end subroutine safe_h5_PT_get

end module h5table_eos_mod
